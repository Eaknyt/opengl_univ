#include "vec2.h"

#include <cmath>
#include <iostream>

namespace epwmath {

Vec2::Vec2() :
    m_x(0.),
    m_y(0.)
{}

Vec2::Vec2(float x, float y) :
    m_x(x),
    m_y(y)
{}

Vec2::Vec2(const Vec2 &other) :
    m_x(other.m_x),
    m_y(other.m_y)
{}

float Vec2::x() const
{
    return m_x;
}

void Vec2::setX(float x)
{
    if (m_x != x) {
        m_x = x;
    }
}

float Vec2::y() const
{
    return m_y;
}

void Vec2::setY(float y)
{
    if (m_y != y) {
        m_y = y;
    }
}

float Vec2::length() const
{
    return std::sqrt(std::pow(m_x, 2) + std::pow(m_y, 2));
}

void Vec2::normalize()
{
    float myLength = length();

    m_x /= myLength;
    m_y /= myLength;
}

Vec2 Vec2::normalized() const
{
    Vec2 ret(*this);
    ret.normalize();
    
    return ret;
}

float Vec2::dotProduct(const Vec2 &other) const
{
    return m_x * other.m_x + m_y * other.m_y;
}

float Vec2::angle(const Vec2 &v) const
{
    float dotProd = dotProduct(v);

    float cosAngle = dotProd / (length() * v.length());

    return std::acos(cosAngle);
}

Vec2 Vec2::abs() const
{
    return Vec2(std::abs(m_x), std::abs(m_y));
}

bool Vec2::isReal() const
{
    return !std::isinf(m_x) && !std::isinf(m_y) &&
           !std::isnan(m_x) && !std::isnan(m_y);
}

float Vec2::angle(const Vec2 &v1, const Vec2 &v2)
{
    return v1.angle(v2);
}

float Vec2::dotProduct(const Vec2 &p1, const Vec2 &p2)
{
    return p1.dotProduct(p2);
}

bool operator==(const Vec2 &lhs, const Vec2 &rhs)
{
    return lhs.m_x == rhs.m_x && lhs.m_y == rhs.m_y;
}

bool operator!=(const Vec2 &lhs, const Vec2 &rhs)
{
    return !(lhs == rhs);
}

const Vec2 operator-(const Vec2 &v)
{
    return Vec2(-v.m_x, -v.m_y);
}

std::ostream &operator<<(std::ostream &os, const Vec2 &v)
{
    os << "Vec2(" << std::to_string(v.m_x) << ", "
       << std::to_string(v.m_y)
       << ")";

    return os;
}

} // namespace epwmath
