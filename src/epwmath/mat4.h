#ifndef H_EPWMATH_MAT4_H
#define H_EPWMATH_MAT4_H

#include <array>
#include <iosfwd>

#include "vec3.h"
#include "vec4.h"


namespace epwmath {

class Vec3;

class Mat4
{
public:
    Mat4();
    Mat4(float m11, float m12, float m13, float m14,
         float m21, float m22, float m23, float m24,
         float m31, float m32, float m33, float m34,
         float m41, float m42, float m43, float m44);
    Mat4(const Mat4 &other);

    bool isIdentity() const;
    void setIdentity();

    Vec3 map(const Vec3 &v) const;
    Vec4 map(const Vec4 &v) const;

    float determinant() const;

    bool isInvertible() const;

    Mat4 inverted() const;

    const float *constData() const;

    static Mat4 fromFrustum(float left, float right, float bottom,
                            float top,
                            float near, float far);

    static Mat4 fromInfinite(float left, float right, float bottom,
                             float top,
                             float near);

    static Mat4 fromOrtho(float left, float right, float bottom,
                          float top,
                          float near, float far);

    static Mat4 fromPerspective(float fovy, float aspectRatio,
                                float near, float far);

    static Mat4 fromLookAt(const Vec3 &pos, const Vec3 &target,
                           const Vec3 &up);

    static Mat4 fromTranslate(float x, float y, float z);
    static Mat4 fromTranslate(const Vec3 &v);

    static Mat4 fromScale(float sx, float sy, float sz);
    static Mat4 fromScale(const Vec3 &v);

    static Mat4 fromRotate(float ax, float ay, float az);
    static Mat4 fromRotateAround(const Vec3 &point,
                                 float ax, float ay, float az);

    Mat4 &translate(float x, float y, float z);

    inline Mat4 &translate(const Vec3 &v)
    {
        return translate(v.x(), v.y(), v.z());
    }

    Mat4 &scale(float sx, float sy, float sz);

    inline Mat4 &scale(const Vec3 &v)
    {
        return scale(v.x(), v.y(), v.z());
    }

    Mat4 &rotate(float ax, float ay, float az);

    inline Mat4 &rotate(const Vec3 &angles)
    {
        return rotate(angles.x(), angles.y(), angles.z());
    }

    Mat4 &rotateAround(const Vec3 &point, float ax, float ay, float az);

    inline Mat4 &rotateAround(const Vec3 &point, const Vec3 &angles)
    {
        return rotateAround(point, angles.x(), angles.y(), angles.z());
    }

    friend const Mat4 operator*(const Mat4 &m, float scalar);
    friend const Vec3 operator*(const Mat4 &m, const Vec3 &v);
    friend const Vec4 operator*(const Mat4 &m, const Vec4 &v);
    friend const Mat4 operator*(const Mat4 &a, const Mat4 &b);

    friend const Mat4 operator/(const Mat4 &m, float scalar);

    Mat4 &operator*=(const Mat4 &t);

    Mat4 &operator=(const Mat4 other);

    friend bool operator==(const Mat4 &a, const Mat4 &b);
    friend bool operator!=(const Mat4 &a, const Mat4 &b);

    float operator()(int row, int col) const;
    float &operator()(int row, int col);

    friend std::ostream &operator<<(std::ostream &os, const Mat4 &t);

private:
    std::array<float, 16> m_data;
}; // class Mat4

} // namespace epwmath

#endif // H_EPWMATH_MAT4_H
