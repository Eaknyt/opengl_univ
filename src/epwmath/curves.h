#ifndef H_CURVES_H
#define H_CURVES_H

#include <vector>

namespace epwmath {

class Vec3;

using Curve = std::vector<Vec3>;

float binomial(int n, int i);
float bernstein(int degree, int i, float u);

std::vector<Vec3> hermite(const Vec3 &p0, const Vec3 &v0,
                          const Vec3 &p1, const Vec3 &v1,
                          long count);

std::vector<Vec3> bezierBernstein(const std::vector<Vec3> &controlPoints,
                                  long count);
std::vector<Vec3> bezierCasteljau(const std::vector<Vec3> &controlPoints,
                                  long count);

} // namespace epwmath

#endif // H_CURVES_H
