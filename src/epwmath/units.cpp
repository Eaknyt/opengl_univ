#include "units.h"

#include <cmath>


namespace epwmath {

float radToDeg(float rad)
{
    return rad * 180. / M_PI;
}

float degToRad(float degrees)
{
    return degrees * M_PI / 180.;
}

} // namespace epwmath

