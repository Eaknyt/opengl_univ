#include "mat2.h"

#include <iostream>

#include "vec2.h"


namespace epwmath {

Mat2::Mat2() :
    m_data()
{
    setIdentity();
}

Mat2::Mat2(const Mat2 &other) :
    m_data()
{
    m_data = other.m_data;
}

Mat2::Mat2(float m00, float m10, float m01, float m11) :
    m_data()
{
    m_data[0] = m00;
    m_data[1] = m10;
    m_data[2] = m01;
    m_data[3] = m11;
}

bool Mat2::isIdentity() const
{
    bool ret = true;

    for (std::size_t x = 0; x < 2; ++x) {
        for (std::size_t y = 0; y < 2; ++y) {
            float mxy = operator()(x, y);

            if (x == y && mxy != 1) {
                ret = false;
                break;
            }
            else if (x != y && mxy != 0) {
                ret = false;
                break;
            }
        }
    } 

    return ret;
}

void Mat2::setIdentity()
{
    for (std::size_t x = 0; x < 2; ++x) {
        for (std::size_t y = 0; y < 2; ++y) {
            (*this)(x, y) = (x == y) ? 1 : 0;
        }
    }
}

const Vec2 operator*(const Mat2 &m, const Vec2 &p)
{
    float px = p.x();
    float py = p.y();

    float x = m(0, 0) * px + m(0, 1) * py;
    float y = m(1, 0) * px + m(1, 1) * py;

    Vec2 ret(x, y);

    return ret;
}

const Mat2 operator*(const Mat2 &a, const Mat2 &b)
{
    float a00 = a(0, 0);
    float a01 = a(0, 1);
    float a10 = a(1, 0);
    float a11 = a(1, 1);
    float b00 = b(0, 0);
    float b01 = b(0, 1);
    float b10 = b(1, 0);
    float b11 = b(1, 1);

    Mat2 ret;
    ret(0, 0) = a00 * b00 + a01 * b10;
    ret(0, 1) = a00 * b01 + a01 * b11;
    ret(1, 0) = a10 * b00 + a11 * b10;
    ret(1, 1) = a10 * b01 + a11 * b11;

    return ret;
}

bool operator==(const Mat2 &m1, const Mat2 &m2)
{
    return m1.m_data == m2.m_data;
}

bool operator!=(const Mat2 &m1, const Mat2 &m2)
{
    return !(m1 == m2);
}

float Mat2::operator()(int x, int y) const
{
    return m_data[y + x * 2];
}

float &Mat2::operator()(int x, int y)
{
    return m_data[y + x * 2];
}

std::ostream &operator<<(std::ostream &os, const Mat2 &m)
{
    os << "epwmath::Mat2(" << std::endl
       << m(0, 0) << " " << m(0, 1) << std::endl
       << m(1, 0) << " " << m(1, 1) << ")";

    return os;
}

} // namespace epwmath
