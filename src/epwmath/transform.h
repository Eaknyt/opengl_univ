#ifndef H_EPWMATH_TRANSFORM_H
#define H_EPWMATH_TRANSFORM_H

#include "mat4.h"
#include "vec3.h"

namespace epwmath {

class Transform
{
public:
    enum class Order
    {
        ScaleRotateTranslate,
        TranslateRotateScale
    };

    Transform();

    Vec3 translation() const;
    void setTranslation(const Vec3 &t);

    Vec3 scale() const;
    void setScale(const Vec3 &s);

    Vec3 rotation() const;
    void setRotation(const Vec3 &r);

    Vec3 rotationPivot() const;
    void setRotationPivot(const Vec3 &rotationPivot);

    Order order() const;
    void setOrder(Order order);

    Mat4 matrix() const;

private:
    void updateMatrix();

private:
    Vec3 m_translation;
    Vec3 m_scale;
    Vec3 m_rotation;
    Vec3 m_rotationPivot;

    Order m_order;

    Mat4 m_matrix;
};

} // namespace epwmath

#endif // H_EPWMATH_TRANSFORM_H
