#include "boundingbox.h"

namespace epwmath {

AABoundingBox::AABoundingBox() :
    m_center(),
    m_radii()
{}

AABoundingBox::AABoundingBox(const std::vector<Vec3> vertices) :
    m_center(),
    m_radii()
{
    compute(vertices);
}

Vec3 AABoundingBox::center() const
{
    return m_center;
}

Vec3 AABoundingBox::radii() const
{
    return m_radii;
}

Vec3 AABoundingBox::nearPoint() const
{
    return m_center - m_radii;
}

Vec3 AABoundingBox::farPoint() const
{
    return m_center + m_radii;
}

std::array<Vec3, 8> AABoundingBox::getCorners()
{
    std::array<Vec3, 8> ret;

    const float cx = m_center.x();
    const float cy = m_center.y();
    const float cz = m_center.z();

    const float rx = m_radii.x();
    const float ry = m_radii.y();
    const float rz = m_radii.z();

    ret.at(0) = {cx - rx, cy - ry, cz + rz};
    ret.at(1) = {cx + rx, cy - ry, cz + rz};
    ret.at(2) = {cx + rx, cy + ry, cz + rz};
    ret.at(3) = {cx - rx, cy + ry, cz + rz};

    ret.at(4) = {cx - rx, cy - ry, cz - rz};
    ret.at(5) = {cx + rx, cy - ry, cz - rz};
    ret.at(6) = {cx + rx, cy + ry, cz - rz};
    ret.at(7) = {cx - rx, cy + ry, cz - rz};

    return ret;
}

void AABoundingBox::compute(const std::vector<Vec3> vertices)
{
    if (vertices.empty()) {
        m_center = Vec3();
        m_radii = Vec3();

        return;
    }

    Vec3 min = vertices[0];
    Vec3 max = vertices[0];

    for (int i = 1; i < vertices.size(); ++i) {
        const Vec3 &p = vertices[i];

        const float px = p.x();
        const float py = p.y();
        const float pz = p.z();

        if (px < min.x()) {
            min.setX(px);
        }

        if (py < min.y()) {
            min.setY(py);
        }

        if (pz < min.z()) {
            min.setZ(pz);
        }

        if (px > max.x()) {
            max.setX(px);
        }

        if (py > max.y()) {
            max.setY(py);
        }

        if (pz > max.z()) {
            max.setZ(pz);
        }
    }

    m_center = 0.5 * (min + max);
    m_radii = 0.5 * (max - min);
}

} // namespace epwmath
