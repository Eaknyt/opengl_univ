#include "transform.h"


namespace epwmath {

Transform::Transform() :
    m_translation(),
    m_scale(1.f, 1.f, 1.f),
    m_rotation(),
    m_order(Order::ScaleRotateTranslate),
    m_rotationPivot(),
    m_matrix()
{
    updateMatrix();
}

Vec3 Transform::translation() const
{
    return m_translation;
}

void Transform::setTranslation(const Vec3 &t)
{
    if (m_translation != t) {
        m_translation = t;

        updateMatrix();
    }
}

Vec3 Transform::scale() const
{
    return m_scale;
}

void Transform::setScale(const Vec3 &s)
{
    if (m_scale != s) {
        m_scale = s;

        updateMatrix();
    }
}

Vec3 Transform::rotation() const
{
    return m_rotation;
}

void Transform::setRotation(const Vec3 &r)
{
    if (m_rotation != r) {
        m_rotation = r;

        updateMatrix();
    }
}

Vec3 Transform::rotationPivot() const
{
    return m_rotationPivot;
}

void Transform::setRotationPivot(const Vec3 &pivot)
{
    if (m_rotationPivot != pivot) {
        m_rotationPivot = pivot;

        updateMatrix();
    }
}

Transform::Order Transform::order() const
{
    return m_order;
}

void Transform::setOrder(Transform::Order order)
{
    if (m_order != order) {
        m_order = order;

        updateMatrix();
    }
}

Mat4 Transform::matrix() const
{
    return m_matrix;
}

void Transform::updateMatrix()
{
    Mat4 newMatrix;

    if (m_order == Order::ScaleRotateTranslate) {
        newMatrix.scale(m_scale)
                 .rotate(m_rotation)
                 .translate(m_translation);
    }
    else if (m_order == Order::TranslateRotateScale) {
        newMatrix.translate(m_translation)
                 .rotateAround(m_rotationPivot, m_rotation)
                 .scale(m_scale);
    }

    m_matrix = newMatrix;
}

} // namespace epwmath
