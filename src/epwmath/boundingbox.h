#ifndef H_EPWMATH_BOUNDINGBOX_H
#define H_EPWMATH_BOUNDINGBOX_H

#include <array>
#include <vector>

#include "vec3.h"

namespace epwmath {

class AABoundingBox
{
public:
    AABoundingBox();
    AABoundingBox(const std::vector<Vec3> vertices);

    Vec3 center() const;
    Vec3 radii() const;

    Vec3 nearPoint() const;
    Vec3 farPoint() const;

    std::array<Vec3, 8> getCorners();

private:
    void compute(const std::vector<Vec3> vertices);

private:
    Vec3 m_center;
    Vec3 m_radii;
};

} // namespace epwmath

#endif // H_EPWMATH_BOUNDINGBOX_H
