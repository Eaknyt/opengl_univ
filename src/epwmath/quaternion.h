#ifndef H_EPWMATH_QUATERNION_H
#define H_EPWMATH_QUATERNION_H

#include "mat4.h"

namespace epwmath {

class Vec3;

class Quat
{
public:
    Quat();
    Quat(float scalar, float x, float y, float z);
    Quat(float scalar, const Vec3 &v);

    float x() const;
    void setX(float x);

    float y() const;
    void setY(float y);

    float z() const;
    void setZ(float z);

    float scalar() const;
    void setScalar(float s);

    Quat conjugate() const;
    Quat inverse() const;

    float length() const;

    Quat normalized() const;
    void normalize();

    Mat4 toRotationMatrix() const;

    static Quat fromAxisAngle(const Vec3 &axis, float angle);
    static Quat rotationBetween(const Vec3 &u, const Vec3 &v);

    friend const Quat operator*(const Quat &lhs, const Quat &rhs);

    friend const Vec3 operator*(const Quat &lhs, const Vec3 &rhs);

private:
    float m_x;
    float m_y;
    float m_z;

    float m_scalar;
};

} // namespace epwmath

#endif // H_EPWMATH_QUATERNION_H
