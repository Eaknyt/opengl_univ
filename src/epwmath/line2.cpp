#include "line2.h"

#include <iostream>

namespace epwmath {

Line2::Line2() :
	m_p1(),
    m_p2()
{}

Line2::Line2(float x1, float y1, float x2, float y2) :
    m_p1(x1, y1),
    m_p2(x2, y2)
{}

Line2::Line2(const Vec2 &p1, const Vec2 &p2) :
	m_p1(p1),
	m_p2(p2)
{}

Line2::Line2(const Line2 &other) :
	m_p1(other.m_p1),
	m_p2(other.m_p2)
{}

Vec2 Line2::p1() const
{
	return m_p1;
}

void Line2::setP1(const Vec2 &p)
{
	if (m_p1 != p) {
		m_p1 = p;
	}
}

Vec2 Line2::p2() const
{
	return m_p2;
}

void Line2::setP2(const Vec2 &p)
{
	if (m_p2 != p) {
		m_p2 = p;
	}
}

bool Line2::hasPoint(const Vec2 &p) const
{
	bool ret = (m_p1.x() <= p.x() && p.x() <= m_p2.x()) &&
			   (m_p1.y() <= p.y() && p.y() <= m_p2.y());

	return ret;
}

Line2::IntersectType Line2::intersects(const Line2 &other, Vec2 &p)
{
	float x1 = m_p1.x();
	float y1 = m_p1.y();
	float x2 = m_p2.x();
	float y2 = m_p2.y();
	float x3 = other.m_p1.x();
	float y3 = other.m_p1.y();
	float x4 = other.m_p2.x();
	float y4 = other.m_p2.y();

	// Check for one-point intersection
	float px = ((x1 * y2 - y1 * x2) * (x3 - x4) - (x1 - x2) * (x3 * y4 - y3 * x4)) /
				((x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4));
	
	float py = ((x1 * y2 - y1 * x2) * (y3 - y4) - (y1 - y2) * (x3 * y4 - y3 * x4)) /
				((x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4));

    Vec2 pp(px, py);

	if (pp.isReal()) {
		p = pp;

		return OnePointIntersect;
	}

	// Then check for infinite intersection
	Vec2 meAsVec(x2 - x1, y2 - y1);
	meAsVec.normalize();

	Vec2 lineVec(x4 - x3, y4 - y3);
	lineVec.normalize();

	bool infinite = (meAsVec == lineVec || meAsVec == -lineVec) &&
					(hasPoint(other.m_p1) || hasPoint(other.m_p2));

	if (infinite) {
		return InfiniteIntersect;
	}

	return NoIntersect;
}

std::ostream &operator<<(std::ostream &os, const Line2 &line)
{
    float x1 = line.m_p1.x();
    float y1 = line.m_p1.y();
    float x2 = line.m_p2.x();
    float y2 = line.m_p2.y();

    os << "Line2(" << x1 << ", " << y1 << ", "
       << x2 << ", " << y2 << ")";

    return os;
}

} // namespace epwmath
