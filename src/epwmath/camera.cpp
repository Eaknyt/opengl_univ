#include "camera.h"

#include <cmath>
#include <iostream>

#include "units.h"


namespace epwmath {

Camera::Camera() :
    m_projectionType(Projection::Perspective),
    m_pos(1, 0, 0),
    m_target(),
    m_viewVector(m_target - m_pos),
    m_up(0, 1, 0),
    m_forward(m_target - m_pos),
    m_right(m_up.normal(m_forward)),
    m_aspectRatio(1.),
    m_fovY(45.),
    m_near(1),
    m_far(10000.),
    m_projectionMatrix(),
    m_viewMatrix(),
    m_callback()
{
    updateDirectionVectors();
    updateProjectionMatrix();
    updateViewMatrix();
}

Camera::Projection Camera::projection() const
{
    return m_projectionType;
}

void Camera::setProjection(Camera::Projection projType)
{
    if (m_projectionType != projType) {
        m_projectionType = projType;

        updateProjectionMatrix();

        if (m_callback) {
            m_callback(this);
        }
    }
}

Vec3 Camera::pos() const
{
    return m_pos;
}

void Camera::setPos(const Vec3 &pos)
{
    if (m_pos != pos) {
        m_pos = pos;

        updateDirectionVectors();
        keepForwardUpAngle();
        updateViewMatrix();

        if (m_callback) {
            m_callback(this);
        }
    }
}

Vec3 Camera::target() const
{
    return m_target;
}

void Camera::setTarget(const Vec3 &target)
{
    if (m_target != target) {
        m_target = target;

        updateDirectionVectors();
        keepForwardUpAngle();
        updateViewMatrix();

        if (m_callback) {
            m_callback(this);
        }
    }
}

Vec3 Camera::up() const
{
    return m_up;
}

void Camera::setUp(const Vec3 &up)
{
    if (m_up != up) {
        m_up = up;

        syncRightVector();

        m_target += m_up.normal(m_right);

        updateDirectionVectors();
        updateViewMatrix();

        if (m_callback) {
            m_callback(this);
        }
    }
}

Vec3 Camera::forward() const
{
    return m_forward;
}

Vec3 Camera::right() const
{
    return m_right;
}

float Camera::aspectRatio() const
{
    return m_aspectRatio;
}

void Camera::setAspectRatio(float ratio)
{
    if (m_aspectRatio != ratio) {
        m_aspectRatio = ratio;

        updateProjectionMatrix();

        if (m_callback) {
            m_callback(this);
        }
    }
}

float Camera::fovY() const
{
    return m_fovY;
}

void Camera::setFovY(float fovy)
{
    if (m_fovY != fovy && 0. <= fovy && fovy <= 180.) {
        m_fovY = fovy;

        updateProjectionMatrix();

        if (m_callback) {
            m_callback(this);
        }
    }
}

float Camera::near() const
{
    return m_near;
}

void Camera::setNear(float nearVal)
{
    if (m_near != nearVal) {
        m_near = nearVal;

        updateProjectionMatrix();

        if (m_callback) {
            m_callback(this);
        }
    }
}

float Camera::far() const
{
    return m_far;
}

void Camera::setFar(float farVal)
{
    if (m_far != farVal) {
        m_far = farVal;

        updateProjectionMatrix();

        if (m_callback) {
            m_callback(this);
        }
    }
}

void Camera::zoom(float factor)
{
    float newFovY = m_fovY + factor;

    const float minLimit = 0.;
    const float maxLimit = 180.;

    if (newFovY < minLimit) {
        m_fovY = minLimit;
    }
    else if (newFovY > maxLimit) {
        m_fovY = maxLimit;
    }

    setFovY(newFovY);
}

void Camera::lookAt(const Vec3 &pos, const Vec3 &target, const Vec3 &up)
{
    m_pos = pos;
    m_target = target;
    m_up = up;

    updateDirectionVectors();
    syncRightVector();

    updateViewMatrix();

    if (m_callback) {
        m_callback(this);
    }
}

void Camera::moveBy(const Vec3 &amount)
{
    m_pos += amount;
    m_target += amount;

    updateDirectionVectors();
    updateViewMatrix();

    if (m_callback) {
        m_callback(this);
    }
}

void Camera::moveForward(float offset)
{
    const Vec3 moveAmount = m_forward * offset;

    m_pos += moveAmount;
    m_target += moveAmount;

    updateDirectionVectors();
    updateViewMatrix();

    if (m_callback) {
        m_callback(this);
    }
}

void Camera::move(float offsetX, float offsetZ)
{
    const Vec3 amount = (m_right * offsetX) + (m_forward * offsetZ);

    moveBy(amount);
}

void Camera::truck(float offsetX, float offsetY)
{
    const Vec3 amount = (m_right * offsetX) + (m_up * offsetY);

    moveBy(amount);
}

Mat4 Camera::projectionMatrix() const
{
    return m_projectionMatrix;
}

Mat4 Camera::viewMatrix() const
{
    return m_viewMatrix;
}

void Camera::rotateAroundTarget(float angleX, float angleY)
{
    const Quat panQ = panQuat(angleX);
    const Quat tiltQ = tiltQuat(angleY);

    const Quat rotationQuat = panQ * tiltQ;

    m_viewVector = rotationQuat * m_viewVector;

    setPos(m_target - m_viewVector);
}

void Camera::rotateEye(float angleX, float angleY)
{
    const Quat panQ = Quat::fromAxisAngle(Vec3(0, 1, 0), angleX);
    const Quat tiltQ = tiltQuat(-angleY);

    const Quat rotationQ = panQ * tiltQ;

    m_up = rotationQ * m_up;
    m_viewVector = rotationQ * m_viewVector;
    m_target = m_pos + m_viewVector;

    m_forward = m_viewVector.normalized();

    syncRightVector();

    updateViewMatrix();

    if (m_callback) {
        m_callback(this);
    }
}

void Camera::setCallback(const std::function<void (Camera *)> &callback)
{
    m_callback = callback;
}

void Camera::syncRightVector()
{
    m_right = m_up.normal(m_forward);
}

void Camera::syncUpVector()
{
    m_up = m_forward.normal(m_right);
}

void Camera::updateDirectionVectors()
{
    m_viewVector = m_target - m_pos;

    m_forward = m_viewVector.normalized();
}

void Camera::keepForwardUpAngle()
{
    syncUpVector();
    syncRightVector();
}

void Camera::updateProjectionMatrix()
{
    switch (m_projectionType)
    {
    case Projection::Perspective:
        m_projectionMatrix = Mat4::fromPerspective(m_fovY, m_aspectRatio,
                                                   m_near, m_far);
        break;
    case Projection::Orthographic:
        m_projectionMatrix = Mat4::fromOrtho(-m_fovY * m_aspectRatio,
                                             m_fovY * m_aspectRatio,
                                             -m_fovY, m_fovY,
                                             m_near, m_far);
        break;
    case Projection::InfinitePerspective:
        m_projectionMatrix = Mat4::fromInfinite(-m_fovY * m_aspectRatio,
                                                m_fovY * m_aspectRatio,
                                                -m_fovY, m_fovY,
                                                m_near);
        break;
    }
}

void Camera::updateViewMatrix()
{
    m_viewMatrix = Mat4::fromLookAt(m_pos, m_target, m_up);
}

Quat Camera::panQuat(float angle) const
{
    return Quat::fromAxisAngle(m_up, angle);
}

Quat Camera::tiltQuat(float angle) const
{
    return Quat::fromAxisAngle(m_right, angle);
}

Quat Camera::rollQuat(float angle) const
{
    return Quat::fromAxisAngle(m_forward, angle);
}

} // namespace epwmath
