#ifndef H_VEC2_H
#define H_VEC2_H

#include <iosfwd>

namespace epwmath {

class Vec2
{
public:
    Vec2();
    Vec2(float x, float y);
    Vec2(const Vec2 &other);

    float x() const;
    void setX(float x);
    
    float y() const;
    void setY(float y);
    
    float length() const;

    void normalize();
    Vec2 normalized() const;

    float dotProduct(const Vec2 &other) const;

    float angle(const Vec2 &v) const;

    Vec2 abs() const;

    bool isReal() const;

    static float angle(const Vec2 &v1, const Vec2 &v2);
    static float dotProduct(const Vec2 &v1, const Vec2 &v2);

    friend bool operator==(const Vec2 &lhs, const Vec2 &rhs);
    friend bool operator!=(const Vec2 &lhs, const Vec2 &rhs);

    friend const Vec2 operator-(const Vec2 &v);

    friend std::ostream &operator<<(std::ostream &os, const Vec2 &v);

private:
    float m_x;
    float m_y;
};

} // namespace epwmath

#endif // H_VEC2_H
