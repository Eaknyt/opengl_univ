#include "surfaces.h"

#include "curves.h"

namespace epwmath {

CurveSet cylindricSurface(const std::vector<Vec3> generatorControlPoints,
                          const Vec3 &p1, const Vec3 &p2,
                          long directorCount, long generatorCount)
{
    CurveSet ret(directorCount);

    for (Curve &c : ret) {
        c.resize(generatorCount);
    }

    Curve generatorCurve = bezierCasteljau(generatorControlPoints,
                                           generatorCount);

    Vec3 directorVec(p2.x() - p1.x(), p2.y() - p1.y(), p2.z() - p1.z());

    for (std::size_t i = 0; i < directorCount; ++i) {
        float u = static_cast<float>(i) / directorCount;

        Vec3 p = (directorVec * u).projectOnLine(p1, p2);
        Vec3 posOffset = p - generatorCurve[0];

        for (int j = 0; j < generatorCurve.size(); ++j) {
            Vec3 generatorPoint = generatorCurve[j] + posOffset;

            ret[i][j] = generatorPoint;
        }
    }

    return ret;
}

CurveSet ruledSurface(const std::vector<Vec3> &b1ControlPoints,
                      const std::vector<Vec3> &b2ControlPoints,
                      long uCount, long vCount)
{
    CurveSet ret(uCount);

    for (Curve &c : ret) {
        c.resize(vCount);
    }

    Curve P = bezierCasteljau(b1ControlPoints, uCount);
    Curve Q = bezierCasteljau(b2ControlPoints, uCount);

    for (std::size_t i = 0; i < uCount; ++i) {
        for (std::size_t j = 0; j < vCount; ++j) {
            float v = static_cast<float>(i) / vCount;

            ret[i][j] = (1 - v) * P[j] + v * Q[j];
        }
    }

    return ret;
}

CurveSet bezierSurface(const std::vector<Vec3> &controlPointsU, int uCount,
                       const std::vector<Vec3> &controlPointsV, int vCount)
{

}

} // namespace epwmath
