#ifndef H_EPWMATH_OCTREE_H
#define H_EPWMATH_OCTREE_H

#include <functional>
#include <vector>

#include "vec3.h"


namespace epwmath {

class Octree
{
public:
    enum class IntersectSurfaceHint
    {
        OutsideSurface,
        WithinSurface,
        IntersectsSurface
    };

    using AdaptativeFunction = std::function<IntersectSurfaceHint (Octree *)>;

    Octree();
    Octree(const Vec3 &center, float radius, Octree *parent);
    ~Octree();

    Vec3 center() const;

    float radius() const;

    Octree *parent() const;

    std::vector<Octree *> children();

    int depth() const;

    bool draw() const;

    bool isEmpty();

    bool hasChildren();

    void clear();

    void subdiv(int depth);
    void subdiv(int depth, const AdaptativeFunction &func);

private:
    Vec3 m_center;
    float m_radius;

    Octree *m_parent;
    std::vector<Octree *> m_children;

    bool m_draw;
};

} // namespace epwmath

#endif // H_EPWMATH_OCTREE_H
