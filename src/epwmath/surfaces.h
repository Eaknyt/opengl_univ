#ifndef H_EPWMATH_SURFACES_H
#define H_EPWMATH_SURFACES_H

#include <vector>

#include "vec3.h"

namespace epwmath {

using CurveSet = std::vector<std::vector<Vec3>>;

CurveSet cylindricSurface(const std::vector<Vec3> generatorControlPoints,
                          const Vec3 &p1, const Vec3 &p2,
                          long directorCount, long generatorCount);

CurveSet ruledSurface(const std::vector<Vec3> &b1ControlPoints,
                      const std::vector<Vec3> &b2ControlPoints,
                      long uCount, long vCount);

CurveSet bezierSurface(const std::vector<Vec3> &controlPointsU, int uCount,
                       const std::vector<Vec3> &controlPointsV, int vCount);

} // namespace epwmath

#endif // H_EPWMATH_SURFACES_H
