#include "mat3.h"

#include <cmath>
#include <iostream>

#include "mat2.h"
#include "units.h"


namespace epwmath {

Mat3::Mat3() :
    m_data()
{
    setIdentity();
}

Mat3::Mat3(float m00, float m01, float m10, float m11)
{
    setIdentity();

    operator()(0, 0) = m00;
    operator()(0, 1) = m01;
    operator()(1, 0) = m10;
    operator()(1, 1) = m11;
}

Mat3::Mat3(float m00, float m01, float m02,
                       float m10, float m11, float m12,
                       float m20, float m21, float m22) :
    m_data()
{
    operator()(0, 0) = m00;
    operator()(0, 1) = m01;
    operator()(0, 2) = m02;

    operator()(1, 0) = m10;
    operator()(1, 1) = m11;
    operator()(1, 2) = m12;

    operator()(2, 0) = m20;
    operator()(2, 1) = m21;
    operator()(2, 2) = m22;
}

Mat3::Mat3(const Mat2 &m) :
    m_data()
{
    setIdentity();

    operator()(0, 0) = m(0, 0);
    operator()(0, 1) = m(0, 1);
    operator()(1, 0) = m(1, 0);
    operator()(1, 1) = m(1, 1);
}

Mat3::Mat3(const Mat3 &other) :
    m_data()
{
    m_data = other.m_data;
}

bool Mat3::isIdentity() const
{
    bool ret = true;

    for (std::size_t x = 0; x < 3; ++x) {
        for (std::size_t y = 0; y < 3; ++y) {
            float mxy = operator()(x, y);

            if ( (x == y && mxy != 1) || (x != y && mxy != 0)) {
                ret = false;
                break;
            }
        }
    } 

    return ret;
}

void Mat3::setIdentity()
{
    for (std::size_t x = 0; x < 3; ++x) {
        for (std::size_t y = 0; y < 3; ++y) {
            operator()(x, y) = (x == y) ? 1 : 0;
        }
    }
}

float Mat3::determinant() const
{
    const float m11 = operator()(0, 0);
    const float m12 = operator()(0, 1);
    const float m13 = operator()(0, 2);
    const float m21 = operator()(1, 0);
    const float m22 = operator()(1, 1);
    const float m23 = operator()(1, 2);
    const float m31 = operator()(2, 0);
    const float m32 = operator()(2, 1);
    const float m33 = operator()(2, 2);

    float ret = m11 * m22 * m33 + m21 * m32 * m13 + m31 * m12 * m23 -
                m11 * m32 * m23 - m31 * m22 * m13 - m21 * m12 * m33;

    return ret;
}

Vec2 Mat3::map(const Vec2 &p) const
{
    float a = operator()(0, 0);
    float b = operator()(1, 0);
    float c = operator()(0, 1);
    float d = operator()(1, 1);
    float e = operator()(0, 2);
    float f = operator()(1, 2);

    float px = p.x();
    float py = p.y();

    float x = a * px + c * py + e;
    float y = b * px + d * py + f;

    Vec2 ret(x, y);

    return ret;
}

Line2 Mat3::map(const Line2 &l) const
{
    Vec2 mp1 = map(l.p1());
    Vec2 mp2 = map(l.p2());

    Line2 ret(mp1, mp2);

    return ret;
}

Mat3 Mat3::fromTranslate(float dx, float dy)
{
    Mat3 ret;
    ret(0, 2) = dx;
    ret(1, 2) = dy;

    return ret;
}

Mat3 Mat3::fromScale(float sx, float sy)
{
    Mat2 m(sx, 0, 0, sy);

    Mat3 ret(m);

    return ret;
}

Mat3 Mat3::fromRotate(float angle)
{
    float m00, m01, m10, m11;
    m00 = std::cos(degToRad(angle));
    m11 = std::cos(degToRad(angle));
    m01 = std::sin(degToRad(angle));
    m10 = -m01;

    Mat2 m(m00, m10, m01, m11);
    Mat3 ret(m);

    return ret;
}

Mat3 Mat3::fromRotate(float angle, float x, float y)
{
    Mat3 tt = Mat3::fromTranslate(x, y);
    Mat3 tr = Mat3::fromRotate(angle);
    Mat3 ttn = Mat3::fromTranslate(-x, -y);

    Mat3 ret = tt * tr * ttn;

    return ret;
}

Mat3 Mat3::fromRotate(float angle, Vec2 p)
{
    return Mat3::fromRotate(angle, p.x(), p.y());
}

Mat3 Mat3::fromHomothety(float factor)
{
    Mat2 m(factor, 0, factor, 0);

    Mat3 ret(m);

    return ret;
}

Mat3 Mat3::fromReflectionX()
{
    Mat2 m(1, 0, 0, -1);

    Mat3 ret(m);

    return ret;
}

Mat3 Mat3::fromReflectionY()
{
    Mat2 m(-1, 0, 0, 1);

    Mat3 ret(m);

    return ret;
}

Mat3 Mat3::fromReflectionXY()
{
    Mat2 m(0, 1, 1, 0);

    Mat3 ret(m);

    return ret;
}

const Vec2 operator*(const Mat3 &m, const Vec2 &p)
{
    return m.map(p);
}

const Line2 operator*(const Mat3 &m, const Line2 &l)
{
    Line2 ret = m.map(l);

    return ret;
}

Mat3 &Mat3::operator*=(const Mat3 &t)
{
    *this = (*this * t);

    return *this;
}

Mat3 &Mat3::operator=(const Mat3 other)
{
    m_data = other.m_data;

    return *this;
}

const Mat3 operator*(const Mat3 &a, const Mat3 &b)
{
    float a11 = a(0, 0);
    float a12 = a(0, 1);
    float a13 = a(0, 2);
    float a21 = a(1, 0);
    float a22 = a(1, 1);
    float a23 = a(1, 2);
    float a31 = a(2, 0);
    float a32 = a(2, 1);
    float a33 = a(2, 2);

    float b11 = b(0, 0);
    float b12 = b(0, 1);
    float b13 = b(0, 2);
    float b21 = b(1, 0);
    float b22 = b(1, 1);
    float b23 = b(1, 2);
    float b31 = b(2, 0);
    float b32 = b(2, 1);
    float b33 = b(2, 2);

    Mat3 ret;
    ret(0, 0) = a11 * b11 + a12 * b21 + a13 * b31;
    ret(0, 1) = a11 * b12 + a12 * b22 + a13 * b32;
    ret(0, 2) = a11 * b13 + a12 * b23 + a13 * b33;
    ret(1, 0) = a21 * b11 + a22 * b21 + a23 * b31;
    ret(1, 1) = a21 * b12 + a22 * b22 + a23 * b32;
    ret(1, 2) = a21 * b13 + a22 * b23 + a23 * b33;
    ret(2, 0) = a31 * b11 + a32 * b21 + a33 * b31;
    ret(2, 1) = a31 * b12 + a32 * b22 + a33 * b32;
    ret(2, 2) = a31 * b13 + a32 * b23 + a33 * b33;

    return ret;
}

bool operator==(const Mat3 &a, const Mat3 &b)
{
    return a.m_data == b.m_data;
}

bool operator!=(const Mat3 &a, const Mat3 &b)
{
    return !(a == b);
}

float Mat3::operator()(int x, int y) const
{
    return m_data[y + x * 3];
}

float &Mat3::operator()(int x, int y)
{
    return m_data[y + x * 3];
}

std::ostream &operator<<(std::ostream &os, const Mat3 &t)
{
    os << "epwmath::Mat3(" << std::endl
       << t(0, 0) << " " << t(0, 1) << " " << t(0, 2) << std::endl
       << t(1, 0) << " " << t(1, 1) << " " << t(1, 2) << std::endl
       << t(2, 0) << " " << t(2, 1) << " " << t(2, 2) << ")";
}

} // namespace epwmath
