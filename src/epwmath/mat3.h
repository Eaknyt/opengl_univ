#ifndef H_EPWMATH_MAT3_H
#define H_EPWMATH_MAT3_H

#include <array>

#include "line2.h"


namespace epwmath {

class Mat2;

class Mat3
{
public:
    Mat3();
    Mat3(float m00, float m01, float m10, float m11);
    Mat3(float m00, float m01, float m02,
               float m10, float m11, float m12,
               float m20, float m21, float m22);
    Mat3(const Mat2 &m);
    Mat3(const Mat3 &other);

    bool isIdentity() const;
    void setIdentity();

    float determinant() const;

    Vec2 map(const Vec2 &p) const;
    Line2 map(const Line2 &l) const;

    static Mat3 fromTranslate(float dx, float dy);
    static Mat3 fromScale(float sx, float sy);
    static Mat3 fromRotate(float angle);
    static Mat3 fromRotate(float angle, float x, float y);
    static Mat3 fromRotate(float angle, Vec2 p);
    static Mat3 fromHomothety(float factor);
    static Mat3 fromReflectionX();
    static Mat3 fromReflectionY();
    static Mat3 fromReflectionXY();

    friend const Vec2 operator*(const Mat3 &m, const Vec2 &p);

    friend const Line2 operator*(const Mat3 &m, const Line2 &l);

    friend const Mat3 operator*(const Mat3 &a,
                                      const Mat3 &b);

    Mat3 &operator*=(const Mat3 &t);

    Mat3 &operator=(const Mat3 other);

    friend bool operator==(const Mat3 &a, const Mat3 &b);
    friend bool operator!=(const Mat3 &a, const Mat3 &b);

    float operator()(int x, int y) const;
    float &operator()(int x, int y);

    friend std::ostream &operator<<(std::ostream &os, const Mat3 &t);

private:
    std::array<float, 9> m_data;
};

} // namespace epwmath

#endif // H_EPWMATH_MAT3_H
