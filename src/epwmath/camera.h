#ifndef H_EPWMATH_CAMERA_H
#define H_EPWMATH_CAMERA_H

#include <functional>

#include "mat4.h"
#include "quaternion.h"
#include "vec3.h"


namespace epwmath {

class Camera
{
public:
    enum class Projection : int
    {
        Perspective,
        Orthographic,
        InfinitePerspective
    };

    Camera();

    Projection projection() const;
    void setProjection(Projection type);

    Vec3 pos() const;
    void setPos(const Vec3 &pos);

    Vec3 target() const;
    void setTarget(const Vec3 &target);

    Vec3 up() const;
    void setUp(const Vec3 &up);

    Vec3 forward() const;
    Vec3 right() const;

    float aspectRatio() const;
    void setAspectRatio(float ratio);

    float fovY() const;
    void setFovY(float fovY);

    float near() const;
    void setNear(float nearVal);

    float far() const;
    void setFar(float farVal);

    void zoom(float factor);

    void lookAt(const Vec3 &pos, const Vec3 &target,
                const Vec3 &up);

    void moveBy(const Vec3 &amount);
    void moveForward(float offset);

    void move(float offsetX, float offsetZ);
    void truck(float offsetX, float offsetY);

    Mat4 projectionMatrix() const;
    Mat4 viewMatrix() const;

    void rotateAroundTarget(float angleX, float angleY);
    void rotateEye(float angleX, float angleY);

    void setCallback(const std::function<void (Camera *)> &callback);

private:
    void syncRightVector();
    void syncUpVector();

    void updateDirectionVectors();
    void keepForwardUpAngle();

    void updateProjectionMatrix();
    void updateViewMatrix();

    Quat panQuat(float angle) const;
    Quat tiltQuat(float angle) const;
    Quat rollQuat(float angle) const;

private:
    Projection m_projectionType;

    Vec3 m_pos;
    Vec3 m_target;
    Vec3 m_viewVector;
    Vec3 m_up;
    Vec3 m_forward;
    Vec3 m_right;

    float m_aspectRatio;
    float m_fovY;

    float m_near;
    float m_far;

    Mat4 m_projectionMatrix;
    Mat4 m_viewMatrix;

    std::function<void (Camera *)> m_callback;
};

} // namespace epwmath

#endif // H_EPWMATH_CAMERA_H
