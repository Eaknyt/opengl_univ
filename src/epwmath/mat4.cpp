#include "mat4.h"

#include <cmath>
#include <iostream>

#include "units.h"

namespace epwmath {

Mat4::Mat4() :
    m_data()
{
    setIdentity();
}

Mat4::Mat4(float m11, float m12, float m13, float m14,
           float m21, float m22, float m23, float m24,
           float m31, float m32, float m33, float m34,
           float m41, float m42, float m43, float m44) :
    m_data()
{
    operator()(0, 0) = m11;
    operator()(0, 1) = m12;
    operator()(0, 2) = m13;
    operator()(0, 3) = m14;

    operator()(1, 0) = m21;
    operator()(1, 1) = m22;
    operator()(1, 2) = m23;
    operator()(1, 3) = m24;

    operator()(2, 0) = m31;
    operator()(2, 1) = m32;
    operator()(2, 2) = m33;
    operator()(2, 3) = m34;

    operator()(3, 0) = m41;
    operator()(3, 1) = m42;
    operator()(3, 2) = m43;
    operator()(3, 3) = m44;
}

Mat4::Mat4(const Mat4 &other) :
    m_data()
{
    m_data = other.m_data;
}

bool Mat4::isIdentity() const
{
    bool ret = true;

    for (std::size_t x = 0; x < 4; ++x) {
        for (std::size_t y = 0; y < 4; ++y) {
            const float mxy = operator()(x, y);

            if ( (x == y && mxy != 1) || (x != y && mxy != 0)) {
                ret = false;
                break;
            }
        }
    }

    return ret;
}

void Mat4::setIdentity()
{
    for (std::size_t x = 0; x < 4; ++x) {
        for (std::size_t y = 0; y < 4; ++y) {
            operator()(x, y) = (x == y) ? 1 : 0;
        }
    }
}

Vec3 Mat4::map(const Vec3 &v) const
{
    const float vx = v.x();
    const float vy = v.y();
    const float vz = v.z();

    const float m11 = operator()(0, 0);
    const float m12 = operator()(0, 1);
    const float m13 = operator()(0, 2);
    const float m14 = operator()(0, 3);
    const float m21 = operator()(1, 0);
    const float m22 = operator()(1, 1);
    const float m23 = operator()(1, 2);
    const float m24 = operator()(1, 3);
    const float m31 = operator()(2, 0);
    const float m32 = operator()(2, 1);
    const float m33 = operator()(2, 2);
    const float m34 = operator()(2, 3);

    const float x = m11 * vx + m12 * vy + m13 * vz + m14;
    const float y = m21 * vx + m22 * vy + m23 * vz + m24;
    const float z = m31 * vx + m32 * vy + m33 * vz + m34;

    Vec3 ret(x, y, z);

    return ret;
}

Vec4 Mat4::map(const Vec4 &v) const
{
    const float vx = v.x();
    const float vy = v.y();
    const float vz = v.z();

    const float m11 = operator()(0, 0);
    const float m12 = operator()(0, 1);
    const float m13 = operator()(0, 2);
    const float m14 = operator()(0, 3);
    const float m21 = operator()(1, 0);
    const float m22 = operator()(1, 1);
    const float m23 = operator()(1, 2);
    const float m24 = operator()(1, 3);
    const float m31 = operator()(2, 0);
    const float m32 = operator()(2, 1);
    const float m33 = operator()(2, 2);
    const float m34 = operator()(2, 3);
    const float m41 = operator()(3, 0);
    const float m42 = operator()(3, 1);
    const float m43 = operator()(3, 2);
    const float m44 = operator()(3, 3);

    const float x = m11 * vx + m12 * vy + m13 * vz + m14;
    const float y = m21 * vx + m22 * vy + m23 * vz + m24;
    const float z = m31 * vx + m32 * vy + m33 * vz + m34;
    const float w = m41 * vx + m42 * vy + m43 * vz + m44;

    Vec4 ret(x, y, z, w);

    return ret;
}

// http://www.cg.info.hiroshima-cu.ac.jp/~miyazaki/knowledge/teche23.html
float Mat4::determinant() const
{
    const float m11 = operator()(0, 0);
    const float m12 = operator()(0, 1);
    const float m13 = operator()(0, 2);
    const float m14 = operator()(0, 3);
    const float m21 = operator()(1, 0);
    const float m22 = operator()(1, 1);
    const float m23 = operator()(1, 2);
    const float m24 = operator()(1, 3);
    const float m31 = operator()(2, 0);
    const float m32 = operator()(2, 1);
    const float m33 = operator()(2, 2);
    const float m34 = operator()(2, 3);
    const float m41 = operator()(3, 0);
    const float m42 = operator()(3, 1);
    const float m43 = operator()(3, 2);
    const float m44 = operator()(3, 3);

    float ret {
        m11 * m22 * m33 * m44 + m11 * m23 * m34 * m42 + m11 * m24 * m32 * m43 +
        m12 * m21 * m34 * m43 + m12 * m23 * m31 * m44 + m12 * m24 * m33 * m41 +
        m13 * m21 * m32 * m44 + m13 * m22 * m34 * m41 + m13 * m24 * m31 * m42 +
        m14 * m21 * m33 * m42 + m14 * m22 * m31 * m43 + m14 * m23 * m32 * m41 -
        m11 * m22 * m34 * m43 - m11 * m23 * m32 * m44 - m11 * m24 * m33 * m42 -
        m12 * m21 * m33 * m44 - m12 * m23 * m34 * m41 - m12 * m24 * m31 * m43 -
        m13 * m21 * m34 * m42 - m13 * m22 * m31 * m44 - m13 * m24 * m32 * m41 -
        m14 * m21 * m32 * m43 - m14 * m22 * m33 * m41 - m14 * m23 * m31 * m42
    };

    return ret;
}

bool Mat4::isInvertible() const
{
    return determinant() != 0;
}

Mat4 Mat4::inverted() const
{
    float myDet = determinant();

    if (myDet == 0) {
        std::cerr << "Mat4::inverse() > matrix is not inversible" << std::endl;
        return Mat4();
    }

    const float a11 = operator()(0, 0);
    const float a12 = operator()(0, 1);
    const float a13 = operator()(0, 2);
    const float a14 = operator()(0, 3);
    const float a21 = operator()(1, 0);
    const float a22 = operator()(1, 1);
    const float a23 = operator()(1, 2);
    const float a24 = operator()(1, 3);
    const float a31 = operator()(2, 0);
    const float a32 = operator()(2, 1);
    const float a33 = operator()(2, 2);
    const float a34 = operator()(2, 3);
    const float a41 = operator()(3, 0);
    const float a42 = operator()(3, 1);
    const float a43 = operator()(3, 2);
    const float a44 = operator()(3, 3);

    const float b11 = a22 * a33 * a44 + a23 * a34 * a42 + a24 * a32 * a43 -
            a22 * a34 * a43 - a23 * a32 * a44 - a24 * a33 * a42;
    const float b12 = a12 * a34 * a43 + a13 * a32 * a44 + a14 * a33 * a42 -
            a12 * a33 * a44 - a13 * a34 * a42 - a14 * a32 * a43;
    const float b13 = a12 * a23 * a44 + a13 * a24 * a42 + a14 * a22 * a43 -
            a12 * a24 * a43 - a13 * a22 * a44 - a14 * a23 * a42;
    const float b14 = a12 * a24 * a33 + a13 * a22 * a34 + a14 * a23 * a32 -
            a12 * a23 * a34 - a13 * a24 * a32 - a14 * a22 * a33;
    const float b21 = a21 * a34 * a43 + a23 * a31 * a44 + a24 * a33 * a41 -
            a21 * a33 * a44 - a23 * a34 * a41 - a24 * a31 * a43;
    const float b22 = a11 * a33 * a44 + a13 * a34 * a41 + a14 * a31 * a43 -
            a11 * a34 * a43 - a13 * a31 * a44 - a14 * a33 * a41;
    const float b23 = a11 * a24 * a43 + a13 * a21 * a44 + a14 * a23 * a41 -
            a11 * a23 * a44 - a13 * a24 * a41 - a14 * a21 * a43;
    const float b24 = a11 * a23 * a34 + a13 * a24 * a31 + a14 * a21 * a33 -
            a11 * a24 * a33 - a13 * a21 * a34 - a14 * a23 * a31;
    const float b31 = a21 * a32 * a44 + a22 * a34 * a41 + a24 * a31 * a42 -
            a21 * a34 * a42 - a22 * a31 * a44 - a24 * a32 * a41;
    const float b32 = a11 * a34 * a42 + a12 * a31 * a44 + a14 * a32 * a41 -
            a11 * a32 * a44 - a12 * a34 * a41 - a14 * a31 * a42;
    const float b33 = a11 * a22 * a44 + a12 * a24 * a41 + a14 * a21 * a42 -
            a11 * a24 * a42 - a12 * a21 * a44 - a14 * a22 * a41;
    const float b34 = a11 * a24 * a32 + a12 * a21 * a34 + a14 * a22 * a31 -
            a11 * a22 * a34 - a12 * a24 * a31 - a14 * a21 * a32;
    const float b41 = a21 * a33 * a42 + a22 * a31 * a43 + a23 * a32 * a41 -
            a21 * a32 * a43 - a22 * a33 * a41 - a23 * a31 * a42;
    const float b42 = a11 * a32 * a43 + a12 * a33 * a41 + a13 * a31 * a42 -
            a11 * a33 * a42 - a12 * a31 * a43 - a13 * a32 * a41;
    const float b43 = a11 * a23 * a42 + a12 * a21 * a43 + a13 * a22 * a41 -
            a11 * a22 * a43 - a12 * a23 * a41 - a13 * a21 * a42;
    const float b44 = a11 * a22 * a33 + a12 * a23 * a31 + a13 * a21 * a32 -
            a11 * a23 * a32 - a12 * a21 * a33 - a13 * a22 * a31;

    const Mat4 b(b11, b12, b13, b14,
                 b21, b22, b23, b24,
                 b31, b32, b33, b34,
                 b41, b42, b43, b44);

    myDet = 1.f / myDet;

    Mat4 ret = b * myDet;

    return ret;
}

const float *Mat4::constData() const
{
    return m_data.data();
}

Mat4 Mat4::fromFrustum(float left, float right, float bottom, float top,
                       float near, float far)
{
    const float m11 = (2 * near) / (right - left);
    const float m13 = (right + left) / (right - left);
    const float m22 = (2 * near) / (top - bottom);
    const float m23 = (top + bottom) / (top - bottom);
    const float m33 = - (far + near) / (far - near);
    const float m34 = - (2 * near * far) / (far - near);
    const float m43 = -1;

    Mat4 ret(m11,   0,      0,      0,
             0,     m22,    0,      0,
             m13,   m23,    m33,    m43,
             0,     0,      m34,    0);

    return ret;
}

Mat4 Mat4::fromInfinite(float left, float right, float bottom, float top,
                        float near)
{
    const float m11 = (2 * near) / (right - left);
    const float m13 = (right + left) / (right - left);
    const float m22 = (2 * near) / (top - bottom);
    const float m23 = (top + bottom) / (top - bottom);
    const float m33 = -1;
    const float m34 = -2 * near;
    const float m43 = -1;

    Mat4 ret(m11,   0,      0,      0,
             0,     m22,    0,      0,
             m13,   m23,    m33,    m43,
             0,     0,      m34,    0);

    return ret;
}

Mat4 Mat4::fromOrtho(float left, float right, float bottom, float top,
                     float near, float far)
{
    const float m11 = 2 / (right - left);
    const float m14 = - (right + left) / (right - left);
    const float m22 = 2 / (top - bottom);
    const float m24 = - (top + bottom) / (top - bottom);
    const float m33 = -2 / (far - near);
    const float m34 = - (far + near) / (far - near);

    Mat4 ret(m11,   0,      0,      m14,
             0,     m22,    0,      m24,
             0,     0,      m33,    m34,
             0,     0,      0,      1);

    return ret;
}

Mat4 Mat4::fromPerspective(float fovy, float aspectRatio,
                           float near, float far)
{
    const float f = 1 / std::tan(degToRad(fovy) / 2);

    const float m11 = f / aspectRatio;
    const float m22 = f;
    const float m33 = (far + near) / (near - far);
    const float m34 = (2 * far * near) / (near - far);
    const float m43 = -1;

    Mat4 ret(m11,   0,      0,      0,
             0,     m22,    0,      0,
             0,     0,      m33,    m34,
             0,     0,      m43,    0);

    return ret;
}

Mat4 Mat4::fromLookAt(const Vec3 &pos, const Vec3 &target, const Vec3 &up)
{
    const Vec3 zAxis = (pos - target).normalized();
    const Vec3 xAxis = Vec3::crossProduct(up, zAxis).normalized();
    const Vec3 yAxis = Vec3::crossProduct(zAxis, xAxis);

    const float m14 = -Vec3::dotProduct(xAxis, pos);
    const float m24 = -Vec3::dotProduct(yAxis, pos);
    const float m34 = -Vec3::dotProduct(zAxis, pos);

    Mat4 ret(xAxis.x(),     xAxis.y(),  xAxis.z(),      m14,
             yAxis.x(),     yAxis.y(),  yAxis.z(),      m24,
             zAxis.x(),     zAxis.y(),  zAxis.z(),      m34,
             0,             0,          0,              1);

    return ret;
}

Mat4 Mat4::fromTranslate(float x, float y, float z)
{
    Mat4 ret;
    ret.translate(x, y , z);

    return ret;
}

Mat4 Mat4::fromTranslate(const Vec3 &v)
{
    Mat4 ret;
    ret.translate(v);

    return ret;
}

Mat4 Mat4::fromScale(float sx, float sy, float sz)
{
    Mat4 ret;
    ret.scale(sx, sy, sz);

    return ret;
}

Mat4 Mat4::fromScale(const Vec3 &v)
{
    Mat4 ret;
    ret.scale(v);

    return ret;
}

Mat4 Mat4::fromRotate(float ax, float ay, float az)
{
    Mat4 ret;
    ret.rotate(ax, ay, az);

    return ret;
}

Mat4 Mat4::fromRotateAround(const Vec3 &point, float ax, float ay, float az)
{
    Mat4 ret;
    ret.rotateAround(point, ax, ay, az);

    return ret;
}

Mat4 &Mat4::translate(float x, float y, float z)
{
    Mat4 m(1, 0, 0, x,
           0, 1, 0, y,
           0, 0, 1, z,
           0, 0, 0, 1);

    (*this) *= m;

    return *this;
}

Mat4 &Mat4::scale(float sx, float sy, float sz)
{
    Mat4 m(sx,  0,  0,  0,
           0,   sy, 0,  0,
           0,   0,  sz, 0,
           0,   0,  0,  1);

    (*this) *= m;

    return *this;
}

Mat4 &Mat4::rotate(float ax, float ay, float az)
{
    // X
    const float axRad = degToRad(ax);
    const float cosAx = std::cos(axRad);
    const float sinAx = std::sin(axRad);

    const Mat4 rx(1,    0,         0,          0,
                  0,    cosAx,     -sinAx,     0,
                  0,    sinAx,     cosAx,      0,
                  0,    0,         0,          1);

    // Y
    const float ayRad = degToRad(ay);
    const float cosAy = std::cos(ayRad);
    const float sinAy = std::sin(ayRad);

    const Mat4 ry(cosAy,     0,     sinAy,  0,
                  0,         1,     0,      0,
                  -sinAy,    0,     cosAy,  0,
                  0,         0,     0,      1);

    // Z
    const float azRad = degToRad(az);
    const float cosAz = std::cos(azRad);
    const float sinAz = std::sin(azRad);

    const Mat4 rz(cosAz,    -sinAz, 0,  0,
                  sinAz,    cosAz,  0,  0,
                  0,        0,      1,  0,
                  0,        0,      0,  1);

    const Mat4 transform = rx * ry * rz;

    (*this) *= transform;

    return *this;
}

Mat4 &Mat4::rotateAround(const Vec3 &point, float ax, float ay, float az)
{
    Mat4 transform;
    transform.translate(point);
    transform.rotate(ax, ay, az);
    transform.translate(-point);

    (*this) *= transform;

    return *this;
}

const Mat4 operator*(const Mat4 &m, float scalar)
{
    Mat4 ret = m;

    for (std::size_t i = 0; i < ret.m_data.size(); ++i) {
        ret.m_data[i] *= scalar;
    }

    return ret;
}

const Vec3 operator*(const Mat4 &m, const Vec3 &v)
{
    Vec3 ret = m.map(v);

    return ret;
}

const Vec4 operator*(const Mat4 &m, const Vec4 &v)
{
    Vec4 ret = m.map(v);

    return ret;
}

const Mat4 operator*(const Mat4 &a, const Mat4 &b)
{
    const float a00 = a(0, 0);
    const float a01 = a(0, 1);
    const float a02 = a(0, 2);
    const float a03 = a(0, 3);
    const float a10 = a(1, 0);
    const float a11 = a(1, 1);
    const float a12 = a(1, 2);
    const float a13 = a(1, 3);
    const float a20 = a(2, 0);
    const float a21 = a(2, 1);
    const float a22 = a(2, 2);
    const float a23 = a(2, 3);
    const float a30 = a(3, 0);
    const float a31 = a(3, 1);
    const float a32 = a(3, 2);
    const float a33 = a(3, 3);

    const float b00 = b(0, 0);
    const float b01 = b(0, 1);
    const float b02 = b(0, 2);
    const float b03 = b(0, 3);
    const float b10 = b(1, 0);
    const float b11 = b(1, 1);
    const float b12 = b(1, 2);
    const float b13 = b(1, 3);
    const float b20 = b(2, 0);
    const float b21 = b(2, 1);
    const float b22 = b(2, 2);
    const float b23 = b(2, 3);
    const float b30 = b(3, 0);
    const float b31 = b(3, 1);
    const float b32 = b(3, 2);
    const float b33 = b(3, 3);

    Mat4 ret;
    ret(0, 0) = a00 * b00 + a01 * b10 + a02 * b20 + a03 * b30;
    ret(0, 1) = a00 * b01 + a01 * b11 + a02 * b21 + a03 * b31;
    ret(0, 2) = a00 * b02 + a01 * b12 + a02 * b22 + a03 * b32;
    ret(0, 3) = a00 * b03 + a01 * b13 + a02 * b23 + a03 * b33;
    ret(1, 0) = a10 * b00 + a11 * b10 + a12 * b20 + a13 * b30;
    ret(1, 1) = a10 * b01 + a11 * b11 + a12 * b21 + a13 * b31;
    ret(1, 2) = a10 * b02 + a11 * b12 + a12 * b22 + a13 * b32;
    ret(1, 3) = a10 * b03 + a11 * b13 + a12 * b23 + a13 * b33;
    ret(2, 0) = a20 * b00 + a21 * b10 + a22 * b20 + a23 * b30;
    ret(2, 1) = a20 * b01 + a21 * b11 + a22 * b21 + a23 * b31;
    ret(2, 2) = a20 * b02 + a21 * b12 + a22 * b22 + a23 * b32;
    ret(2, 3) = a20 * b03 + a21 * b13 + a22 * b23 + a23 * b33;
    ret(3, 0) = a30 * b00 + a31 * b10 + a32 * b20 + a33 * b30;
    ret(3, 1) = a30 * b01 + a31 * b11 + a32 * b21 + a33 * b31;
    ret(3, 2) = a30 * b02 + a31 * b12 + a32 * b22 + a33 * b32;
    ret(3, 3) = a30 * b03 + a31 * b13 + a32 * b23 + a33 * b33;

    return ret;
} // operator*(const Mat4 &a, const Mat4 &b)

const Mat4 operator/(const Mat4 &m, float scalar)
{
    Mat4 ret = m;

    for (std::size_t i = 0; i < ret.m_data.size(); ++i) {
        ret.m_data[i] /= scalar;
    }

    return ret;
}

Mat4 &Mat4::operator*=(const Mat4 &t)
{
    *this = (*this * t);

    return *this;
}

Mat4 &Mat4::operator=(const Mat4 other)
{
    m_data = other.m_data;

    return *this;
}

bool operator==(const Mat4 &a, const Mat4 &b)
{
    return a.m_data == b.m_data;
}

bool operator!=(const Mat4 &a, const Mat4 &b)
{
    return !(a == b);
}

float Mat4::operator()(int row, int col) const
{
    return m_data[row + col * 4];
}

float &Mat4::operator()(int row, int col)
{
    return m_data[row + col * 4];
}

std::ostream &operator<<(std::ostream &os, const Mat4 &t)
{
    os << "epwmath::Mat4(" << std::endl
       << t(0, 0) << " " << t(0, 1) << " " << t(0, 2) << " " << t(0, 3) << std::endl
       << t(1, 0) << " " << t(1, 1) << " " << t(1, 2) << " " << t(1, 3) << std::endl
       << t(2, 0) << " " << t(2, 1) << " " << t(2, 2) << " " << t(2, 3) << std::endl
       << t(3, 0) << " " << t(3, 1) << " " << t(3, 2) << " " << t(3, 3) << ")";
}

} // namespace epwmath
