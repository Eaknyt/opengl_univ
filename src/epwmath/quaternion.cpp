#include "quaternion.h"

#include <cmath>

#include "vec3.h"

namespace epwmath {

// Reference used : http://web.archive.org/web/20041029003853/http://www.j3d.org/matrix_faq/matrfaq_latest.html#Q47

Quat::Quat() :
    m_x(0.),
    m_y(0.),
    m_z(0.),
    m_scalar(0.)
{}

Quat::Quat(float scalar, float x, float y, float z) :
    m_x(x),
    m_y(y),
    m_z(z),
    m_scalar(scalar)
{}

Quat::Quat(float scalar, const Vec3 &v) :
    m_x(v.x()),
    m_y(v.y()),
    m_z(v.z()),
    m_scalar(scalar)
{}

float Quat::x() const
{
    return m_x;
}

void Quat::setX(float x)
{
    if (m_x != x) {
        m_x = x;
    }
}

float Quat::y() const
{
    return m_y;
}

void Quat::setY(float y)
{
    if (m_y != y) {
        m_y = y;
    }
}

float Quat::z() const
{
    return m_z;
}

void Quat::setZ(float z)
{
    if (m_z != z) {
        m_z = z;
    }
}

float Quat::scalar() const
{
    return m_scalar;
}

void Quat::setScalar(float s)
{
    if (m_scalar != s) {
        m_scalar = s;
    }
}

Quat Quat::conjugate() const
{
    Quat ret(m_scalar, -m_x, -m_y, -m_z);

    return ret;
}

Quat Quat::inverse() const
{
    Quat ret;

    return ret;
}

float Quat::length() const
{
    float ret = std::sqrt(std::pow(m_scalar, 2) +
                           std::pow(m_x, 2) +
                           std::pow(m_y, 2) +
                           std::pow(m_z, 2));

    return ret;
}

Quat Quat::normalized() const
{
    const float len = length();

    Quat ret(m_scalar / len, m_x / len, m_y / len, m_z / len);

    return ret;
}

void Quat::normalize()
{
    const float len = length();

    m_x /= len;
    m_y /= len;
    m_z /= len;

    m_scalar /= len;
}

Mat4 Quat::toRotationMatrix() const
{
    const float xx = m_x * m_x;
    const float xy = m_x * m_y;
    const float xz = m_x * m_z;
    const float xw = m_x * m_scalar;
    const float yy = m_y * m_y;
    const float yz = m_y * m_z;
    const float yw = m_y * m_scalar;
    const float zz = m_z * m_z;
    const float zw = m_z * m_scalar;

    const float m00 = 1 - 2 * (yy + zz);
    const float m01 = 2 * (xy + zw);
    const float m02 = 2 * (xz - yw);

    const float m10 = 2 * (xy - zw);
    const float m11 = 1 - 2 * (xx + zz);
    const float m12 = 2 * (yz + xw);

    const float m20 = 2 * (xz + yw);
    const float m21 = 2 * (yz - xw);
    const float m22 = 1 - 2 * (xx + yy);

    Mat4 ret(m00,   m01,    m02,    0,
             m10,   m11,    m12,    0,
             m20,   m21,    m22,    0,
             0,     0,      0,      1);

    return ret;
}

Quat Quat::fromAxisAngle(const Vec3 &axis, float angle)
{
    const Vec3 a = axis.normalized();
    const float angle_2 = angle * 0.5;

    const float sin_a = std::sin(angle_2);
    const float cos_a = std::cos(angle_2);

    Quat ret(cos_a,
             a.x() * sin_a,
             a.y() * sin_a,
             a.z() * sin_a);

    //TODO Normalize the quat if any value is very close to zero

    return ret;
}

Quat Quat::rotationBetween(const Vec3 &u, const Vec3 &v)
{
    const float cos_theta = Vec3::dotProduct(u.normalized(), v.normalized());
    const float angle = std::acos(cos_theta);

    const Vec3 axis = Vec3::crossProduct(u, v).normalized();

    return Quat::fromAxisAngle(axis, angle);
}

const Quat operator*(const Quat &lhs, const Quat &rhs)
{
    // Retrieve values
    const float w1 = lhs.m_scalar;
    const float w2 = rhs.m_scalar;

    const float x1 = lhs.m_x;
    const float x2 = rhs.m_x;

    const float y1 = lhs.m_y;
    const float y2 = rhs.m_y;

    const float z1 = lhs.m_z;
    const float z2 = rhs.m_z;

    // Compute result
    const float ms = w1 * w2 - x1 * x2 - y1 * y2 - z1 * z2;
    const float mx = w1 * x2 + x1 * w2 + y1 * z2 - z1 * y2;
    const float my = w1 * y2 + y1 * w2 + z1 * x2 - x1 * z2;
    const float mz = w1 * z2 + z1 * w2 + x1 * y2 - y1 * x2;

    Quat ret(ms, mx, my, mz);

    return ret;
}

const Vec3 operator*(const Quat &lhs, const Vec3 &rhs)
{
    const Quat q = lhs.normalized();

    const Quat p(0, rhs.x(), rhs.y(), rhs.z());
    const Quat pp = q * p * q.conjugate();

    Vec3 ret(pp.m_x, pp.m_y, pp.m_z);

    return ret;
}

} // namespace epwmath
