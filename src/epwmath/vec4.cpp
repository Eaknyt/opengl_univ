#include "vec4.h"

#include <cmath>
#include <iostream>

namespace epwmath {

Vec4::Vec4() :
    m_x(0.),
    m_y(0.),
    m_z(0.),
    m_w(0.)
{}

Vec4::Vec4(float x, float y, float z, float w) :
    m_x(x),
    m_y(y),
    m_z(z),
    m_w(w)
{}

Vec4::Vec4(const Vec4 &other) :
    m_x(other.m_x),
    m_y(other.m_y),
    m_z(other.m_z),
    m_w(other.m_w)
{}

float Vec4::x() const
{
    return m_x;
}

void Vec4::setX(float x)
{
    if (m_x != x) {
        m_x = x;
    }
}

float Vec4::y() const
{
    return m_y;
}

void Vec4::setY(float y)
{
    if (m_y != y) {
        m_y = y;
    }
}

float Vec4::z() const
{
    return m_z;
}

void Vec4::setZ(float z)
{
    if (m_z != z) {
        m_z = z;
    }
}

float Vec4::w() const
{
    return m_w;
}

void Vec4::setW(float w)
{
    if (m_w != w) {
        m_w = w;
    }
}

float Vec4::length() const
{
    return std::sqrt(std::pow(m_x, 2) + std::pow(m_y, 2) +
                     std::pow(m_z, 2) + std::pow(m_w, 2));
}

void Vec4::normalize()
{
    float myLength = length();

    m_x /= myLength;
    m_y /= myLength;
    m_z /= myLength;
    m_w /= myLength;
}

Vec4 Vec4::normalized() const
{
    Vec4 ret(*this);
    ret.normalize();

    return ret;
}

float Vec4::dotProduct(const Vec4 &other) const
{
    return m_x * other.m_x + m_y * other.m_y + m_z * other.m_z + m_w * other.m_w;
}

const Vec4 operator+(const Vec4 &rhs, const Vec4 &lhs)
{
    Vec4 ret(lhs.m_x + rhs.m_x,
             lhs.m_y + rhs.m_y,
             lhs.m_z + rhs.m_z,
             lhs.m_w + rhs.m_w);

    return ret;
}

Vec4 &Vec4::operator+=(const Vec4 &rhs)
{
    m_x += rhs.m_x;
    m_y += rhs.m_y;
    m_z += rhs.m_z;
    m_w += rhs.m_w;

    return *this;
}

const Vec4 operator-(const Vec4 &lhs, const Vec4 &rhs)
{
    Vec4 ret(lhs.m_x - rhs.m_x,
             lhs.m_y - rhs.m_y,
             lhs.m_z - rhs.m_z,
             lhs.m_w - rhs.m_w);

    return ret;
}

Vec4 &Vec4::operator-=(const Vec4 &rhs)
{
    m_x -= rhs.m_x;
    m_y -= rhs.m_y;
    m_z -= rhs.m_z;
    m_w -= rhs.m_w;

    return *this;
}

const Vec4 operator-(const Vec4 &v)
{
    return Vec4(-v.m_x, -v.m_y, -v.m_z, -v.m_w);
}

const Vec4 operator*(float factor, const Vec4 &v)
{
    Vec4 ret(v.m_x * factor,
             v.m_y * factor,
             v.m_z * factor,
             v.m_w * factor);

    return ret;
}

const Vec4 operator*(const Vec4 &v, float factor)
{
    return factor * v;
}

const Vec4 operator*(const Vec4 &lhs, const Vec4 &rhs)
{
    Vec4 ret(lhs.m_x * rhs.m_x,
             lhs.m_y * rhs.m_y,
             lhs.m_z * rhs.m_z,
             lhs.m_w * rhs.m_w);

    return ret;
}

Vec4 &Vec4::operator*=(float factor)
{
    m_x *= factor;
    m_y *= factor;
    m_z *= factor;
    m_w *= factor;

    return *this;
}

const Vec4 operator/(const Vec4 &lhs, float rhs)
{
    Vec4 ret(lhs.m_x / rhs,
             lhs.m_y / rhs,
             lhs.m_z / rhs,
             lhs.m_w / rhs);

    return ret;
}

Vec4 &Vec4::operator/=(float factor)
{
    m_x /= factor;
    m_y /= factor;
    m_z /= factor;
    m_w /= factor;

    return *this;
}

bool operator==(const Vec4 &lhs, const Vec4 &rhs)
{
    return lhs.m_x == rhs.m_x && lhs.m_y == rhs.m_y && lhs.m_z == rhs.m_z &&
           lhs.m_w == rhs.m_w;
}

bool operator!=(const Vec4 &lhs, const Vec4 &rhs)
{
    return !(lhs == rhs);
}

std::ostream &operator<<(std::ostream &os, const Vec4 &v)
{
    os << "epwmath::Vec4(" +
          std::to_string(v.m_x) + ", " +
          std::to_string(v.m_y) + ", " +
          std::to_string(v.m_z) + ", " +
          std::to_string(v.m_w) +
          ")";

    return os;
}

} // namespace epwmath
