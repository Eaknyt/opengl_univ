#ifndef H_EPWMATH_MAT2_H
#define H_EPWMATH_MAT2_H

#include <array>
#include <iosfwd>


namespace epwmath {

class Vec2;

class Mat2
{
public:
    Mat2();
    Mat2(float m00, float m10, float m01, float m11);
    Mat2(const Mat2 &other);

    bool isIdentity() const;
    void setIdentity();

    friend const Vec2 operator*(const Mat2 &m, const Vec2 &p);

    friend const Mat2 operator*(const Mat2 &m1, const Mat2 &m2);

    Mat2 &operator=(const Mat2 other);

    friend bool operator==(const Mat2 &m1, const Mat2 &m2);
    friend bool operator!=(const Mat2 &m1, const Mat2 &m2);

    float operator()(int x, int y) const;
    float &operator()(int x, int y);

    friend std::ostream &operator<<(std::ostream &os, const Mat2 &m);

private:
    std::array<float, 4> m_data;
};

} // namespace epwmath

#endif // H_EPWMATH_MAT2_H
