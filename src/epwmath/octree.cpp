#include "octree.h"

#include <array>


namespace epwmath {

Octree::Octree() :
    m_center(),
    m_radius(0),
    m_parent(nullptr),
    m_children(),
    m_draw(false)
{}

Octree::Octree(const Vec3 &center, float radius, Octree *parent) :
    m_center(center),
    m_radius(radius),
    m_parent(parent),
    m_children(),
    m_draw(true)
{}

Octree::~Octree()
{
    m_parent = nullptr;

    m_children.clear();
}

Vec3 Octree::center() const
{
    return m_center;
}

float Octree::radius() const
{
    return m_radius;
}

Octree *Octree::parent() const
{
    return m_parent;
}

std::vector<Octree *> Octree::children()
{
    return m_children;
}

int Octree::depth() const
{
    int ret = 0;
    Octree *it = m_parent;

    if (it) {
        ret++;

        it = it->m_parent;
    }

    return ret;
}

bool Octree::draw() const
{
    return m_draw;
}

bool Octree::isEmpty()
{
    return m_children.empty();
}

bool Octree::hasChildren()
{
    return !isEmpty();
}

void Octree::clear()
{
    m_children.clear();
}

void Octree::subdiv(int depth)
{
    if (!m_children.empty()) {
        return;
    }

    if (!depth) {
        return;
    }

    const float cx = m_center.x();
    const float cy = m_center.y();
    const float cz = m_center.z();

    const float radius_2 = m_radius * 0.5;

    std::array<Vec3, 8> childrenCenters = {
        Vec3 {cx - radius_2, cy - radius_2, cz - radius_2},
        Vec3 {cx + radius_2, cy - radius_2, cz - radius_2},
        Vec3 {cx - radius_2, cy + radius_2, cz - radius_2},
        Vec3 {cx + radius_2, cy + radius_2, cz - radius_2},
        Vec3 {cx - radius_2, cy - radius_2, cz + radius_2},
        Vec3 {cx + radius_2, cy - radius_2, cz + radius_2},
        Vec3 {cx - radius_2, cy + radius_2, cz + radius_2},
        Vec3 {cx + radius_2, cy + radius_2, cz + radius_2}
    };

    int nextDepth = depth - 1;

    for (const Vec3 &center : childrenCenters) {
        auto child = new Octree(center, radius_2, this);

        m_children.push_back(child);

        for (Octree *c : m_children) {
            c->subdiv(nextDepth);
        }
    }
}

void Octree::subdiv(int depth, const Octree::AdaptativeFunction &func)
{
    if (!m_children.empty()) {
        return;
    }

    Octree::IntersectSurfaceHint intersectResult = func(this);

    bool subdivideAgain = false;

    // Is inside
    if (intersectResult == Octree::IntersectSurfaceHint::WithinSurface) {
        m_draw = true;
    }
    // Is outside
    else if (intersectResult == Octree::IntersectSurfaceHint::OutsideSurface) {
        m_draw = false;
    }
    // Intersects
    else if (intersectResult == Octree::IntersectSurfaceHint::IntersectsSurface) {
        m_draw = !depth;

        subdivideAgain = depth;
    }

    if (!subdivideAgain) {
        return;
    }

    // Subdivide
    int nextDepth = depth - 1;

    const float cx = m_center.x();
    const float cy = m_center.y();
    const float cz = m_center.z();

    const float radius_2 = m_radius * 0.5;

    std::array<Vec3, 8> subdivCenters = {
        Vec3 {cx - radius_2, cy - radius_2, cz - radius_2},
        Vec3 {cx + radius_2, cy - radius_2, cz - radius_2},
        Vec3 {cx - radius_2, cy + radius_2, cz - radius_2},
        Vec3 {cx + radius_2, cy + radius_2, cz - radius_2},
        Vec3 {cx - radius_2, cy - radius_2, cz + radius_2},
        Vec3 {cx + radius_2, cy - radius_2, cz + radius_2},
        Vec3 {cx - radius_2, cy + radius_2, cz + radius_2},
        Vec3 {cx + radius_2, cy + radius_2, cz + radius_2}
    };

    for (const Vec3 &center : subdivCenters) {
        auto child = new Octree(center, radius_2, this);

        m_children.push_back(child);

        for (Octree *c : m_children) {
            c->subdiv(nextDepth, func);
        }
    }
}

} // namespace epwmath
