#include "curves.h"

#include <cmath>
#include <iostream>

#include "units.h"
#include "vec3.h"


namespace {

using namespace epwmath;

Vec3 casteljau_recursive(float u, int i, int k,
                         const std::vector<Vec3> controlPoints)
{
    if (!k) {
        return controlPoints[i];
    }

    // /!\ Recursion
    Vec3 ret = (1 - u) * casteljau_recursive(u, i, k - 1, controlPoints)
            + u * casteljau_recursive(u, i + 1, k - 1, controlPoints);

    return ret;
}

} // anon namespace


namespace epwmath {

float binomial(int n, int i)
{
    return std::tgamma(n + 1) / (std::tgamma(i + 1) * std::tgamma(n - i + 1));
}

float bernstein(int degree, int i, float u)
{
    float ret =
            binomial(degree, i) * std::pow(u, i) * std::pow(1 - u, degree - i);

    return ret;
}

std::vector<Vec3> hermite(const Vec3 &p0, const Vec3 &v0,
                          const Vec3 &p1, const Vec3 &v1,
                          long count)
{
    std::vector<Vec3> ret(count);
    ret[0] = p0;
    ret[count - 1] = p1;

    for (long i = 1; i < count - 1; ++i) {
        float u = static_cast<float>(i) / count;

        float u_p2 = std::pow(u, 2);
        float u_p3 = std::pow(u, 3);

        float a = 2 * u_p3 - 3 * u_p2 + 1;
        float b = -2 * u_p3 + 3 * u_p2;
        float c = u_p3 - 2 * u_p2 + u;
        float d = u_p3 - u_p2;

        ret[i] = a * p0 + b * p1 + c * v0 + d * v1;
    }

    return ret;
}

std::vector<Vec3> bezierBernstein(const std::vector<Vec3> &controlPoints,
                                  long count)
{
    int order = controlPoints.size();
    int degree = order - 1;

    std::vector<Vec3> ret(count);
    ret[0] = controlPoints[0];
    ret[count - 1] = controlPoints[degree];

    for (long i = 1; i < count - 1; ++i) {
        float u = static_cast<float>(i) / count;

        Vec3 p;

        for (int j = 0; j < order; ++j) {
            p += controlPoints[j] * bernstein(degree, j, u);
        }

        ret[i] = p;
    }

    return ret;
}

std::vector<Vec3> bezierCasteljau(const std::vector<Vec3> &controlPoints,
                                  long count)
{
    int order = controlPoints.size();
    int degree = order - 1;

    std::vector<Vec3> ret(count);
    ret[0] = controlPoints[0];
    ret[count - 1] = controlPoints[degree];

    for (long i = 1; i < count - 1; ++i) {
        float u = static_cast<float>(i) / count;

        Vec3 p = casteljau_recursive(u, 0, degree, controlPoints);

        ret[i] = p;
    }

    return ret;
}

} // namespace epwmath
