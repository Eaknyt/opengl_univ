#include "vec3.h"

#include <cmath>
#include <iostream>

#include "mat4.h"

namespace epwmath {

Vec3::Vec3() :
    m_x(0.),
    m_y(0.),
    m_z(0.)
{}

Vec3::Vec3(float x, float y, float z) :
    m_x(x),
    m_y(y),
    m_z(z)
{}

Vec3::Vec3(const Vec3 &other) :
    m_x(other.m_x),
    m_y(other.m_y),
    m_z(other.m_z)
{}

float Vec3::x() const
{
    return m_x;
}

void Vec3::setX(float x)
{
    if (m_x != x) {
        m_x = x;
    }
}

float Vec3::y() const
{
    return m_y;
}

void Vec3::setY(float y)
{
    if (m_y != y) {
        m_y = y;
    }
}

float Vec3::z() const
{
    return m_z;
}

void Vec3::setZ(float z)
{
    if (m_z != z) {
        m_z = z;
    }
}

float Vec3::length() const
{
    return std::sqrt(std::pow(m_x, 2) + std::pow(m_y, 2) + std::pow(m_z, 2));
}

void Vec3::normalize()
{
    float myLength = length();

    m_x /= myLength;
    m_y /= myLength;
    m_z /= myLength;
}

Vec3 Vec3::normalized() const
{
    Vec3 ret(*this);
    ret.normalize();
    
    return ret;
}

float Vec3::angle(const Vec3 &v) const
{
    float dotProd = dotProduct(v);

    float cosAngle = dotProd / (length() * v.length());

    return std::acos(cosAngle);
}

float Vec3::dotProduct(const Vec3 &other) const
{
    return m_x * other.m_x + m_y * other.m_y + m_z * other.m_z;
}

Vec3 Vec3::crossProduct(const Vec3 &other) const
{
    Vec3 ret(m_y * other.m_z - m_z * other.m_y,
             m_z * other.m_x - m_x * other.m_z,
             m_x * other.m_y - m_y * other.m_x);

    return ret;
}

Vec3 Vec3::normal(const Vec3 &other) const
{
    Vec3 ret = crossProduct(other).normalized();

    return ret;
}

Vec3 Vec3::normal(const Vec3 &v1, const Vec3 &v2, const Vec3 &v3)
{
    Vec3 ret = crossProduct((v2 - v1), (v3 - v1)).normalized();

    return ret;
}

float Vec3::dotProduct(const Vec3 &p1, const Vec3 &p2)
{
    return p1.dotProduct(p2);
}

Vec3 Vec3::crossProduct(const Vec3 &v1, const Vec3 &v2)
{
    return v1.crossProduct(v2);
}

float Vec3::angle(const Vec3 &v1, const Vec3 &v2)
{
    return v1.angle(v2);
}

Vec3 Vec3::projectOnLine(const Vec3 &p1, const Vec3 &p2) const
{
    Vec3 ba(m_x - p1.m_x,
            m_y - p1.m_y,
            m_z - p1.m_z);

    Vec3 bc(p2.m_x - p1.m_x,
            p2.m_y - p1.m_y,
            p2.m_z - p1.m_z);

    float bToRetLength = ba.dotProduct(bc) / bc.length();

    Vec3 u = bc.normalized();

    Vec3 ret(p1.m_x + u.x() * bToRetLength,
             p1.m_y + u.y() * bToRetLength,
             p1.m_z + u.z() * bToRetLength);

    return ret;
}

Vec3 Vec3::projectOnPlane(const Vec3 &a, const Vec3 &normal) const
{
    Vec3 ma(a.m_x - m_x,
            a.m_y - m_y,
            a.m_z - m_z);

    float nLength = normal.length();

    float mToRetLength = ma.dotProduct(normal) / nLength;

    Vec3 ret(m_x - normal.x() * mToRetLength,
             m_y - normal.y() * mToRetLength,
             m_z - normal.z() * mToRetLength);

    return ret;
}

Vec3 Vec3::unproject(const Mat4 &mv, const Mat4 &p, int width, int height) const
{
    const Mat4 mvp = p * mv;
    const Mat4 inverse = mvp.inverted();

    const float ndcX = 2.f * (x() / static_cast<float>(width)) - 1.f;
    const float ndcY = 1.f - 2.f * y() / static_cast<float>(height);
    const float ndcZ = 2.f * z() - 1.f;

    Vec4 worldCoords = inverse * Vec4(ndcX, ndcY, ndcZ, 1.f);
    worldCoords /= worldCoords.w();

    Vec3 ret(worldCoords.x(), worldCoords.y(), worldCoords.z());

    return ret;
}

const Vec3 operator*(const Vec3 &lhs, const Vec3 &rhs)
{
    Vec3 ret(lhs.x() * rhs.x(),
             lhs.y() * rhs.y(),
             lhs.z() * rhs.z());

    return ret;
}

const Vec3 operator+(const Vec3 &lhs, const Vec3 &rhs)
{
    Vec3 ret(lhs.x() + rhs.x(),
             lhs.y() + rhs.y(),
             lhs.z() + rhs.z());

    return ret;
}

Vec3 &Vec3::operator+=(const Vec3 &rhs)
{
    m_x += rhs.x();
    m_y += rhs.y();
    m_z += rhs.z();

    return *this;
}

const Vec3 operator-(const Vec3 &lhs, const Vec3 &rhs)
{
    Vec3 ret(lhs.x() - rhs.x(),
             lhs.y() - rhs.y(),
             lhs.z() - rhs.z());

    return ret;
}

Vec3 &Vec3::operator-=(const Vec3 &rhs)
{
    m_x -= rhs.x();
    m_y -= rhs.y();
    m_z -= rhs.z();

    return *this;
}

Vec3 &Vec3::operator*=(float factor)
{
    m_x *= factor;
    m_y *= factor;
    m_z *= factor;

    return *this;
}

const Vec3 operator/(const Vec3 &lhs, float rhs)
{
    Vec3 ret(lhs.x() / rhs,
             lhs.y() / rhs,
             lhs.z() / rhs);

    return ret;
}

Vec3 &Vec3::operator/=(float factor)
{
    m_x /= factor;
    m_y /= factor;
    m_z /= factor;

    return *this;
}

const Vec3 operator-(const Vec3 &v)
{
    return Vec3(-v.m_x, -v.m_y, -v.m_z);
}

const Vec3 operator*(float factor, const Vec3 &v)
{
    Vec3 ret(v.x() * factor,
             v.y() * factor,
             v.z() * factor);

    return ret;
}

const Vec3 operator*(const Vec3 &v, float factor)
{
    return factor * v;
}

bool operator==(const Vec3 &lhs, const Vec3 &rhs)
{
    return lhs.m_x == rhs.m_x && lhs.m_y == rhs.m_y && lhs.m_z == rhs.m_z;
}

bool operator!=(const Vec3 &lhs, const Vec3 &rhs)
{
    return !(lhs == rhs);
}

std::ostream &operator<<(std::ostream &os, const Vec3 &v)
{
    os << "epwmath::Vec3(" +
          std::to_string(v.m_x) + ", " +
          std::to_string(v.m_y) + ", " +
          std::to_string(v.m_z) +
          ")";

    return os;
}

} // namespace epwmath
