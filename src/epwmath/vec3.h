#ifndef H_VEC3_H
#define H_VEC3_H

#include <iosfwd>

namespace epwmath {

class Mat4;

class Vec3
{
public:
    Vec3();
    Vec3(float x, float y, float z);
    Vec3(const Vec3 &other);

    float x() const;
    void setX(float x);
    
    float y() const;
    void setY(float y);
    
    float z() const;
    void setZ(float z);

    float length() const;

    void normalize();
    Vec3 normalized() const;

    float angle(const Vec3 &v) const;

    float dotProduct(const Vec3 &other) const;
    Vec3 crossProduct(const Vec3 &other) const;

    Vec3 normal(const Vec3 &other) const;
    static Vec3 normal(const Vec3 &v1, const Vec3 &v2, const Vec3 &v3);

    static float dotProduct(const Vec3 &v1, const Vec3 &v2);
    static Vec3 crossProduct(const Vec3 &v1, const Vec3 &v2);
    static float angle(const Vec3 &v1, const Vec3 &v2);

    Vec3 projectOnLine(const Vec3 &p1, const Vec3 &p2) const;
    Vec3 projectOnPlane(const Vec3 &a, const Vec3 &normal) const;

    Vec3 unproject(const Mat4 &mv, const Mat4 &p, int width, int height) const;

    friend const Vec3 operator+(const Vec3 &rhs, const Vec3 &lhs);
    Vec3 &operator+=(const Vec3 &rhs);

    friend const Vec3 operator-(const Vec3 &lhs, const Vec3 &rhs);
    Vec3 &operator-=(const Vec3 &rhs);

    friend const Vec3 operator-(const Vec3 &v);

    friend const Vec3 operator*(float factor, const Vec3 &v);
    friend const Vec3 operator*(const Vec3 &v, float factor);

    friend const Vec3 operator*(const Vec3 &lhs, const Vec3 &rhs);

    Vec3 &operator*=(float factor);

    friend const Vec3 operator/(const Vec3 &lhs, float rhs);
    Vec3 &operator/=(float factor);

    friend bool operator==(const Vec3 &lhs, const Vec3 &rhs);
    friend bool operator!=(const Vec3 &lhs, const Vec3 &rhs);
   
    friend std::ostream &operator<<(std::ostream &os, const Vec3 &v);

private:
    float m_x;
    float m_y;
    float m_z;
};

} // namespace epwmath

#endif // H_VEC3_H
