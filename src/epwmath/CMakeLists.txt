project(libepwmath)

SET(SOURCES
    units.cpp
    vec2.cpp
    vec3.cpp
    vec4.cpp
    line2.cpp
    mat2.cpp
    mat3.cpp
    mat4.cpp
    quaternion.cpp
    curves.cpp
    surfaces.cpp
    boundingbox.cpp
    octree.cpp
    transform.cpp
    camera.cpp)

SET(HEADERS
    units.h
    vec2.h
    vec3.h
    vec4.h
    line2.h
    mat2.h
    mat3.h
    mat4.h
    quaternion.h
    curves.h
    surfaces.h
    boundingbox.h
    octree.h
    transform.h
    camera.h)

add_library(epwmath SHARED ${SOURCES} ${HEADERS})
