#ifndef H_EPWMATH_LINE2_H
#define H_EPWMATH_LINE2_H

#include <iosfwd>
#include <utility>

#include "vec2.h"

namespace epwmath {

class Line2
{
public:
	enum IntersectType
	{
		NoIntersect,
        OnePointIntersect,
        InfiniteIntersect
	};

	Line2();
    Line2(float x1, float y1, float x2, float y2);
    Line2(const Vec2 &p1, const Vec2 &p2);
	Line2(const Line2 &other);

    Vec2 p1() const;
    void setP1(const Vec2 &p);

    Vec2 p2() const;
    void setP2(const Vec2 &p);

    bool hasPoint(const Vec2 &p) const;

    IntersectType intersects(const Line2 &other, Vec2 &p);

    friend std::ostream &operator<<(std::ostream &os, const Line2 &line);

private:
    Vec2 m_p1;
    Vec2 m_p2;
};

} // namespace epwmath

#endif // H_EPWMATH_LINE2_H
