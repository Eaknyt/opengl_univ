#ifndef H_EPWMATH_VEC4_H
#define H_EPWMATH_VEC4_H

#include <iosfwd>

namespace epwmath {

class Vec4
{
public:
    Vec4();
    Vec4(float x, float y, float z, float w);
    Vec4(const Vec4 &other);

    float x() const;
    void setX(float x);

    float y() const;
    void setY(float y);

    float z() const;
    void setZ(float z);

    float w() const;
    void setW(float w);

    float length() const;

    void normalize();
    Vec4 normalized() const;

    float dotProduct(const Vec4 &other) const;

    friend const Vec4 operator+(const Vec4 &rhs, const Vec4 &lhs);
    Vec4 &operator+=(const Vec4 &rhs);

    friend const Vec4 operator-(const Vec4 &lhs, const Vec4 &rhs);
    Vec4 &operator-=(const Vec4 &rhs);

    friend const Vec4 operator-(const Vec4 &v);

    friend const Vec4 operator*(float factor, const Vec4 &v);
    friend const Vec4 operator*(const Vec4 &v, float factor);

    friend const Vec4 operator*(const Vec4 &lhs, const Vec4 &rhs);

    Vec4 &operator*=(float factor);

    friend const Vec4 operator/(const Vec4 &lhs, float rhs);
    Vec4 &operator/=(float factor);

    friend bool operator==(const Vec4 &lhs, const Vec4 &rhs);
    friend bool operator!=(const Vec4 &lhs, const Vec4 &rhs);

    friend std::ostream &operator<<(std::ostream &os, const Vec4 &v);

private:
    float m_x;
    float m_y;
    float m_z;
    float m_w;
};

} // namespace epwmath

#endif // H_EPWMATH_VEC4_H
