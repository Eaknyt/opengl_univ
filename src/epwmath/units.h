#ifndef H_EPWMATH_UNITS_H
#define H_EPWMATH_UNITS_H

namespace epwmath {

float radToDeg(float rad);
float degToRad(float degrees);

} // namespace epwmath

 #endif // H_EPWMATH_UNITS_H
