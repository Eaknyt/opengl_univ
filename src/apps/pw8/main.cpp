#include <assert.h>
#include <iostream>

#include <QtCore/QDebug>

#include <QtGui/QKeyEvent>

#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QSplitter>
#include <QtWidgets/QTabWidget>

// Own includes
#include "epwmath/segmentation.h"

#include "epwgl/geometrydrawer.h"
#include "epwgl/helpers.h"
#include "epwgl/offloader.h"
#include "epwgl/renderer.h"
#include "epwgl/scene.h"
#include "epwgl/sceneviewer.h"


using namespace epwgl;
using namespace epwmath;


////////////////////// MainWindow //////////////////////

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);

    void setBackfaceCulling(bool cull);

protected:
    void keyPressEvent(QKeyEvent *e);

private Q_SLOTS:
    void segmentMesh();

private:
    Scene *m_scene;

    Renderer *m_renderer;

    SceneViewer *m_viewer;

    Geometry *m_mesh;
    GeometryDrawer *m_meshDrawer;

    QHBoxLayout *m_controlsFrameLayout;
};

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    m_scene(new Scene(this)),
    m_renderer(new Renderer),
    m_viewer(nullptr),
    m_mesh(nullptr),
    m_meshDrawer(nullptr),
    m_controlsFrameLayout(nullptr)
{
    m_renderer->setScene(m_scene);

    auto myCentralWidget = new QWidget(this);
    setCentralWidget(myCentralWidget);

    auto myLayout = new QVBoxLayout(myCentralWidget);

    // Build the controls bar
    auto controlsFrame = new QFrame(myCentralWidget);
    controlsFrame->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);

    //  The button to segment the current mesh
    auto segmMeshPushButton = new QPushButton("Segment mesh", controlsFrame);

    connect(segmMeshPushButton, &QPushButton::clicked,
            this, &MainWindow::segmentMesh);

    //  A check box for enabling backface culling or not
    auto cullBackFacesCheckBox = new QCheckBox(controlsFrame);
    cullBackFacesCheckBox->setText("Enable backface culling");
    cullBackFacesCheckBox->setChecked(false);

    //  A combo box for setting the render mode
    auto renderModeCBox = new QComboBox(controlsFrame);
    renderModeCBox->addItem("Solid");
    renderModeCBox->addItem("Wireframe");
    renderModeCBox->addItem("Points");

    renderModeCBox->setItemData(0, static_cast<int>(SceneViewer::RenderMode::Solid));
    renderModeCBox->setItemData(1, static_cast<int>(SceneViewer::RenderMode::Wireframe));
    renderModeCBox->setItemData(2, static_cast<int>(SceneViewer::RenderMode::Points));

    m_controlsFrameLayout = new QHBoxLayout(controlsFrame);
    m_controlsFrameLayout->addWidget(segmMeshPushButton);
    m_controlsFrameLayout->addWidget(cullBackFacesCheckBox);
    m_controlsFrameLayout->addWidget(new QLabel("Render Mode"));
    m_controlsFrameLayout->addWidget(renderModeCBox);
    m_controlsFrameLayout->addStretch();

    // Build viewers and asset browser
    auto splitter = new QSplitter(myCentralWidget);
    splitter->setHandleWidth(15);

    m_viewer = new SceneViewer(splitter);
    m_viewer->setScene(m_scene);
    m_viewer->setRenderer(m_renderer);

    splitter->addWidget(m_viewer);

    myLayout->addWidget(controlsFrame);
    myLayout->addWidget(splitter);

    // S/S connections
    connect(cullBackFacesCheckBox, &QCheckBox::toggled,
            this, &MainWindow::setBackfaceCulling);

    connect(renderModeCBox, static_cast<void (QComboBox::*) (int)>(&QComboBox::currentIndexChanged),
            [=] (int index) {
        m_viewer->setRenderMode(Viewer::RenderMode(renderModeCBox->itemData(index).toInt()));
    });

    // Populate scene
    Geometry *geom = readOff("res/off/mesh_segm.off");
    auto drawer = new GeometryDrawer(geom);

    m_scene->addDrawer(drawer);
    m_viewer->centerScene();
}

void MainWindow::setBackfaceCulling(bool cull)
{
    m_renderer->setCullBackfaces(cull);

    m_viewer->update();
}

void MainWindow::keyPressEvent(QKeyEvent *e)
{
    const int key = e->key();

    if (key == Qt::Key_C) {
        m_viewer->centerScene();
    }
}

void MainWindow::segmentMesh()
{

}


/////////////////////// Main ///////////////////////

int main(int argc, char **argv)
{
    QApplication app(argc, argv);

    MainWindow mw;
    mw.show();

    return app.exec();
}

#include "main.moc"
