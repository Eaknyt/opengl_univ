#include <iostream>

#include <QtCore/QDebug>

#include <QtGui/QKeyEvent>

#include <QtWidgets/QApplication>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QSplitter>

// Own includes
#include "epwmath/camera.h"

#include "epwgl/color.h"
#include "epwgl/drawhelpers.h"
#include "epwgl/quadrics.h"

#include "editorlib/viewer.h"

using namespace epwgl;
using namespace epwmath;


/////////////////////// Pw4Viewer ///////////////////////

class Pw4Viewer : public Viewer
{
public:
    Pw4Viewer(QWidget *parent = nullptr);

protected:
    void render() override;

    void keyPressEvent(QKeyEvent *e) override;

private:
    Geometry m_geometry;
    int m_drawMode;
    int m_sphereDef;
};

Pw4Viewer::Pw4Viewer(QWidget *parent) :
    Viewer(parent),
    m_geometry(),
    m_drawMode(GL_QUADS),
    m_sphereDef(15)
{
    setFocus();

    m_camera->setPos({50, 0, 0});
    m_camera->setTarget({5, 0, 5});

    m_geometry = cylinder(100, 10, 20);
}

void Pw4Viewer::keyPressEvent(QKeyEvent *e)
{
    const int keyId = e->key();

    if (keyId == Qt::Key_S) {
        m_geometry = sphere(m_sphereDef, m_sphereDef, 20);

        m_drawMode = GL_QUADS;

        update();
    }
    else if (keyId == Qt::Key_Y) {
        m_geometry = cylinder(100, 10, 20);

        m_drawMode = GL_QUADS;

        update();
    }
    else if (keyId == Qt::Key_C) {
        m_geometry = cone(30, {5, 0, 5}, 10, 20);

        m_drawMode = GL_TRIANGLES;

        update();
    }
    else if (keyId == Qt::Key_Plus) {
        m_sphereDef++;
        m_geometry = sphere(m_sphereDef, m_sphereDef, 20);

        update();
    }
}

void Pw4Viewer::render()
{
    eglColor(GlobalColor::Red);

    eglDrawGeometry(m_geometry, m_drawMode);
}


/////////////////////// Main ///////////////////////

int main(int argc, char **argv)
{
    QApplication app(argc, argv);

    QMainWindow mw;

    QSplitter splitter(&mw);
    splitter.setHandleWidth(15);

    auto viewer = new Pw4Viewer(&splitter);
    splitter.addWidget(viewer);

    // Go
    mw.setCentralWidget(&splitter);
    mw.show();

    return app.exec();
}
