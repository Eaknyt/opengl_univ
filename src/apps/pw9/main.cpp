#include <assert.h>
#include <iostream>

#include <QtCore/QDebug>

#include <QtGui/QKeyEvent>

#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QSplitter>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QToolBar>

// Own includes
#include "epwgl/scene/renderable.h"
#include "epwgl/scene/renderer.h"
#include "epwgl/scene/scene.h"

#include "epwgl/geometrydrawer.h"
#include "epwgl/offloader.h"
#include "epwgl/subdiv.h"

#include "editorlib/assetbrowser.h"
#include "editorlib/sceneoutlineview.h"
#include "editorlib/sceneviewer.h"
#include "editorlib/viewercontrols.h"

using namespace epwgl;
using namespace epwmath;


////////////////////// MainWindow //////////////////////

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);

private Q_SLOTS:
    void updateSubdivControls();
    void subdivCurrentMesh();
    void setFacesCulling(bool enable);

private:
    void loadOff(const QString &fileName);

private:
    Scene *m_scene;

    Renderer *m_renderer;

    SceneViewer *m_viewer;

    Renderable *m_currentMesh;

    SceneOutlineView *m_sceneOutlineView;
    AssetBrowser *m_assetBrowser;

    QPushButton *m_subdivBtn;
};

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    m_scene(new Scene(this)),
    m_renderer(new Renderer),
    m_viewer(nullptr),
    m_currentMesh(nullptr),
    m_sceneOutlineView(nullptr),
    m_assetBrowser(nullptr),
    m_subdivBtn(nullptr)
{
    m_renderer->setScene(m_scene);
    m_renderer->setRenderMode(Renderer::RenderMode::Wireframe);

    auto myCentralWidget = new QWidget(this);
    setCentralWidget(myCentralWidget);

    auto myLayout = new QVBoxLayout(myCentralWidget);

    auto splitter = new QSplitter(myCentralWidget);
    splitter->setHandleWidth(15);

    auto rightPaneSplitter = new QSplitter(splitter);
    rightPaneSplitter->setHandleWidth(15);
    rightPaneSplitter->setOrientation(Qt::Vertical);

    //  Outline view
    m_sceneOutlineView = new SceneOutlineView(this);
    m_sceneOutlineView->setScene(m_scene);

    auto outlineViewTab = new QTabWidget(rightPaneSplitter);
    outlineViewTab->addTab(m_sceneOutlineView, "Scene Outline");

    //  Asset browser
    m_assetBrowser = new AssetBrowser(this);
    auto assetBrowserTab = new QTabWidget(rightPaneSplitter);
    assetBrowserTab->addTab(m_assetBrowser, "Asset Browser");

    //  Viewer
    m_viewer = new SceneViewer(splitter);
    m_viewer->setScene(m_scene);
    m_viewer->setRenderer(m_renderer);

    auto viewerLayout = new QVBoxLayout(m_viewer);
    viewerLayout->addWidget(new ViewerControls(m_viewer));
    viewerLayout->addStretch();

    // Controls
    auto toolbar = new QToolBar(this);
    addToolBar(Qt::TopToolBarArea, toolbar);

    auto faceCullingCheckbox = new QCheckBox(this);
    faceCullingCheckbox->setText("Enable face culling");

    connect(faceCullingCheckbox, &QCheckBox::toggled,
            this, &MainWindow::setFacesCulling);

    m_subdivBtn = new QPushButton("Subdiv", toolbar);
    m_subdivBtn->setDisabled(true);

    connect(m_subdivBtn, &QPushButton::clicked,
            this, &MainWindow::subdivCurrentMesh);

    toolbar->addWidget(faceCullingCheckbox);
    toolbar->addSeparator();
    toolbar->addWidget(m_subdivBtn);

    // Assemble all widgets
    rightPaneSplitter->addWidget(outlineViewTab);
    rightPaneSplitter->addWidget(assetBrowserTab);

    splitter->addWidget(m_viewer);
    splitter->addWidget(rightPaneSplitter);

    myLayout->addWidget(splitter);

    // S/S connections
    connect(m_assetBrowser, &AssetBrowser::offSelected,
            this, &MainWindow::loadOff);
}

void MainWindow::updateSubdivControls()
{
    m_subdivBtn->setEnabled(m_scene->size() != 0);
}

void MainWindow::subdivCurrentMesh()
{
    if (!m_currentMesh) {
        return;
    }

    subdivPolyhedral(m_currentMesh->mesh()->geometry());

    Q_EMIT m_scene->sceneChanged();
}

void MainWindow::setFacesCulling(bool enable)
{
    m_renderer->setCullBackfaces(enable);

    m_viewer->update();
}

void MainWindow::loadOff(const QString &fileName)
{
    m_scene->clear();

    Geometry *geom = readOff("res/off/" + fileName.toStdString());

    m_currentMesh = new Renderable;
    m_currentMesh->mesh()->setDrawMode(GeometryDrawer::Triangles);
    m_currentMesh->mesh()->setGeometry(geom);

    m_scene->addRenderable(m_currentMesh);

    updateSubdivControls();
}


/////////////////////// Main ///////////////////////

int main(int argc, char **argv)
{
    QApplication app(argc, argv);

    MainWindow mw;
    mw.show();

    return app.exec();
}

#include "main.moc"
