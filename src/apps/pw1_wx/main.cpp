#include <iostream>
#include <memory>

#include <wx/wxprec.h>
#ifndef WX_PRECOMP
    #include <wx/wx.h>
    #include <wx/glcanvas.h>
    #include <wx/sizer.h>
    #include <wx/spinctrl.h>
#endif

// Own includes
#include "epwgl/color.h"
#include "epwgl/drawhelpers.h"

#include "epwmath/vec3.h"


using namespace epwgl;
using namespace epwmath;


/////////////////////// GlCanvas ///////////////////////

class GlCanvas : public wxGLCanvas
{ 
public:
    GlCanvas(wxWindow *parent, int *args = nullptr);
 
    void OnPaint(wxPaintEvent &e);
    void OnSize(wxSizeEvent &e);

    void setPointToProject(const Vec3 &p);

    wxDECLARE_EVENT_TABLE();

private:
    void initGL();
    void render();

private:
    std::unique_ptr<wxGLContext> m_context;
    bool m_glIsInitialized;

    Vec3 m_pointToProject;
};

wxBEGIN_EVENT_TABLE(GlCanvas, wxGLCanvas)
    EVT_PAINT(GlCanvas::OnPaint)
    EVT_SIZE(GlCanvas::OnSize)
wxEND_EVENT_TABLE()

GlCanvas::GlCanvas(wxWindow *parent, int *args) :
    wxGLCanvas(parent, wxID_ANY, args, wxDefaultPosition, wxDefaultSize,
               wxFULL_REPAINT_ON_RESIZE),
    m_context(std::make_unique<wxGLContext>(this)),
    m_glIsInitialized(false)
{
    SetBackgroundStyle(wxBG_STYLE_CUSTOM);
}

void GlCanvas::OnPaint(wxPaintEvent &e)
{
    wxPaintDC(this);
    SetCurrent(*m_context);
    {
        glClear(GL_COLOR_BUFFER_BIT);
        glLoadIdentity();

        if (!m_glIsInitialized) {
            initGL();
        }

        render();

        glFlush();
    }
    SwapBuffers();
}

void GlCanvas::OnSize(wxSizeEvent &e)
{
    glViewport(0, 0, e.GetSize().GetWidth(), e.GetSize().GetHeight());

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(-2.0, 2.0, -2.0, 2.0, -2.0, 2.0);

    glMatrixMode(GL_MODELVIEW);

    Refresh();
}

void GlCanvas::setPointToProject(const Vec3 &p)
{
    m_pointToProject = p;

    Refresh();
}

void GlCanvas::initGL()
{
    eglClearColor(GlobalColor::Black);

    m_glIsInitialized = true;
}

void GlCanvas::render()
{
    eglColor(GlobalColor::Cyan);

    // Draw the line
    Vec3 lp1(2, 1.3, 0.2);
    Vec3 lp2(0.5, 0, 1);

    eglDrawLine(lp1, lp2);

    // Draw a point outside the line
    glPointSize(6);

    eglColor(GlobalColor::XAxis);
    eglDrawPoint(m_pointToProject);

    // Draw the projection of p1 on the line
    Vec3 pp = m_pointToProject.projectOnLine(lp1, lp2);

    eglColor(GlobalColor::YAxis);
    eglDrawPoint(pp);
}


/////////////////////// MainWindow ///////////////////////

class MainWindow: public wxFrame
{
public:
    MainWindow();

private:
    void OnExit(wxCommandEvent &);

private:
};

MainWindow::MainWindow() :
    wxFrame(nullptr, wxID_ANY, "Programmation 3D - TP1")
{
    // Menu bar
    auto menuFile = new wxMenu;
    menuFile->Append(wxID_EXIT);

    auto menuBar = new wxMenuBar;
    menuBar->Append(menuFile, "&File");

    SetMenuBar(menuBar);

    Bind(wxEVT_MENU, &MainWindow::OnExit, this, wxID_EXIT);

    // Child widgets
    //  GL Canvas
    int args[] = { WX_GL_RGBA, WX_GL_DOUBLEBUFFER, WX_GL_DEPTH_SIZE, 16, 0 };
    auto canvas = new GlCanvas(this, args);

    //  Right panel
    auto rightPanel = new wxPanel(this);
    auto rightPanelSizer = new wxBoxSizer(wxVERTICAL);
    rightPanel->SetSizer(rightPanelSizer);

    auto xSpinBox = new wxSpinCtrlDouble(rightPanel);
    xSpinBox->SetDigits(3);
    xSpinBox->SetIncrement(0.1);
    rightPanelSizer->Add(xSpinBox, 1, wxEXPAND);

    auto ySpinBox = new wxSpinCtrlDouble(rightPanel);
    ySpinBox->SetDigits(3);
    ySpinBox->SetIncrement(0.1f);
    rightPanelSizer->Add(ySpinBox, 1, wxEXPAND);

    auto zSpinBox = new wxSpinCtrlDouble(rightPanel);
    zSpinBox->SetDigits(3);
    zSpinBox->SetIncrement(0.1f);
    rightPanelSizer->Add(zSpinBox, 1, wxEXPAND);

    xSpinBox->Bind(wxEVT_COMMAND_SPINCTRLDOUBLE_UPDATED, [=] (wxSpinDoubleEvent &e) {
        canvas->setPointToProject(Vec3(e.GetValue(),
                                         ySpinBox->GetValue(),
                                         zSpinBox->GetValue()));
    });

    ySpinBox->Bind(wxEVT_COMMAND_SPINCTRLDOUBLE_UPDATED, [=] (wxSpinDoubleEvent &e) {
        canvas->setPointToProject(Vec3(xSpinBox->GetValue(),
                                         e.GetValue(),
                                         zSpinBox->GetValue()));
    });

    zSpinBox->Bind(wxEVT_COMMAND_SPINCTRLDOUBLE_UPDATED, [=] (wxSpinDoubleEvent &e) {
        canvas->setPointToProject(Vec3(xSpinBox->GetValue(),
                                         ySpinBox->GetValue(),
                                         e.GetValue()));
    });

    // Self sizer
    auto sizer = new wxBoxSizer(wxHORIZONTAL);
    sizer->Add(canvas, 1, wxEXPAND);
    sizer->Add(rightPanel, 1);

    SetSizer(sizer);
    SetAutoLayout(true);
}

void MainWindow::OnExit(wxCommandEvent &)
{
    Close(true);
}


/////////////////////// MyApp ///////////////////////

class MyApp: public wxApp
{
public:
    virtual bool OnInit();
};

bool MyApp::OnInit()
{
    auto mw = new MainWindow();
    mw->Show();

    return true;
}

wxIMPLEMENT_APP(MyApp);
