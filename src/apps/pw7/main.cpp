#include <assert.h>
#include <iostream>

#include <QtCore/QDebug>

#include <QtGui/QKeyEvent>

#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QSplitter>
#include <QtWidgets/QTabWidget>

// Own includes
#include "epwmath/geometry.h"
#include "epwmath/segmentation.h"

#include "epwgl/geometrydrawer.h"
#include "epwgl/geometrymeshes.h"
#include "epwgl/helpers.h"
#include "epwgl/renderer.h"
#include "epwgl/scene.h"
#include "epwgl/sceneviewer.h"


using namespace epwgl;
using namespace epwmath;


////////////////////// MainWindow //////////////////////

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    enum class MeshType
    {
        Cylinder,
        Sphere
    };

    explicit MainWindow(QWidget *parent = nullptr);

    void setMesh(MeshType meshType);

    void setBackfaceCulling(bool cull);

protected:
    void keyPressEvent(QKeyEvent *e);

private Q_SLOTS:
    void updateSharpEdges();

private:
    Geometry *geometryForMeshType(MeshType meshType) const;

    void updateMesh();

private:
    Scene *m_scene;

    Renderer *m_renderer;

    SceneViewer *m_viewer;

    MeshType m_meshType;
    Geometry *m_mesh;

    GeometryDrawer *m_meshDrawer;
    GeometryDrawer *m_sharpEdgesDrawer;

    QHBoxLayout *m_controlsFrameLayout;

    QWidget *m_cylinderControlsWidget;
    QSpinBox *m_cylMeridiansSpinBox;
    QSpinBox *m_cylRadiusSpinBox;
    QSpinBox *m_cylHeightSpinBox;

    QWidget *m_sphereControlsWidget;
    QSpinBox *m_sphereMeridiansSpinBox;
    QSpinBox *m_sphereParallelsSpinBox;
    QSpinBox *m_sphereRadiusSpinBox;

    QSpinBox *m_sharpEdgesAngleSpinBox;
};

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    m_scene(new Scene(this)),
    m_renderer(new Renderer),
    m_viewer(nullptr),
    m_meshType(MeshType::Cylinder),
    m_mesh(nullptr),
    m_meshDrawer(nullptr),
    m_sharpEdgesDrawer(nullptr),
    m_controlsFrameLayout(nullptr),
    m_cylinderControlsWidget(nullptr),
    m_cylMeridiansSpinBox(nullptr),
    m_cylRadiusSpinBox(nullptr),
    m_cylHeightSpinBox(nullptr),
    m_sphereControlsWidget(nullptr),
    m_sphereMeridiansSpinBox(nullptr),
    m_sphereParallelsSpinBox(nullptr),
    m_sphereRadiusSpinBox(nullptr),
    m_sharpEdgesAngleSpinBox(nullptr)
{
    m_renderer->setScene(m_scene);

    auto myCentralWidget = new QWidget(this);
    setCentralWidget(myCentralWidget);

    auto myLayout = new QVBoxLayout(myCentralWidget);

    // Build the controls bar
    auto controlsFrame = new QFrame(myCentralWidget);
    controlsFrame->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);

    //  A combo box for choosing the mesh
    auto meshTypeCBox = new QComboBox(controlsFrame);
    meshTypeCBox->addItem("Cylinder Mesh");
    meshTypeCBox->addItem("Sphere Mesh");

    meshTypeCBox->setItemData(0, static_cast<int>(MeshType::Cylinder));
    meshTypeCBox->setItemData(1, static_cast<int>(MeshType::Sphere));

    //  Controls for the cylinder
    m_cylinderControlsWidget = new QWidget(controlsFrame);

    m_cylMeridiansSpinBox = new QSpinBox(m_cylinderControlsWidget);
    m_cylMeridiansSpinBox->setValue(20);

    connect(m_cylMeridiansSpinBox, static_cast<void (QSpinBox::*) (int)>(&QSpinBox::valueChanged),
            this, &MainWindow::updateMesh);

    m_cylRadiusSpinBox = new QSpinBox(m_cylinderControlsWidget);
    m_cylRadiusSpinBox->setValue(10);

    connect(m_cylRadiusSpinBox, static_cast<void (QSpinBox::*) (int)>(&QSpinBox::valueChanged),
            this, &MainWindow::updateMesh);

    m_cylHeightSpinBox = new QSpinBox(m_cylinderControlsWidget);
    m_cylHeightSpinBox->setValue(20);

    connect(m_cylHeightSpinBox, static_cast<void (QSpinBox::*) (int)>(&QSpinBox::valueChanged),
            this, &MainWindow::updateMesh);

    auto cylinderControlsLayout = new QHBoxLayout(m_cylinderControlsWidget);
    cylinderControlsLayout->addWidget(new QLabel("Meridians", m_cylinderControlsWidget));
    cylinderControlsLayout->addWidget(m_cylMeridiansSpinBox);
    cylinderControlsLayout->addWidget(new QLabel("Radius", m_cylinderControlsWidget));
    cylinderControlsLayout->addWidget(m_cylRadiusSpinBox);
    cylinderControlsLayout->addWidget(new QLabel("Height", m_cylinderControlsWidget));
    cylinderControlsLayout->addWidget(m_cylHeightSpinBox);

    //  Controls for the sphere
    m_sphereControlsWidget = new QWidget(controlsFrame);
    m_sphereControlsWidget->setVisible(false);

    m_sphereMeridiansSpinBox = new QSpinBox(m_sphereControlsWidget);
    m_sphereMeridiansSpinBox->setValue(20);

    connect(m_sphereMeridiansSpinBox, static_cast<void (QSpinBox::*) (int)>(&QSpinBox::valueChanged),
            this, &MainWindow::updateMesh);

    m_sphereParallelsSpinBox = new QSpinBox(m_sphereControlsWidget);
    m_sphereParallelsSpinBox->setValue(20);

    connect(m_sphereParallelsSpinBox, static_cast<void (QSpinBox::*) (int)>(&QSpinBox::valueChanged),
            this, &MainWindow::updateMesh);

    m_sphereRadiusSpinBox = new QSpinBox(m_sphereControlsWidget);
    m_sphereRadiusSpinBox->setValue(8);

    connect(m_sphereRadiusSpinBox, static_cast<void (QSpinBox::*) (int)>(&QSpinBox::valueChanged),
            this, &MainWindow::updateMesh);

    auto sphereControlsLayout = new QHBoxLayout(m_sphereControlsWidget);
    sphereControlsLayout->addWidget(new QLabel("Meridians", m_sphereControlsWidget));
    sphereControlsLayout->addWidget(m_sphereMeridiansSpinBox);
    sphereControlsLayout->addWidget(new QLabel("Parallels", m_sphereControlsWidget));
    sphereControlsLayout->addWidget(m_sphereParallelsSpinBox);
    sphereControlsLayout->addWidget(new QLabel("Radius", m_sphereControlsWidget));
    sphereControlsLayout->addWidget(m_sphereRadiusSpinBox);

    //  A spinbox to show sharp edges using a given angle
    m_sharpEdgesAngleSpinBox = new QSpinBox(controlsFrame);
    m_sharpEdgesAngleSpinBox->setMaximum(360);

    connect(m_sharpEdgesAngleSpinBox, static_cast<void (QSpinBox::*) (int)>(&QSpinBox::valueChanged),
            this, &MainWindow::updateSharpEdges);

    //  A check box for enabling backface culling or not
    auto cullBackFacesCheckBox = new QCheckBox(controlsFrame);
    cullBackFacesCheckBox->setText("Enable backface culling");
    cullBackFacesCheckBox->setChecked(false);

    //  A combo box for setting the render mode
    auto renderModeCBox = new QComboBox(controlsFrame);
    renderModeCBox->addItem("Solid");
    renderModeCBox->addItem("Wireframe");
    renderModeCBox->addItem("Points");

    renderModeCBox->setItemData(0, static_cast<int>(SceneViewer::RenderMode::Solid));
    renderModeCBox->setItemData(1, static_cast<int>(SceneViewer::RenderMode::Wireframe));
    renderModeCBox->setItemData(2, static_cast<int>(SceneViewer::RenderMode::Points));

    m_controlsFrameLayout = new QHBoxLayout(controlsFrame);
    m_controlsFrameLayout->addWidget(new QLabel("Mesh"));
    m_controlsFrameLayout->addWidget(meshTypeCBox);
    m_controlsFrameLayout->addWidget(new QLabel("Sharp edges filter"));
    m_controlsFrameLayout->addWidget(m_sharpEdgesAngleSpinBox);
    m_controlsFrameLayout->addWidget(cullBackFacesCheckBox);
    m_controlsFrameLayout->addWidget(new QLabel("Render Mode"));
    m_controlsFrameLayout->addWidget(renderModeCBox);
    m_controlsFrameLayout->addStretch();

    // Build viewers and asset browser
    auto splitter = new QSplitter(myCentralWidget);
    splitter->setHandleWidth(15);

    m_viewer = new SceneViewer(splitter);
    m_viewer->setScene(m_scene);
    m_viewer->setRenderer(m_renderer);

    splitter->addWidget(m_viewer);

    myLayout->addWidget(controlsFrame);
    myLayout->addWidget(splitter);

    // S/S connections
    QObject::connect(meshTypeCBox, static_cast<void (QComboBox::*) (int)>(&QComboBox::currentIndexChanged),
                     [=] (int index) {
        setMesh(MeshType(meshTypeCBox->itemData(index).toInt()));
    });

    connect(cullBackFacesCheckBox, &QCheckBox::toggled,
            this, &MainWindow::setBackfaceCulling);

    connect(renderModeCBox, static_cast<void (QComboBox::*) (int)>(&QComboBox::currentIndexChanged),
            [=] (int index) {
        m_viewer->setRenderMode(Viewer::RenderMode(renderModeCBox->itemData(index).toInt()));
    });


    updateMesh();
}

void MainWindow::updateMesh()
{
    Geometry *geom = geometryForMeshType(m_meshType);

    if (m_meshType == MeshType::Cylinder) {
        if (m_controlsFrameLayout->itemAt(2)->widget() == m_sphereControlsWidget) {
            m_controlsFrameLayout->removeWidget(m_sphereControlsWidget);

            m_sphereControlsWidget->setVisible(false);
            m_cylinderControlsWidget->setVisible(true);
        }

        m_controlsFrameLayout->insertWidget(2, m_cylinderControlsWidget);
    }
    else if (m_meshType == MeshType::Sphere) {
        if (m_controlsFrameLayout->itemAt(2)->widget() == m_cylinderControlsWidget) {
            m_controlsFrameLayout->removeWidget(m_cylinderControlsWidget);

            m_cylinderControlsWidget->setVisible(false);
            m_sphereControlsWidget->setVisible(true);
        }

        m_controlsFrameLayout->insertWidget(2, m_sphereControlsWidget);
    }

    assert (geom);

    m_mesh = geom;

    if (!m_meshDrawer) {
        m_meshDrawer = new GeometryDrawer(geom, GeometryDrawer::Triangles);

        m_scene->addDrawer(m_meshDrawer);
    }

    m_meshDrawer->setGeometry(m_mesh);

    updateSharpEdges();
}

void MainWindow::setMesh(MeshType meshType)
{
    if (m_meshType != meshType) {
        m_meshType = meshType;

        updateMesh();

        m_viewer->centerScene();
    }
}

void MainWindow::setBackfaceCulling(bool cull)
{
    m_renderer->setCullBackfaces(cull);

    m_viewer->update();
}

void MainWindow::keyPressEvent(QKeyEvent *e)
{
    const int key = e->key();

    if (key == Qt::Key_C) {
        m_viewer->centerScene();
    }
}

void MainWindow::updateSharpEdges()
{
    std::vector<unsigned int> meshSharpEdges =
            sharpEdges(m_mesh, m_sharpEdgesAngleSpinBox->value());

    if (!m_sharpEdgesDrawer) {
        auto sharpEdgesGeom = new Geometry;
        sharpEdgesGeom->vertices = m_mesh->vertices;

        sharpEdgesGeom->faceIndexes = meshSharpEdges;

        m_sharpEdgesDrawer = new GeometryDrawer(sharpEdgesGeom,
                                                GeometryDrawer::Lines);
        m_sharpEdgesDrawer->setColor(GlobalColor::Green);

        m_scene->addDrawer(m_sharpEdgesDrawer);
    }
    else {
        m_sharpEdgesDrawer->geometry()->vertices = m_mesh->vertices;
        m_sharpEdgesDrawer->geometry()->faceIndexes = meshSharpEdges;
    }

    m_viewer->update();
}

Geometry *MainWindow::geometryForMeshType(MainWindow::MeshType meshType) const
{
    Geometry *ret = nullptr;

    if (meshType == MeshType::Cylinder) {
        ret = cylinderMesh(m_cylMeridiansSpinBox->value(),
                           m_cylRadiusSpinBox->value(),
                           m_cylHeightSpinBox->value());
    }
    else {
        ret = sphereMesh(m_sphereMeridiansSpinBox->value(),
                         m_sphereParallelsSpinBox->value(),
                         m_sphereRadiusSpinBox->value());
    }

    return ret;
}


/////////////////////// Main ///////////////////////

int main(int argc, char **argv)
{
    QApplication app(argc, argv);

    MainWindow mw;
    mw.show();

    return app.exec();
}

#include "main.moc"
