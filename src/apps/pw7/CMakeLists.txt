project(pw7)

set(SOURCES
    main.cpp)

set(HEADERS)

# OpenGL
find_package(OpenGL REQUIRED)
include_directories(${OPENGL_INCLUDE_DIRS})

# Qt5
find_package(Qt5Widgets REQUIRED)
set(CMAKE_AUTOMOC true)
set(CMAKE_INCLUDE_CURRENT_DIR ON)

# Rules
add_executable(pw7 ${SOURCES})
	
target_link_libraries(pw7 epwmath epwgl ${OPENGL_LIBRARIES} Qt5::Widgets)
