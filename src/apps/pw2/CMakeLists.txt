project(pw2)

set(SOURCES
    main.cpp
    vec3edit.cpp
    vectordimindicator.cpp)

set(HEADERS
    vec3edit.h
    vectordimindicator.h)

# OpenGL
find_package(OpenGL REQUIRED)
include_directories(${OPENGL_INCLUDE_DIRS})

# Qt5
find_package(Qt5Widgets REQUIRED)
set(CMAKE_AUTOMOC true)

# Rules
add_executable(pw2 ${SOURCES})
	
target_link_libraries(pw2 epwmath epwgl ${OPENGL_LIBRARIES} ${Qt5Widgets_LIBRARIES})
