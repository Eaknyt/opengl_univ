#include "vectordimindicator.h"

#include <QPaintEvent>
#include <QPainter>


////////////////////// VectorDimIndicatorPrivate //////////////////////

class VectorDimIndicatorPrivate
{
public:
    VectorDimIndicatorPrivate(VectorDimIndicator *qq);

    VectorDimIndicator *q;

    QString text;
    QColor color;
};

VectorDimIndicatorPrivate::VectorDimIndicatorPrivate(VectorDimIndicator *qq) :
    q(qq),
    text(),
    color()
{}


////////////////////// VectorDimIndicator //////////////////////

VectorDimIndicator::VectorDimIndicator(QWidget *parent) :
    QWidget(parent),
    d(new VectorDimIndicatorPrivate(this))
{}

VectorDimIndicator::VectorDimIndicator(const QString &text,
                                       const QColor &color,
                                       QWidget *parent) :
    QWidget(parent),
    d(new VectorDimIndicatorPrivate(this))
{
    d->text = text;
    d->color = color;
}

VectorDimIndicator::~VectorDimIndicator()
{}

QString VectorDimIndicator::text() const
{
    return d->text;
}

void VectorDimIndicator::setText(const QString &text)
{
    if (d->text != text) {
        d->text = text;

        Q_EMIT textChanged(text);
    }
}

QColor VectorDimIndicator::color() const
{
    return d->color;
}

void VectorDimIndicator::setColor(const QColor &color)
{
    if (d->color != color) {
        d->color = color;

        Q_EMIT colorChanged(color);
    }
}

QSize VectorDimIndicator::sizeHint() const
{
    const QFontMetrics fm(fontMetrics());

    int w = fm.width(d->text) + 6;
    int h = fm.height();

    QSize ret(w, h);

    return ret;
}

QSize VectorDimIndicator::minimumSizeHint() const
{
    QSize ret(3, fontMetrics().height());

    return ret;
}

void VectorDimIndicator::paintEvent(QPaintEvent *event)
{
    QPainter p(this);
    p.setRenderHints(QPainter::Antialiasing | QPainter::TextAntialiasing);

    const QRect eventRect = event->rect();

    p.save();
    {
        const int rectRadius = 2;

        p.setPen(d->color);
        p.setBrush(d->color);
        p.drawRoundedRect(eventRect.adjusted(0, 0, -1, -1),
                          rectRadius, rectRadius);
    }
    p.restore();

    if (eventRect.width() >= sizeHint().width()) {
        p.setPen(Qt::white);
        p.drawText(eventRect, Qt::AlignHCenter | Qt::AlignVCenter, d->text);
    }
}
