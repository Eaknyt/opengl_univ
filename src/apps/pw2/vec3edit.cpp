#include "vec3edit.h"

#include <QDoubleSpinBox>
#include <QHBoxLayout>

#include "vectordimindicator.h"


using namespace epwmath;

////////////////////// Vec3EditPrivate //////////////////////

class Vec3EditPrivate
{
public:
    Vec3EditPrivate(Vec3Edit *qq);

    Vec3Edit *q;

    QDoubleSpinBox *xSpinBox;
    QDoubleSpinBox *ySpinBox;
    QDoubleSpinBox *zSpinBox;
};

Vec3EditPrivate::Vec3EditPrivate(Vec3Edit *qq) :
    q(qq),
    xSpinBox(new QDoubleSpinBox(qq)),
    ySpinBox(new QDoubleSpinBox(qq)),
    zSpinBox(new QDoubleSpinBox(qq))
{}


////////////////////// Vec3Edit //////////////////////

Vec3Edit::Vec3Edit(QWidget *parent) :
    QWidget(parent),
    d(new Vec3EditPrivate(this))
{
    setFocusPolicy(Qt::StrongFocus);
    setFocusProxy(d->xSpinBox);
    setTabOrder(d->xSpinBox, d->ySpinBox);
    setTabOrder(d->ySpinBox, d->zSpinBox);

    auto layout = new QHBoxLayout(this);
    layout->setContentsMargins(0, 0, 0, 0);

    auto xIndicator = new VectorDimIndicator("X", Qt::red, this);
    auto yIndicator = new VectorDimIndicator("Y",
                                             QColor::fromRgbF(0.24, 0.73, 0.13),
                                             this);
    auto zIndicator = new VectorDimIndicator("Z",
                                             QColor::fromRgbF(0.26, 0.54, 1.0),
                                             this);

    layout->addWidget(xIndicator, 0, Qt::AlignLeft);
    layout->addWidget(d->xSpinBox, 1);

    layout->addWidget(yIndicator, 0, Qt::AlignLeft);
    layout->addWidget(d->ySpinBox, 1);

    layout->addWidget(zIndicator, 0, Qt::AlignLeft);
    layout->addWidget(d->zSpinBox, 1);

    connect(d->xSpinBox, static_cast<void (QDoubleSpinBox::*)(double)>(&QDoubleSpinBox::valueChanged),
            this, [=] (double val) {
        Q_EMIT valueChanged(Vec3(static_cast<double>(val),
                                 d->ySpinBox->value(),
                                 d->zSpinBox->value()));
    });

    connect(d->ySpinBox, static_cast<void (QDoubleSpinBox::*)(double)>(&QDoubleSpinBox::valueChanged),
            this, [=] (double val) {
        Q_EMIT valueChanged(Vec3(d->xSpinBox->value(),
                                 static_cast<double>(val),
                                 d->zSpinBox->value()));
    });

    connect(d->zSpinBox, static_cast<void (QDoubleSpinBox::*)(double)>(&QDoubleSpinBox::valueChanged),
            this, [=] (double val) {
        Q_EMIT valueChanged(Vec3(d->xSpinBox->value(),
                                 d->ySpinBox->value(),
                                 static_cast<double>(val)));
    });
}

Vec3Edit::~Vec3Edit()
{}

Vec3 Vec3Edit::value() const
{
    return Vec3(d->xSpinBox->value(),
                d->ySpinBox->value(),
                d->zSpinBox->value());
}

void Vec3Edit::setValue(const Vec3 &val)
{
    if (value() != val) {
        d->xSpinBox->setValue(val.x());
        d->ySpinBox->setValue(val.y());
        d->zSpinBox->setValue(val.z());

        Q_EMIT valueChanged(val);
    }
}

int Vec3Edit::decimals() const
{
    return d->xSpinBox->decimals();
}

void Vec3Edit::setDecimals(int prec)
{
    if (decimals() != prec) {
        d->xSpinBox->setDecimals(prec);
        d->ySpinBox->setDecimals(prec);
        d->zSpinBox->setDecimals(prec);

        Q_EMIT decimalsChanged(prec);
    }
}

double Vec3Edit::xMin() const
{
    return d->xSpinBox->minimum();
}

void Vec3Edit::setXMin(double min)
{
    if (xMin() != min) {
        d->xSpinBox->setMinimum(min);

        Q_EMIT xMinChanged(min);
    }
}

double Vec3Edit::xMax() const
{
    return d->xSpinBox->maximum();
}

void Vec3Edit::setXMax(double max)
{
    if (xMax() != max) {
        d->xSpinBox->setMaximum(max);

        Q_EMIT xMaxChanged(max);
    }
}

double Vec3Edit::yMin() const
{
    return d->ySpinBox->minimum();
}

void Vec3Edit::setYMin(double min)
{
    if (yMin() != min) {
        d->ySpinBox->setMaximum(min);

        Q_EMIT yMinChanged(min);
    }
}

double Vec3Edit::yMax() const
{
    return d->ySpinBox->maximum();
}

void Vec3Edit::setYMax(double max)
{
    if (yMax() != max) {
        d->ySpinBox->setMaximum(max);

        Q_EMIT yMaxChanged(max);
    }
}

double Vec3Edit::zMin() const
{
    return d->zSpinBox->minimum();
}

void Vec3Edit::setZMin(double min)
{
    if (zMin() != min) {
        d->zSpinBox->setMinimum(min);

        Q_EMIT zMinChanged(min);
    }
}

double Vec3Edit::zMax() const
{
    return d->zSpinBox->maximum();
}

void Vec3Edit::setZMax(double max)
{
    if (zMax() != max) {
        d->zSpinBox->setMaximum(max);

        Q_EMIT zMaxChanged(max);
    }
}
