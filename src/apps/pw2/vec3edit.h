#ifndef VEC3EDIT_H
#define VEC3EDIT_H

#include <QWidget>

#include "epwmath/vec3.h"

class Vec3EditPrivate;

class Vec3Edit : public QWidget
{
    Q_OBJECT

    Q_PROPERTY(epwmath::Vec3 value READ value WRITE setValue NOTIFY valueChanged)
    Q_PROPERTY(int decimals READ decimals WRITE setDecimals NOTIFY decimalsChanged)
    Q_PROPERTY(double xMin READ xMin WRITE setXMin NOTIFY xMinChanged)
    Q_PROPERTY(double xMax READ xMax WRITE setXMax NOTIFY xMaxChanged)
    Q_PROPERTY(double yMin READ yMin WRITE setYMin NOTIFY yMinChanged)
    Q_PROPERTY(double yMax READ yMax WRITE setYMax NOTIFY yMaxChanged)
    Q_PROPERTY(double zMin READ zMin WRITE setZMin NOTIFY zMinChanged)
    Q_PROPERTY(double zMax READ zMax WRITE setZMax NOTIFY zMaxChanged)

public:
    explicit Vec3Edit(QWidget *parent = nullptr);
    ~Vec3Edit();

    epwmath::Vec3 value() const;

    int decimals() const;

    double xMin() const;
    double xMax() const;

    double yMin() const;
    double yMax() const;

    double zMin() const;
    double zMax() const;

Q_SIGNALS:
    void valueChanged(const epwmath::Vec3 &);

    void decimalsChanged(int);

    void xMinChanged(double);
    void xMaxChanged(double);

    void yMinChanged(double);
    void yMaxChanged(double);

    void zMinChanged(double);
    void zMaxChanged(double);

public Q_SLOTS:
    void setValue(const epwmath::Vec3 &val);

    void setDecimals(int prec);

    void setXMin(double min);
    void setXMax(double max);

    void setYMin(double min);
    void setYMax(double max);

    void setZMin(double min);
    void setZMax(double max);

private:
    const QScopedPointer<Vec3EditPrivate> d;
};

#endif // VEC3EDIT_H
