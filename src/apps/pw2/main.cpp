#include <assert.h>
#include <iostream>

#include <QtGui/QOpenGLContext>
#include <QtGui/QOpenGLFunctions_2_0>
#include <QtGui/QPainter>
#include <QtGui/QWheelEvent>

#include <QtWidgets/QApplication>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QOpenGLWidget>
#include <QtWidgets/QSplitter>

// Own includes
#include "epwgl/color.h"
#include "epwgl/drawhelpers.h"

#include "epwmath/camera.h"
#include "epwmath/curves.h"
#include "epwmath/mat4.h"
#include "epwmath/vec3.h"

#include "vec3edit.h"


using namespace epwgl;
using namespace epwmath;


/////////////////////// Helpers ///////////////////////

const float GLVIEW_BOUND = 5.;

class GlWidget;
GlWidget *glWidget = nullptr;

std::vector<Vec3> commonControlsPoints {
    {0, 0, 0},
    {2, 1, 0},
    {1, -1, 0},
    {1, 0, 0},
    {0.5, 2, 0}
};


/////////////////////// GlWidget ///////////////////////

class GlWidget : public QOpenGLWidget
{
public:
    GlWidget(QWidget *parent = nullptr);
    ~GlWidget();

protected:
    void initializeGL() override;
    void paintGL() override;
    void resizeGL(int w, int h) override;

    void wheelEvent(QWheelEvent *event) override;

private:
    void render();

    void drawBezierCurve(const std::vector<Vec3> curve,
                         GlobalColor curveColor,
                         const std::vector<Vec3> controlPoints);

    void drawLegend();

private:
    QOpenGLFunctions_2_0 *f;
    Camera m_camera;
};

GlWidget::GlWidget(QWidget *parent) :
    QOpenGLWidget(parent),
    f(nullptr),
    m_camera()
{}

GlWidget::~GlWidget()
{
    makeCurrent();

    // Delete shaders if necessary

    doneCurrent();
}

void GlWidget::initializeGL()
{
    QOpenGLContext *ctx = QOpenGLContext::currentContext();
    f = ctx->versionFunctions<QOpenGLFunctions_2_0>();
    assert(f);

    m_camera.setPos({2, 2, 4});

    eglClearColor(GlobalColor::Black);

    f->glEnable(GL_DEPTH_TEST);
}

void GlWidget::paintGL()
{
    f->glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    f->glMatrixMode(GL_PROJECTION);
    f->glLoadMatrixf(m_camera.projectionMatrix().constData());

    f->glMatrixMode(GL_MODELVIEW);
    f->glLoadMatrixf(m_camera.viewMatrix().constData());

    render();

    f->glFlush();
}

void GlWidget::resizeGL(int w, int h)
{
    f->glViewport(0, 0, w, h);
    m_camera.setAspectRatio(static_cast<float>(w) / static_cast<float>(h));
}

void GlWidget::wheelEvent(QWheelEvent *e)
{
    m_camera.zoom((e->angleDelta().y() < 0) ? .02 : -.02);

    update();
}

void GlWidget::render()
{

    // Hermite
    //    std::vector<Vec3> curve = hermite(Vec3(-5, 0, 0), Vec3(-1, 1, 0),
    //                                        Vec3(-2, 0, 0), Vec3(1, 1, 0),
    //                                        50);
    //    eglDrawCurve(curve);

    f->glPointSize(7);

    // Bezier w/ Bernstein
    std::vector<Vec3> cPoints = commonControlsPoints;
    for (Vec3 &cp : cPoints) {
        cp += Vec3(-4, 0, 0);
    }

    std::vector<Vec3> bezierCurve = bezierBernstein(cPoints, 150);
    drawBezierCurve(bezierCurve, GlobalColor::White, cPoints);

    // Bezier w/ Casteljau
    cPoints = commonControlsPoints;
    for (Vec3 &cp : cPoints) {
        cp += Vec3(2, 0, 0);
    }

    bezierCurve = bezierCasteljau(cPoints, 150);
    drawBezierCurve(bezierCurve, GlobalColor::Yellow, cPoints);

    // Draw other stuff using QPainter
    drawLegend();
}

void GlWidget::drawBezierCurve(const std::vector<Vec3> curve,
                               GlobalColor curveColor,
                               const std::vector<Vec3> controlPoints)
{
    eglColor(curveColor);
    eglDrawCurve(curve);

    eglColor(GlobalColor::Red);
    eglDrawCurve(controlPoints);

    //  Draw control points
    eglColor(GlobalColor::YAxis);

    for (const Vec3 &cp : controlPoints) {
        eglDrawPoint(cp);
    }
}

void GlWidget::drawLegend()
{
    QPainter p(this);

    QPoint topLeft = rect().topLeft();
    const int xOffsetFromLeft = 10;

    p.setPen(Qt::white);
    p.drawLine(topLeft + QPoint(xOffsetFromLeft, 15),
               topLeft + QPoint(70, 15));

    p.drawText(topLeft + QPoint(80, 20),
               "Bezier Curve using Bernstein");


    p.setPen(Qt::yellow);
    p.drawLine(topLeft + QPoint(xOffsetFromLeft, 35),
               topLeft + QPoint(70, 35));
    p.setPen(Qt::white);

    p.drawText(rect().topLeft() + QPoint(80, 40),
               "Bezier Curve using Casteljau");
}


/////////////////////// Main ///////////////////////

void onControlPointEdited(const QString &objectName, const Vec3 &v)
{
    int editorNumber = objectName.toInt();

    commonControlsPoints[editorNumber] = v;

    glWidget->update();
}

int main(int argc, char **argv)
{
    QApplication app(argc, argv);

    QMainWindow mw;

    QSplitter splitter(&mw);
    splitter.setHandleWidth(15);

    glWidget = new GlWidget(&splitter);
    splitter.addWidget(glWidget);

    QFrame controlsPanel(&splitter);
    splitter.addWidget(&controlsPanel);

    splitter.setStretchFactor(0, 1);
    splitter.setStretchFactor(1, 2);

    QFormLayout controlsLayout(&controlsPanel);
    controlsLayout.addWidget(new QLabel("Controls Points"));

    // Create editors for control points
    for (std::size_t i = 0; i < commonControlsPoints.size(); ++i)  {
        QString iStr = QString::number(i);

        auto cpEditor = new Vec3Edit;
        cpEditor->setObjectName(iStr);
        cpEditor->setXMin(-GLVIEW_BOUND);
        cpEditor->setXMax(GLVIEW_BOUND);
        cpEditor->setYMin(-GLVIEW_BOUND);
        cpEditor->setYMax(GLVIEW_BOUND);
        cpEditor->setZMin(-GLVIEW_BOUND);
        cpEditor->setZMax(GLVIEW_BOUND);
        cpEditor->setValue(commonControlsPoints[i]);

        QObject::connect(cpEditor, &Vec3Edit::valueChanged,
                         [cpEditor] (const Vec3 &v) {
            onControlPointEdited(cpEditor->objectName(), v);
        });

        controlsLayout.addRow("#" + QString::number(i), cpEditor);
    }

    // Go
    mw.setCentralWidget(&splitter);
    mw.show();

    return app.exec();
}
