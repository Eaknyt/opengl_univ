#include <assert.h>
#include <iostream>

#include <QtGui/QOpenGLContext>
#include <QtGui/QOpenGLFunctions_2_0>
#include <QtGui/QPainter>
#include <QtGui/QWheelEvent>

#include <QtWidgets/QApplication>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QOpenGLWidget>
#include <QtWidgets/QSplitter>

// Own includes
#include "epwgl/color.h"
#include "epwgl/drawhelpers.h"

#include "epwmath/camera.h"
#include "epwmath/curves.h"
#include "epwmath/mat4.h"
#include "epwmath/surfaces.h"
#include "epwmath/vec3.h"


using namespace epwgl;
using namespace epwmath;


/////////////////////// Helpers ///////////////////////

class GlWidget;
GlWidget *glWidget = nullptr;


/////////////////////// GlWidget ///////////////////////

class GlWidget : public QOpenGLWidget
{
public:
    GlWidget(QWidget *parent = nullptr);
    ~GlWidget();

protected:
    void initializeGL() override;
    void paintGL() override;
    void resizeGL(int w, int h) override;

    void wheelEvent(QWheelEvent *event) override;

private:
    void render();

private:
    QOpenGLFunctions_2_0 *f;
    Camera m_camera;
};

GlWidget::GlWidget(QWidget *parent) :
    QOpenGLWidget(parent),
    f(nullptr),
    m_camera()
{}

GlWidget::~GlWidget()
{
    makeCurrent();

    // Delete shaders if necessary

    doneCurrent();
}

void GlWidget::initializeGL()
{
    QOpenGLContext *ctx = QOpenGLContext::currentContext();
    f = ctx->versionFunctions<QOpenGLFunctions_2_0>();
    assert(f);

    m_camera.setPos({4, 4, 4});

    eglClearColor(GlobalColor::Black);

    f->glEnable(GL_DEPTH_TEST);
}

void GlWidget::paintGL()
{
    f->glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    f->glMatrixMode(GL_PROJECTION);
    f->glLoadMatrixf(m_camera.projectionMatrix().constData());

    f->glMatrixMode(GL_MODELVIEW);
    f->glLoadMatrixf(m_camera.viewMatrix().constData());

    render();

    f->glFlush();
}

void GlWidget::resizeGL(int w, int h)
{
    f->glViewport(0, 0, w, h);
    m_camera.setAspectRatio(static_cast<float>(w) / static_cast<float>(h));
}

void GlWidget::wheelEvent(QWheelEvent *e)
{
    m_camera.zoom((e->angleDelta().y() < 0) ? .03 : -.03);

    update();
}

void GlWidget::render()
{
    // Cylindric surface
    std::vector<Vec3> genControlPoints {
        {0, 0, 0},
        {2, 1, 0},
        {1, -1, 0},
    };

    eglColor(GlobalColor::Green);
    eglDrawCurve(bezierCasteljau(genControlPoints, 100));

    CurveSet cylSurface = cylindricSurface(genControlPoints,
                                           {0, 0, 0}, {1, 1, 0},
                                           100, 100);

    eglColor(GlobalColor::Red);
    eglDrawSurface(cylSurface);

    // Ruled surface
    std::vector<Vec3> b1ControlPoints {
        {0, 0, 0},
        {2, 1, 0},
        {1, -1, 0}
    };

    std::vector<Vec3> b2ControlPoints {
        {1, 0, 0},
        {1.5, -1.1, 0},
        {2.8, 1.5, 0}
    };

    const int u = 100;

    eglColor(GlobalColor::Green);
    eglDrawCurve(bezierCasteljau(b1ControlPoints, u));
    eglDrawCurve(bezierCasteljau(b2ControlPoints, u));

    CurveSet rulSurface = ruledSurface(b1ControlPoints, b2ControlPoints,
                                       u, u);

    eglColor(GlobalColor::Red);
    eglDrawSurface(rulSurface);
}


/////////////////////// Main ///////////////////////

int main(int argc, char **argv)
{
    QApplication app(argc, argv);

    QMainWindow mw;

    QSplitter splitter(&mw);
    splitter.setHandleWidth(15);

    glWidget = new GlWidget(&splitter);
    splitter.addWidget(glWidget);

    // Go
    mw.setCentralWidget(&splitter);
    mw.show();

    return app.exec();
}
