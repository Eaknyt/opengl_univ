project(pw3)

set(SOURCES
    main.cpp)

set(HEADERS)

# OpenGL
find_package(OpenGL REQUIRED)
include_directories(${OPENGL_INCLUDE_DIRS})

# Qt5
find_package(Qt5Widgets REQUIRED)
set(CMAKE_AUTOMOC true)

# Rules
add_executable(pw3 ${SOURCES})
	
target_link_libraries(pw3 epwmath epwgl ${OPENGL_LIBRARIES} ${Qt5Widgets_LIBRARIES})
