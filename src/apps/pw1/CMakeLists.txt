project(pw1)

set(SOURCES
    main.cpp)

# OpenGL
find_package(OpenGL REQUIRED)
find_package(GLUT REQUIRED)
include_directories(${OPENGL_INCLUDE_DIRS} ${GLUT_INCLUDE_DIRS})

add_executable(pw1 ${SOURCES})

target_link_libraries(pw1 epwmath epwgl ${OPENGL_LIBRARIES} ${GLUT_LIBRARY})
