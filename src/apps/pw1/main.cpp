// OS includes
#include <iostream>

#include <GL/glut.h>

// Own includes
#include "epwgl/color.h"
#include "epwgl/drawhelpers.h"

#include "epwmath/vec3.h"


/////////////////////// Constants ///////////////////////

const int WINDOW_WIDTH = 480;
const int WINDOW_HEIGHT = 480;

// Esc Key
const int KEY_ESC = 27;

using namespace epwgl;
using namespace epwmath;


/////////////////////// Helpers ///////////////////////

void initGL()
{
    eglClearColor(GlobalColor::Black);
}

void initScene()
{

}

void render()
{
    eglColor(GlobalColor::Cyan);

    //eglDrawLine(Vec3(-1, -1, 0), Vec3(1, 1, 0));
    //eglDrawLine(Vec3(1, -1, 0), Vec3(-1, 1, 0));

    //eglDrawTriangle(Vec3(-1, -1, 0),
    //                Vec3(1, -1, 0),
    //                Vec3(1, 1, 0));

    //std::array<float, 12> polygonData = {
    //    -1, -1, 0,
    //    1, -1, 0,
    //    1, 1, 0,
    //    -1, 1, 0
    //};

    //eglDrawPolygon(polygonData);

    // Draw the line
    Vec3 lp1(1, 1, 0);
    Vec3 lp2(-1, -1, 0);

    eglDrawLine(lp1, lp2);

    // Draw a point outside the line
    glPointSize(6);
    Vec3 p1(-1.3, 1.6, 0.2);

    eglColor(GlobalColor::XAxis);
    eglDrawPoint(p1);

    // Draw the projection of p1 on the line
    Vec3 pp1 = p1.projectOnLine(lp1, lp2);

    eglColor(GlobalColor::YAxis);
    eglDrawPoint(pp1);
}


/////////////////////// GLUT Callbacks ///////////////////////

void onDisplay()
{
    glClear(GL_COLOR_BUFFER_BIT);
    glLoadIdentity();

    render();

    glFlush();
}

void onResize(GLsizei width, GLsizei height)
{
    glViewport(0, 0, width, height);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(-2.0, 2.0, -2.0, 2.0, -2.0, 2.0);

    glMatrixMode(GL_MODELVIEW);
}

void onKeyPressed(unsigned char key, int x, int y)
{
    switch (key) {
    case KEY_ESC:
        exit(0);
        break;
    default:
        std::cerr << "The key " << key << " is not enabled" << std::endl;
        break;
    }
}


/////////////////////// Main ///////////////////////

int main(int argc, char **argv)
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGBA);

    glutInitWindowSize(WINDOW_WIDTH, WINDOW_HEIGHT);
    glutInitWindowPosition(0, 0);
    glutCreateWindow("Programmation 3D - TP1");

    initGL();
    initScene();

    glutDisplayFunc(&onDisplay);
    glutReshapeFunc(&onResize);
    glutKeyboardFunc(&onKeyPressed);

    glutMainLoop();

    return 0;
}
