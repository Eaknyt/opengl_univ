#include <cmath>
#include <iostream>

#include <QtCore/QDebug>

#include <QtGui/QKeyEvent>

#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QSplitter>
#include <QtWidgets/QTabWidget>

// Own includes
#include "epwgl/color.h"
#include "epwgl/drawhelpers.h"
#include "epwgl/geometrydrawer.h"
#include "epwgl/offloader.h"
#include "epwgl/volumes.h"

#include "epwgl/scene/renderable.h"
#include "epwgl/scene/renderer.h"
#include "epwgl/scene/scene.h"

#include "editorlib/assetbrowser.h"
#include "editorlib/sceneoutlineview.h"
#include "editorlib/sceneviewer.h"
#include "editorlib/viewercontrols.h"

#include "epwmath/boundingbox.h"
#include "epwmath/camera.h"
#include "epwmath/vec2.h"



using namespace epwgl;
using namespace epwmath;


////////////////////// MainWindow //////////////////////

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);

private Q_SLOTS:
    void loadOff(const QString &fileName);

    void setBackfaceCulling(bool cull);

private:
    Scene *m_scene;

    Renderer *m_renderer;

    SceneViewer *m_viewer1;
    SceneViewer *m_viewer2;

    SceneOutlineView *m_sceneOutlineView;
    AssetBrowser *m_assetBrowser;
};

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    m_scene(new Scene(this)),
    m_renderer(new Renderer),
    m_viewer1(nullptr),
    m_viewer2(nullptr),
    m_sceneOutlineView(nullptr),
    m_assetBrowser(nullptr)
{
    m_renderer->setScene(m_scene);

    auto myCentralWidget = new QWidget(this);
    setCentralWidget(myCentralWidget);

    auto myLayout = new QVBoxLayout(myCentralWidget);

    // Build viewers and asset browser
    auto splitter = new QSplitter(myCentralWidget);
    splitter->setHandleWidth(15);

    auto rightPaneSplitter = new QSplitter(splitter);
    rightPaneSplitter->setHandleWidth(15);
    rightPaneSplitter->setOrientation(Qt::Vertical);

    //  Outline view
    m_sceneOutlineView = new SceneOutlineView(this);
    m_sceneOutlineView->setScene(m_scene);

    auto outlineViewTab = new QTabWidget(rightPaneSplitter);
    outlineViewTab->addTab(m_sceneOutlineView, "Scene Outline");

    //  Asset browser
    m_assetBrowser = new AssetBrowser(this);
    auto assetBrowserTab = new QTabWidget(rightPaneSplitter);
    assetBrowserTab->addTab(m_assetBrowser, "Asset Browser");

    //  Viewer 1
    m_viewer1 = new SceneViewer(splitter);

    auto viewer1Layout = new QVBoxLayout(m_viewer1);
    viewer1Layout->addWidget(new ViewerControls(m_viewer1));
    viewer1Layout->addStretch();

    m_viewer1->setScene(m_scene);
    m_viewer1->setRenderer(m_renderer);

    //  Viewer 2
    m_viewer2 = new SceneViewer(splitter);
    m_viewer2->camera()->setPos({0, 100, 0});
    m_viewer2->setScene(m_scene);
    m_viewer2->setRenderer(m_renderer);

    // Assemble all widgets
    rightPaneSplitter->addWidget(outlineViewTab);
    rightPaneSplitter->addWidget(assetBrowserTab);

    splitter->addWidget(m_viewer1);
    splitter->addWidget(m_viewer2);
    splitter->addWidget(rightPaneSplitter);

    myLayout->addWidget(splitter);

    // S/S connections
    connect(m_assetBrowser, &AssetBrowser::offSelected,
            this, &MainWindow::loadOff);
}

void MainWindow::loadOff(const QString &fileName)
{
    Geometry *geom = readOff("res/off/" + fileName.toStdString());

    auto renderable = new Renderable;
    renderable->mesh()->setDrawMode(GeometryDrawer::Triangles);
    renderable->mesh()->setGeometry(geom);

    m_scene->addRenderable(renderable);
}

void MainWindow::setBackfaceCulling(bool cull)
{
    m_renderer->setCullBackfaces(cull);

    m_viewer1->update();
    m_viewer2->update();
}


/////////////////////// Main ///////////////////////

int main(int argc, char **argv)
{
    QApplication app(argc, argv);

    MainWindow mw;
    mw.show();

    return app.exec();
}

#include "main.moc"
