#include <cmath>
#include <iostream>

#include <QtCore/QDebug>

#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QVBoxLayout>

// Own includes
#include "epwgl/color.h"
#include "epwgl/helpers.h"
#include "epwgl/viewer.h"

#include "epwmath/octree.h"
#include "epwmath/quadrics.h"
#include "epwmath/vec3.h"


using namespace epwgl;
using namespace epwmath;


/////////////////////// Pw5Viewer ///////////////////////

class Pw5Viewer : public epwgl::Viewer
{
    Q_OBJECT

public:
    enum class AdapterFunc
    {
        NoAdapter,
        VolumicSphere,
        VolumicCylinder,
        Intersect,
        Subtract,
        Union,
    };

    Pw5Viewer(QWidget *parent = nullptr);

    void setAdapterFunc(AdapterFunc func);

    int sphereRadius() const;
    int cylRadius() const;

public Q_SLOTS:
    void setResolution(int res);
    void setShowRefSphere(bool show);

    void setSphereRadius(float sphereRadius);
    void setCylRadius(float radius);

protected:
    void render() override;

private:
    void computeRefObject();
    void computeOctree();

    Octree::IntersectSurfaceHint octreeSphereHelper(Octree *octree) const;
    Octree::IntersectSurfaceHint octreeCylinderHelper(Octree *octree) const;

    Octree::IntersectSurfaceHint octreeIntersectHelper(Octree *octree) const;
    Octree::IntersectSurfaceHint octreeSubtractHelper(Octree *octree) const;
    Octree::IntersectSurfaceHint octreeUnionHelper(Octree *octree) const;

private:
    Octree m_octree;

    AdapterFunc m_adapterFunc;
    int m_res;

    // Sphere / Cylinder common
    Vec3 m_center;
    bool m_showRefObjects;

    // Sphere-specific
    float m_sphereRadius;

    Geometry m_sphereRefGeom;

    // Cylinder-specific
    Vec3 m_cylAxis;
    float m_cylRadius;

    Geometry m_cylRefGeom;
};

Pw5Viewer::Pw5Viewer(QWidget *parent) :
    epwgl::Viewer(parent),
    m_octree(),
    m_adapterFunc(AdapterFunc::NoAdapter),
    m_res(0),
    m_center(),
    m_showRefObjects(false),
    m_sphereRadius(5),
    m_sphereRefGeom(),
    m_cylAxis(0, 0, 5),
    m_cylRadius(2),
    m_cylRefGeom()
{
    setRenderMode(Viewer::RenderMode::Wireframe);

    m_octree = Octree(m_center, m_sphereRadius, nullptr);

    computeRefObject();
}

void Pw5Viewer::setAdapterFunc(AdapterFunc func)
{
    if (m_adapterFunc != func) {
        m_adapterFunc = func;

        computeOctree();
        computeRefObject();

        update();
    }
}

void Pw5Viewer::setResolution(int res)
{
    if (m_res != res) {
        m_res = res;

        computeOctree();

        update();
    }
}

void Pw5Viewer::render()
{
    if (m_showRefObjects) {
        eglColor(GlobalColor::White);

        if (m_adapterFunc == AdapterFunc::VolumicSphere) {
            eglDrawGeometry(m_sphereRefGeom, GL_QUADS);
        }
        else if (m_adapterFunc == AdapterFunc::VolumicCylinder) {
            eglDrawGeometry(m_cylRefGeom, GL_QUADS);
        }
        else if (m_adapterFunc >= AdapterFunc::Intersect) {
            eglDrawGeometry(m_sphereRefGeom, GL_QUADS);
            eglDrawGeometry(m_cylRefGeom, GL_QUADS);
        }
    }

    eglColor(GlobalColor::Red);
    eglDrawOctree(&m_octree);
}

void Pw5Viewer::computeRefObject()
{
    if (m_adapterFunc == AdapterFunc::NoAdapter) {
        m_sphereRefGeom = Geometry();
        m_cylRefGeom = Geometry();
    }
    else if (m_adapterFunc == AdapterFunc::VolumicSphere) {
        m_sphereRefGeom = sphere(20, 20, m_sphereRadius);
    }
    else if (m_adapterFunc == AdapterFunc::VolumicCylinder) {
        m_cylRefGeom = cylinder(100, m_cylRadius, m_cylAxis.z());
    }
    else if (m_adapterFunc >= AdapterFunc::Intersect) {
        m_sphereRefGeom = sphere(20, 20, m_sphereRadius);
        m_cylRefGeom = cylinder(100, m_cylRadius, m_cylAxis.z());
    }
}

void Pw5Viewer::computeOctree()
{
    m_octree.clear();

    if (m_adapterFunc == AdapterFunc::NoAdapter) {
        m_octree.subdiv(m_res);
    }
    else if (m_adapterFunc == AdapterFunc::VolumicSphere) {
        m_octree.subdiv(m_res, [=] (Octree *octree) -> Octree::IntersectSurfaceHint {
            octreeSphereHelper(octree);
        });
    }
    else if (m_adapterFunc == AdapterFunc::VolumicCylinder) {
        m_octree.subdiv(m_res, [=] (Octree *octree) -> Octree::IntersectSurfaceHint {
            octreeCylinderHelper(octree);
        });
    }
    else if (m_adapterFunc == AdapterFunc::Intersect) {
        m_octree.subdiv(m_res, [=] (Octree *octree) -> Octree::IntersectSurfaceHint {
            octreeIntersectHelper(octree);
        });
    }
    else if (m_adapterFunc == AdapterFunc::Subtract) {
        m_octree.subdiv(m_res, [=] (Octree *octree) -> Octree::IntersectSurfaceHint {
            octreeSubtractHelper(octree);
        });
    }
    else if (m_adapterFunc == AdapterFunc::Union) {
        m_octree.subdiv(m_res, [=] (Octree *octree) -> Octree::IntersectSurfaceHint {
            octreeUnionHelper(octree);
        });
    }
}

Octree::IntersectSurfaceHint Pw5Viewer::octreeSphereHelper(Octree *octree) const
{
    const Vec3 vc = octree->center();

    const float voxelRadius = octree->radius();

    const Vec3 p0 = vc + Vec3(voxelRadius, voxelRadius, voxelRadius);
    const Vec3 p1 = vc + Vec3(-voxelRadius, voxelRadius, voxelRadius);
    const Vec3 p2 = vc + Vec3(voxelRadius, -voxelRadius, voxelRadius);
    const Vec3 p3 = vc + Vec3(-voxelRadius, -voxelRadius, voxelRadius);
    const Vec3 p4 = vc + Vec3(voxelRadius, voxelRadius, -voxelRadius);
    const Vec3 p5 = vc + Vec3(-voxelRadius, voxelRadius, -voxelRadius);
    const Vec3 p6 = vc + Vec3(voxelRadius, -voxelRadius, -voxelRadius);
    const Vec3 p7 = vc + Vec3(-voxelRadius, -voxelRadius, -voxelRadius);

    const float sx = m_center.x();
    const float sy = m_center.y();
    const float sz = m_center.z();

    std::array<Vec3, 9> voxelsVectors = {
        Vec3(vc.x(), vc.y(), vc.z()),
        Vec3(p0.x() - sx, p0.y() - sy, p0.z() - sz),
        Vec3(p1.x() - sx, p1.y() - sy, p1.z() - sz),
        Vec3(p2.x() - sx, p2.y() - sy, p2.z() - sz),
        Vec3(p3.x() - sx, p3.y() - sy, p3.z() - sz),
        Vec3(p4.x() - sx, p4.y() - sy, p4.z() - sz),
        Vec3(p5.x() - sx, p5.y() - sy, p5.z() - sz),
        Vec3(p6.x() - sx, p6.y() - sy, p6.z() - sz),
        Vec3(p7.x() - sx, p7.y() - sy, p7.z() - sz)
    };

    int counter = 0;

    for (const Vec3 &v : voxelsVectors) {
        if (v.length() <= m_sphereRadius) {
            counter++;
        }
    }

    auto ret = Octree::IntersectSurfaceHint::OutsideSurface;

    if (counter > 0 && counter < 9) {
        ret = Octree::IntersectSurfaceHint::IntersectsSurface;
    }
    else if (counter == 9){
        ret = Octree::IntersectSurfaceHint::WithinSurface;
    }

    return ret;
}

Octree::IntersectSurfaceHint Pw5Viewer::octreeCylinderHelper(Octree *octree) const
{
    const Vec3 vc = octree->center();

    const float voxelRadius = octree->radius();

    std::array<Vec3, 9> voxelVertices = {
        vc,
        Vec3(vc + Vec3(voxelRadius, voxelRadius, voxelRadius)),
        Vec3(vc + Vec3(-voxelRadius, voxelRadius, voxelRadius)),
        Vec3(vc + Vec3(voxelRadius, -voxelRadius, voxelRadius)),
        Vec3(vc + Vec3(-voxelRadius, -voxelRadius, voxelRadius)),
        Vec3(vc + Vec3(voxelRadius, voxelRadius, -voxelRadius)),
        Vec3(vc + Vec3(-voxelRadius, voxelRadius, -voxelRadius)),
        Vec3(vc + Vec3(voxelRadius, -voxelRadius, -voxelRadius)),
        Vec3(vc + Vec3(-voxelRadius, -voxelRadius, -voxelRadius))
    };

    const float cylAxisX = m_cylAxis.x();
    const float cylAxisY = m_cylAxis.y();
    const float cylAxisZ = m_cylAxis.z();

    const float cx = m_center.x();
    const float cy = m_center.y();
    const float cz = m_center.z();

    int counter = 0;

    for (const Vec3 &v : voxelVertices) {
        Vec3 proj = v.projectOnLine(m_cylAxis + m_center, m_center);

        const float projX = proj.x();
        const float projY = proj.y();
        const float projZ = proj.z();

        const bool projIsOnCylAxisX = (cx <= projX && projX <= cylAxisX) || (cx >= projX && projX >= cylAxisX);
        const bool projIsOnCylAxisY = (cy <= projY && projY <= cylAxisY) || (cy >= projY && projY >= cylAxisY);
        const bool projIsOnCylAxisZ = (cz <= projZ && projZ <= cylAxisZ) || (cz >= projZ && projZ >= cylAxisZ);

        if (projIsOnCylAxisX && projIsOnCylAxisY && projIsOnCylAxisZ) {
            const Vec3 vertexToProj(v.x() - projX,
                                    v.y() - projY,
                                    v.z() - projZ);

            if (vertexToProj.length() <= m_cylRadius) {
                counter++;
            }
        }
    }

    auto ret = Octree::IntersectSurfaceHint::OutsideSurface;

    if (counter > 0 && counter < 9) {
        ret = Octree::IntersectSurfaceHint::IntersectsSurface;
    }
    else if (counter == 9){
        ret = Octree::IntersectSurfaceHint::WithinSurface;
    }

    return ret;
}

Octree::IntersectSurfaceHint Pw5Viewer::octreeIntersectHelper(Octree *octree) const
{
    const Octree::IntersectSurfaceHint sphereHint = octreeSphereHelper(octree);
    const Octree::IntersectSurfaceHint cylHint = octreeCylinderHelper(octree);

    return (sphereHint == cylHint) ? sphereHint
                                   : Octree::IntersectSurfaceHint::OutsideSurface;
}

Octree::IntersectSurfaceHint Pw5Viewer::octreeSubtractHelper(Octree *octree) const
{
    const Octree::IntersectSurfaceHint sphereHint = octreeSphereHelper(octree);
    const Octree::IntersectSurfaceHint cylHint = octreeCylinderHelper(octree);

    Octree::IntersectSurfaceHint ret = Octree::IntersectSurfaceHint::OutsideSurface;

    if (sphereHint == Octree::IntersectSurfaceHint::WithinSurface &&
            cylHint == Octree::IntersectSurfaceHint::OutsideSurface) {
        return sphereHint;
    }
    else if (cylHint == Octree::IntersectSurfaceHint::WithinSurface &&
                 sphereHint == Octree::IntersectSurfaceHint::OutsideSurface) {
        return cylHint;
    }
    else if (sphereHint == Octree::IntersectSurfaceHint::IntersectsSurface &&
             cylHint == Octree::IntersectSurfaceHint::OutsideSurface) {
        return sphereHint;
    }
    else if (cylHint == Octree::IntersectSurfaceHint::IntersectsSurface &&
             sphereHint == Octree::IntersectSurfaceHint::OutsideSurface) {
        return cylHint;
    }

    return ret;
}

Octree::IntersectSurfaceHint Pw5Viewer::octreeUnionHelper(Octree *octree) const
{
    const Octree::IntersectSurfaceHint sphereHint = octreeSphereHelper(octree);
    const Octree::IntersectSurfaceHint cylHint = octreeCylinderHelper(octree);

    if (sphereHint == Octree::IntersectSurfaceHint::WithinSurface ||
            cylHint == Octree::IntersectSurfaceHint::WithinSurface) {
        return Octree::IntersectSurfaceHint::WithinSurface;
    }

    if (sphereHint == Octree::IntersectSurfaceHint::IntersectsSurface ||
            cylHint == Octree::IntersectSurfaceHint::IntersectsSurface) {
        return Octree::IntersectSurfaceHint::IntersectsSurface;
    }

    if (sphereHint == Octree::IntersectSurfaceHint::OutsideSurface ||
            cylHint == Octree::IntersectSurfaceHint::OutsideSurface) {
        return Octree::IntersectSurfaceHint::OutsideSurface;
    }

    return Octree::IntersectSurfaceHint::OutsideSurface;
}

void Pw5Viewer::setShowRefSphere(bool show)
{
    if (m_showRefObjects != show) {
        m_showRefObjects = show;

        update();
    }
}

int Pw5Viewer::sphereRadius() const
{
    return m_sphereRadius;
}

void Pw5Viewer::setSphereRadius(float radius)
{
    m_sphereRadius = radius;

    computeRefObject();

    m_octree.clear();
    m_octree = Octree(m_center, m_sphereRadius, nullptr);

    computeOctree();

    update();
}

int Pw5Viewer::cylRadius() const
{
    return m_cylRadius;
}

void Pw5Viewer::setCylRadius(float radius)
{
    m_cylRadius = radius;

    computeRefObject();

    m_octree.clear();
    m_octree = Octree(m_center, (m_sphereRadius > m_cylRadius) ? m_sphereRadius
                                                               : m_cylRadius,
                      nullptr);

    computeOctree();

    update();
}

//TODO USE FLOATS INSTEAD OF floatS


/////////////////////// Main ///////////////////////

int main(int argc, char **argv)
{
    QApplication app(argc, argv);

    auto viewer = new Pw5Viewer;

    auto viewerLayout = new QVBoxLayout(viewer);

    // Create controls
    auto controlsFrame = new QFrame(viewer);
    controlsFrame->setAutoFillBackground(true);

    //  A combo box for choosing the adapter function
    auto adapterFuncCBox = new QComboBox(controlsFrame);
    adapterFuncCBox->addItem("No adapter");
    adapterFuncCBox->addItem("Volumic Sphere");
    adapterFuncCBox->addItem("Volumic Cylinder");
    adapterFuncCBox->addItem("Intersect");
    adapterFuncCBox->addItem("Subtract");
    adapterFuncCBox->addItem("Union");

    adapterFuncCBox->setItemData(0, static_cast<int>(Pw5Viewer::AdapterFunc::NoAdapter));
    adapterFuncCBox->setItemData(1, static_cast<int>(Pw5Viewer::AdapterFunc::VolumicSphere));
    adapterFuncCBox->setItemData(2, static_cast<int>(Pw5Viewer::AdapterFunc::VolumicCylinder));
    adapterFuncCBox->setItemData(3, static_cast<int>(Pw5Viewer::AdapterFunc::Intersect));
    adapterFuncCBox->setItemData(4, static_cast<int>(Pw5Viewer::AdapterFunc::Subtract));
    adapterFuncCBox->setItemData(5, static_cast<int>(Pw5Viewer::AdapterFunc::Union));

    //  A check box for displaying the reference quadric object
    auto showRefCheckBox = new QCheckBox(controlsFrame);
    showRefCheckBox->setChecked(false);

    //  A spin box for controlling the sphere radius
    auto sphereRadiusSpinBox = new QDoubleSpinBox(controlsFrame);
    sphereRadiusSpinBox->setValue(viewer->sphereRadius());

    //  A spin box for controlling the cyl radius
    auto cylRadiusSpinBox = new QDoubleSpinBox(controlsFrame);
    cylRadiusSpinBox->setValue(viewer->cylRadius());

    //  A spin box for controlling the octree resolution
    auto resSpinBox = new QSpinBox(controlsFrame);
    resSpinBox->setMaximum(10);

    //  A combo box for setting the render mode
    auto renderModeCBox = new QComboBox(controlsFrame);
    renderModeCBox->addItem("Solid");
    renderModeCBox->addItem("Wireframe");
    renderModeCBox->addItem("Points");

    renderModeCBox->setItemData(0, static_cast<int>(Viewer::RenderMode::Solid));
    renderModeCBox->setItemData(1, static_cast<int>(Viewer::RenderMode::Wireframe));
    renderModeCBox->setItemData(2, static_cast<int>(Viewer::RenderMode::Points));

    renderModeCBox->setCurrentIndex(1);

    auto controlsFrameLayout = new QHBoxLayout(controlsFrame);
    controlsFrameLayout->addStretch();
    controlsFrameLayout->addWidget(new QLabel("Adapter function :"));
    controlsFrameLayout->addWidget(adapterFuncCBox);
    controlsFrameLayout->addWidget(new QLabel("Show reference object(s) :"));
    controlsFrameLayout->addWidget(showRefCheckBox);
    controlsFrameLayout->addWidget(new QLabel("Sphere Radius :"));
    controlsFrameLayout->addWidget(sphereRadiusSpinBox);
    controlsFrameLayout->addWidget(new QLabel("Cylinder Radius :"));
    controlsFrameLayout->addWidget(cylRadiusSpinBox);
    controlsFrameLayout->addWidget(new QLabel("Resolution :"));
    controlsFrameLayout->addWidget(resSpinBox);
    controlsFrameLayout->addWidget(new QLabel("Render Mode :"));
    controlsFrameLayout->addWidget(renderModeCBox);

    viewerLayout->addWidget(controlsFrame);
    viewerLayout->addStretch();

    // S/S connections
    QObject::connect(adapterFuncCBox, static_cast<void (QComboBox::*) (int)>(&QComboBox::currentIndexChanged),
                     [=] (int index) {
        viewer->setAdapterFunc(Pw5Viewer::AdapterFunc(adapterFuncCBox->itemData(index).toInt()));
    });

    QObject::connect(showRefCheckBox, &QCheckBox::toggled,
                     viewer, &Pw5Viewer::setShowRefSphere);

    QObject::connect(sphereRadiusSpinBox, static_cast<void (QDoubleSpinBox::*) (double)>(&QDoubleSpinBox::valueChanged),
                     viewer, &Pw5Viewer::setSphereRadius);

    QObject::connect(cylRadiusSpinBox, static_cast<void (QDoubleSpinBox::*) (double)>(&QDoubleSpinBox::valueChanged),
                     viewer, &Pw5Viewer::setCylRadius);

    QObject::connect(resSpinBox, static_cast<void (QSpinBox::*) (int)>(&QSpinBox::valueChanged),
                     viewer, &Pw5Viewer::setResolution);

    QObject::connect(renderModeCBox, static_cast<void (QComboBox::*) (int)>(&QComboBox::currentIndexChanged),
                     [=] (int index) {
        viewer->setRenderMode(Viewer::RenderMode(renderModeCBox->itemData(index).toInt()));
    });

    // Go
    viewer->show();

    return app.exec();
}

#include "main.moc"
