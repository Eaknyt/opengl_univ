#include "cameracontroller.h"

#include <assert.h>

#include <QtGui/QKeyEvent>

#include "epwmath/camera.h"
#include "epwmath/vec3.h"

#include "viewer.h"


using namespace epwmath;


CameraController::CameraController(Viewer *view) :
    m_viewer(view),
    m_moveSpeed(0.5),
    m_rotationSpeed(0.08),
    m_axisDirection(AxisDirection::None)
{
    assert (m_viewer);
}

void CameraController::drag(float offsetX, float offsetY)
{
    Camera *camera = m_viewer->camera();

    camera->truck(offsetX * m_moveSpeed, offsetY * m_moveSpeed);
}

void CameraController::rotateAroundTarget(float offsetX, float offsetY)
{
    const float angleX = offsetX * 90.0f / m_viewer->width();
    const float angleY = offsetY * 90.0f / m_viewer->height();

    Camera *camera = m_viewer->camera();

    camera->rotateAroundTarget(angleX * m_rotationSpeed,
                               angleY * m_rotationSpeed);
}

void CameraController::rotateEye(float offsetX, float offsetY)
{
    const float angleX = offsetX * 90.0f / m_viewer->width();
    const float angleY = offsetY * 90.0f / m_viewer->height();

    Camera *camera = m_viewer->camera();

    camera->rotateEye(angleX * m_rotationSpeed, angleY * m_rotationSpeed);
}

void CameraController::moveForward(float offset)
{
    Camera *camera = m_viewer->camera();

    camera->moveForward(offset * m_moveSpeed);
}

void CameraController::moveRight(float offset)
{
    Camera *camera = m_viewer->camera();

    camera->truck(offset * m_moveSpeed, 0);
}

void CameraController::move(float offsetX, float offsetZ)
{
    Camera *camera = m_viewer->camera();

    camera->move(offsetX * m_moveSpeed, offsetZ * m_moveSpeed);
}

float CameraController::moveSpeed() const
{
    return m_moveSpeed;
}

void CameraController::setMoveSpeed(float speed)
{
    if (m_moveSpeed != speed) {
        m_moveSpeed = speed;
    }
}

float CameraController::rotationSpeed() const
{
    return m_rotationSpeed;
}

void CameraController::setRotationSpeed(float rotationSpeed)
{
    if (m_rotationSpeed != rotationSpeed) {
        m_rotationSpeed = rotationSpeed;
    }
}

void CameraController::keyPressEvent(QKeyEvent *e)
{
    const int key = e->key();

    auto keyDirection = AxisDirection::None;

    if (key == Qt::Key_Z) {
        keyDirection = AxisDirection::Up;
    }
    else if (key == Qt::Key_S) {
        keyDirection = AxisDirection::Down;
    }
    else if (key == Qt::Key_Q) {
        keyDirection = AxisDirection::Left;
    }
    else if (key == Qt::Key_D) {
        keyDirection = AxisDirection::Right;
    }

    m_axisDirection = (AxisDirection) (m_axisDirection | keyDirection);

    moveTowardsDirection();
}

void CameraController::keyReleaseEvent(QKeyEvent *e)
{
    if (e->isAutoRepeat()) {
        e->ignore();
    }

    const int key = e->key();

    auto keyDirection = AxisDirection::None;

    if (key == Qt::Key_Z) {
        keyDirection = AxisDirection::Up;
    }
    else if (key == Qt::Key_S) {
        keyDirection = AxisDirection::Down;
    }
    else if (key == Qt::Key_Q) {
        keyDirection = AxisDirection::Left;
    }
    else if (key == Qt::Key_D) {
        keyDirection = AxisDirection::Right;
    }

    m_axisDirection = (AxisDirection) (m_axisDirection & ~keyDirection);

    moveTowardsDirection();
}

void CameraController::moveTowardsDirection()
{
    Vec3 dir;

    Camera *camera = m_viewer->camera();

    if (m_axisDirection & AxisDirection::Up) {
        dir += camera->forward();
    }
    else if (m_axisDirection & AxisDirection::Down) {
        dir += -camera->forward();
    }
    else if (m_axisDirection & AxisDirection::Left) {
        dir += camera->right();
    }
    else if (m_axisDirection & AxisDirection::Right) {
        dir += -camera->right();
    }

    camera->moveBy(dir * m_moveSpeed);
}
