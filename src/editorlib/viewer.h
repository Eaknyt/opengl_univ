#ifndef H_VIEWER_H
#define H_VIEWER_H

#include <QtGui/QOpenGLContext>
#include <QtGui/QOpenGLFunctions_2_1>

#include <QtWidgets/QOpenGLWidget>

#include "epwmath/vec3.h"

namespace epwmath {
class Camera;
} // namespace epwmath

class CameraController;


class Viewer : public QOpenGLWidget
{
    Q_OBJECT

public:
    enum class EventState
    {
        NoEventState,
        DraggingState,
        RotateAroundTarget,
        RotateEye
    };

    Viewer(QWidget *parent = nullptr);
    virtual ~Viewer();

    epwmath::Camera *camera() const;
    CameraController *cameraController() const;

protected:
    virtual void initializeGL() override;
    virtual void paintGL() override;
    virtual void resizeGL(int w, int h) override;

    virtual void mousePressEvent(QMouseEvent *e) override;
    virtual void mouseMoveEvent(QMouseEvent *e) override;
    virtual void mouseReleaseEvent(QMouseEvent *e) override;

    virtual void wheelEvent(QWheelEvent *e) override;

    virtual void keyPressEvent(QKeyEvent *e) override;
    virtual void keyReleaseEvent(QKeyEvent *e) override;

protected:
    virtual void render();

protected:
    QOpenGLFunctions_2_1 *gl;

    epwmath::Camera *m_camera;
    CameraController *m_cameraController;

    EventState m_eventState;

    QPoint m_lastMousePosOnPress;
    QPoint m_lastMousePos;
};

#endif // H_VIEWER_H
