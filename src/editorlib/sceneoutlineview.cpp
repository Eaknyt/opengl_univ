#include "sceneoutlineview.h"

#include <QtCore/QDebug>

#include <QtWidgets/QCheckBox>
#include <QtWidgets/QStyledItemDelegate>

#include "epwgl/scene/renderable.h"
#include "epwgl/scene/scene.h"


using namespace epwgl;


////////////////////// SceneOutlineView //////////////////////

SceneOutlineView::SceneOutlineView(QWidget *parent) :
    QTreeWidget(parent),
    m_scene(nullptr),
    m_renderableToItem(),
    m_itemToRenderable()
{
    QStringList columnsLabels;
    columnsLabels << "Name" << "AABB" << "Normals";

    setHeaderLabels(columnsLabels);

    connect(this, &SceneOutlineView::itemChanged,
            [=] (QTreeWidgetItem *item, int column) {
        if (column == 0 || column > 2) {
            return;
        }

        const QVariant itemData = item->data(column, Qt::CheckStateRole);
        const bool isChecked = itemData.toBool();

        auto renderableEntry = m_itemToRenderable.find(item);
        Q_ASSERT (renderableEntry != m_itemToRenderable.end());

        Renderable *renderable = renderableEntry->second;

        if (column == 1) {
            renderable->setShowBoundingVolume(isChecked);
        }
        else if (column == 2) {
            renderable->setShowNormals(isChecked);
        }
    });
}

void SceneOutlineView::setScene(Scene *scene)
{
    if (m_scene != scene) {
        m_scene = scene;

        if (!m_scene) {
            return;
        }

        connect(m_scene, &Scene::renderableAdded,
                this, &SceneOutlineView::addRenderableItem);

        connect(m_scene, &Scene::renderableRemoved,
                this, &SceneOutlineView::removeRenderableItem);
    }
}

void SceneOutlineView::addRenderableItem(Renderable *renderable)
{
    auto renderableItem = new QTreeWidgetItem;

    const QString text = "Renderable " + QString::number(m_scene->size());
    renderableItem->setText(0, text);
    renderableItem->setData(1, Qt::CheckStateRole, renderable->isShowingBoundingVolume());
    renderableItem->setData(2, Qt::CheckStateRole, renderable->isShowingNormals());

    m_renderableToItem.insert({renderable, renderableItem});
    m_itemToRenderable.insert({renderableItem, renderable});

    addTopLevelItem(renderableItem);
}

void SceneOutlineView::removeRenderableItem(Renderable *renderable)
{
    auto itemEntryToRemove = m_renderableToItem.find(renderable);

    if (itemEntryToRemove != m_renderableToItem.end()) {
        m_renderableToItem.erase(itemEntryToRemove);

        QTreeWidgetItem *itemToRemove = itemEntryToRemove->second;

        m_itemToRenderable.erase(itemToRemove);

        takeTopLevelItem(indexOfTopLevelItem(itemToRemove));
    }
}

#include "sceneoutlineview.moc"
