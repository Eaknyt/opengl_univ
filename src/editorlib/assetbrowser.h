#ifndef H_ASSETBROWSER_H
#define H_ASSETBROWSER_H

#include <QtWidgets/QWidget>


class QListWidget;


class AssetBrowser : public QWidget
{
    Q_OBJECT

public:
    explicit AssetBrowser(QWidget *parent = nullptr);

Q_SIGNALS:
    void offSelected(const QString &path);

private:
    QListWidget *m_listWidget;
};

#endif // H_ASSETBROWSER_H
