#include "sceneviewer.h"

#include "epwgl/drawhelpers.h"
#include "epwgl/geometrydrawer.h"

#include "epwgl/scene/renderer.h"
#include "epwgl/scene/scene.h"

#include "epwmath/boundingbox.h"
#include "epwmath/camera.h"


using namespace epwgl;
using namespace epwmath;


SceneViewer::SceneViewer(QWidget *parent) :
    Viewer(parent),
    m_scene(nullptr),
    m_renderer(nullptr)
{}

SceneViewer::SceneViewer(Scene *scene, QWidget *parent) :
    Viewer(parent),
    m_scene(nullptr),
    m_renderer(nullptr)
{
    setScene(scene);
}

SceneViewer::~SceneViewer()
{}

Scene *SceneViewer::scene() const
{
    return m_scene;
}

void SceneViewer::setScene(Scene *scene)
{
    if (m_scene != scene) {
        disconnect(scene, &Scene::sceneChanged,
                   this, static_cast<void (SceneViewer::*)()>(&SceneViewer::update));

        m_scene = scene;

        if (m_renderer) {
            m_renderer->setScene(scene);
        }

        //TODO retrieve a camera from the scene if existing
        m_scene->addCamera(m_camera);

        connect(m_scene, &Scene::sceneChanged,
                this, static_cast<void (SceneViewer::*)()>(&SceneViewer::update));
    }
}

void SceneViewer::setRenderer(Renderer *renderer)
{
    if (m_renderer != renderer) {
        m_renderer = renderer;

        if (m_scene) {
            m_renderer->setScene(m_scene);
        }
    }
}

void SceneViewer::setCamera(Camera *camera)
{
    if (m_camera != camera) {
        m_camera = camera;

        update();
    }
}

void SceneViewer::centerScene()
{
//    const AABoundingBox sceneBoundingVolume = m_scene->boundingVolume();

//    const Vec3 c = sceneBoundingVolume.center();

//    const Vec3 centerPosDistance = c - viewer1Camera->pos();
//    const float boundingSphereRadius = std::max({c.x(), c.y(), c.z()}src/apps/pw6/main.cpp);
//    const Vec3 boundingVolumeRadii = sceneBoundingVolume.radii();

//    m_camera->setPos(c - Vec3(boundingVolumeRadii.x(), 0, 0));
//    m_camera->setTarget({c.x(), c.y(), c.z()});
//    m_camera->setUp({0, 1, 0});
//    viewer1Camera->setNear(centerPosDistance.length() - boundingSphereRadius);
//    viewer1Camera->setFar(viewer1Camera->near() + 2 * boundingSphereRadius);
}

void SceneViewer::initializeGL()
{
    QOpenGLContext *ctx = QOpenGLContext::currentContext();
    gl = ctx->versionFunctions<QOpenGLFunctions_2_1>();
    Q_ASSERT (gl);

    eglClearColor(GlobalColor::Black);

    gl->glEnable(GL_DEPTH_TEST);
    gl->glDepthFunc(GL_LEQUAL);
    gl->glDepthRange(0.0f, 1.0f);

    gl->glEnable(GL_DEPTH_TEST);
    gl->glEnableClientState(GL_VERTEX_ARRAY);
}

void SceneViewer::paintGL()
{
    eglColor(GlobalColor::Red); //TODO support geometry color attributes

    m_renderer->render(gl, m_camera);
}
