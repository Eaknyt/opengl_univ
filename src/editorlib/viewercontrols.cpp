#include "viewercontrols.h"

#include <assert.h>

#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QComboBox>

#include "epwmath/camera.h"

#include "cameracontroller.h"
#include "dragtoolbutton.h"
#include "viewer.h"


using namespace epwmath;


ViewerControls::ViewerControls(Viewer *viewer) :
    QFrame(viewer),
    m_viewer(viewer)
{
    assert (m_viewer);

    auto cameraProjectionComboBox = new QComboBox(this);
    cameraProjectionComboBox->addItem("Perspective");
    cameraProjectionComboBox->addItem("Orthographic");

    cameraProjectionComboBox->setItemData(0, static_cast<int>(Camera::Projection::Perspective));
    cameraProjectionComboBox->setItemData(1, static_cast<int>(Camera::Projection::Orthographic));

    connect(cameraProjectionComboBox, static_cast<void (QComboBox::*) (int)>(&QComboBox::currentIndexChanged),
             [=] (int index) {
        auto newProj = Camera::Projection(index);

        m_viewer->camera()->setProjection(newProj);
    });

    auto moveForwardBtn = new DragToolButton(this);
    moveForwardBtn->setText("MoveFd");
    moveForwardBtn->setCallback([=] (float offsetX, float offsetY) {
        Q_UNUSED(offsetX);

        m_viewer->cameraController()->moveForward(offsetY);
    });

    auto dragBtn = new DragToolButton(this);
    dragBtn->setText("Drag");
    dragBtn->setCallback([=] (float offsetX, float offsetY) {
        m_viewer->cameraController()->drag(offsetX, offsetY);
    });

    auto rotateAroundTargetBtn = new DragToolButton(this);
    rotateAroundTargetBtn->setText("RotateAroundTarget");
    rotateAroundTargetBtn->setCallback([=] (float offsetX, float offsetY) {
        m_viewer->cameraController()->rotateAroundTarget(offsetX, offsetY);
    });

    auto rotateEyeBtn = new DragToolButton(this);
    rotateEyeBtn->setText("RotateEye");
    rotateEyeBtn->setCallback([=] (float offsetX, float offsetY) {
        m_viewer->cameraController()->rotateEye(offsetX, offsetY);
    });

    auto layout = new QHBoxLayout(this);

    layout->addWidget(cameraProjectionComboBox);

    layout->addStretch();

    layout->addWidget(moveForwardBtn);
    layout->addWidget(dragBtn);
    layout->addWidget(rotateAroundTargetBtn);
    layout->addWidget(rotateEyeBtn);
}
