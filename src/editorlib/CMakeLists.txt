project(libeditorlib)

SET(SOURCES
    dragtoolbutton.cpp
    rendersettingswidget.cpp
    viewer.cpp
    cameracontroller.cpp
    sceneviewer.cpp
    sceneoutlineview.cpp
    assetbrowser.cpp
    viewercontrols.cpp)

SET(HEADERS
    dragtoolbutton.h
    rendersettingswidget.h
    viewer.h
    cameracontroller.h
    sceneviewer.h
    sceneoutlineview.h
    assetbrowser.h
    viewercontrols.h)

# Qt5
find_package(Qt5Widgets REQUIRED)
set(CMAKE_AUTOMOC true)
set(CMAKE_INCLUDE_CURRENT_DIR ON)

include_directories(${CMAKE_SOURCE_DIR}/src/)

add_library(editorlib SHARED ${SOURCES} ${HEADERS})

target_link_libraries(editorlib epwgl epwmath ${OPENGL_LIBRARIES} Qt5::Widgets)
