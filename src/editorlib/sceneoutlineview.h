#ifndef H_SCENEOUTLINEVIEW_h
#define H_SCENEOUTLINEVIEW_h

#include <QtWidgets/QTreeWidget>

namespace epwgl {
class Renderable;
class Scene;
} // namespace epwgl


class SceneOutlineView : public QTreeWidget
{
public:
    SceneOutlineView(QWidget *parent = nullptr);

    void setScene(epwgl::Scene *scene);

private:
    void addRenderableItem(epwgl::Renderable *renderable);
    void removeRenderableItem(epwgl::Renderable *renderable);

private:
    friend class SceneOutlineViewDelegate;

    epwgl::Scene *m_scene;

    std::map<epwgl::Renderable *, QTreeWidgetItem *> m_renderableToItem;
    std::map<QTreeWidgetItem *, epwgl::Renderable *> m_itemToRenderable;
};

#endif // H_SCENEOUTLINEVIEW_h
