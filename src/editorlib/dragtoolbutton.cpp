#include "dragtoolbutton.h"

#include <QtGui/QMouseEvent>


DragToolButton::DragToolButton(QWidget *parent) :
    QToolButton(parent),
    m_lastMousePosOnPress(),
    m_lastMousePos()
{
    setCheckable(true);
}

void DragToolButton::setCallback(const std::function<void (float, float)> &callback)
{
    m_callback = callback;
}

void DragToolButton::mousePressEvent(QMouseEvent *e)
{
    setChecked(true);

    m_lastMousePosOnPress = m_lastMousePos = e->pos();
}

void DragToolButton::mouseMoveEvent(QMouseEvent *e)
{
    setCursor(Qt::BlankCursor);

    const QPoint newMousePos = e->pos();

    const QPoint mouseOffset = m_lastMousePos - newMousePos;
    const float offsetX = mouseOffset.x();
    const float offsetY = mouseOffset.y();

    m_callback(offsetX, offsetY);

    QCursor::setPos(mapToGlobal(m_lastMousePosOnPress));

    m_lastMousePos = m_lastMousePosOnPress;
}

void DragToolButton::mouseReleaseEvent(QMouseEvent *e)
{
    Q_UNUSED(e);

    setChecked(false);

    setCursor(Qt::ArrowCursor);
}
