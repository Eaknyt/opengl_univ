#ifndef H_CAMERACONTROLLER_H
#define H_CAMERACONTROLLER_H

class QKeyEvent;

namespace epwmath {
class Vec3;
}

class Viewer;

class CameraController
{
public:
    CameraController(Viewer *view);

    void drag(float offsetX, float offsetY);

    void rotateAroundTarget(float offsetX, float offsetY);
    void rotateEye(float offsetX, float offsetY);

    void moveForward(float offset);
    void moveRight(float offset);
    void move(float offsetX, float offsetZ);

    float moveSpeed() const;
    void setMoveSpeed(float moveSpeed);

    float rotationSpeed() const;
    void setRotationSpeed(float rotationSpeed);

    void keyPressEvent(QKeyEvent *e);
    void keyReleaseEvent(QKeyEvent *e);

    void moveTowardsDirection();

private:
    enum AxisDirection
    {
        None = 0,
        Up = 1,
        Down = 2,
        Left = 4,
        Right = 8,
    };

    Viewer *m_viewer;

    float m_moveSpeed;
    float m_rotationSpeed;

    AxisDirection m_axisDirection;
};

#endif // H_CAMERACONTROLLER_H
