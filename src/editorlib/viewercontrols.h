#ifndef H_VIEWERCONTROLS_H
#define H_VIEWERCONTROLS_H

#include <QtWidgets/QFrame>

class Viewer;

class ViewerControls : public QFrame
{
    Q_OBJECT

public:
    ViewerControls(Viewer *viewer);

private:
    Viewer *m_viewer;
};

#endif // H_VIEWERCONTROLS_H
