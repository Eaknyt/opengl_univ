#include "assetbrowser.h"

#include <QtWidgets/QListWidget>
#include <QtWidgets/QVBoxLayout>

#include <QtCore/QDebug>
#include <QtCore/QDir>
#include <QtCore/QDirIterator>

AssetBrowser::AssetBrowser(QWidget *parent) :
    QWidget(parent),
    m_listWidget(new QListWidget(this))
{
    auto layout = new QVBoxLayout(this);
    layout->addWidget(m_listWidget);

    // Load assets names
    QDirIterator it("res/off");

    while (it.hasNext()) {
        const QString filePath = it.next();

        if (!filePath.endsWith(".")) {
            const int lastSepIdx = filePath.lastIndexOf("/") + 1;
            const QString fileName = filePath.mid(lastSepIdx);

            if (fileName.endsWith(".off")) {
                m_listWidget->addItem(fileName);
            }
        }
    }

    // S/S connections
    connect(m_listWidget, &QListWidget::itemActivated,
            [=] (QListWidgetItem *item) {
        Q_EMIT offSelected(item->text());
    });
}
