#ifndef H_SCENEVIEWER_H
#define H_SCENEVIEWER_H

#include "editorlib/viewer.h"

namespace epwmath {
class Camera;
} // namespace epwmath

namespace epwgl {
class Renderer;
class Scene;
} // namespace epwgl


class SceneViewer : public Viewer
{
    Q_OBJECT

public:
    explicit SceneViewer(QWidget *parent = nullptr);
    SceneViewer(epwgl::Scene *scene, QWidget *parent = nullptr);
    ~SceneViewer();

    epwgl::Scene *scene() const;
    void setScene(epwgl::Scene *scene);

    void setRenderer(epwgl::Renderer *renderer);

    void setCamera(epwmath::Camera *camera);

    void centerScene();

protected:
    void initializeGL() override;
    void paintGL() override;

protected:
    epwgl::Scene *m_scene;
    epwgl::Renderer *m_renderer;
};

#endif // H_SCENEVIEWER_H
