#include "rendersettingswidget.h"

#include <QtWidgets/QCheckBox>


RenderControlsComboBox::RenderControlsComboBox(QWidget *parent) :
    QComboBox(parent),
    m_popup(new QFrame(this))
{
    m_popup->setVisible(false);
}

void RenderControlsComboBox::showPopup()
{
    m_popup->setVisible(true);
    m_popup->move(mapToGlobal(geometry().bottomLeft()));

    auto c = new QCheckBox(m_popup);
    c->setText("eazeazezaeaze");
}

void RenderControlsComboBox::hidePopup()
{
    m_popup->setVisible(false);
}
