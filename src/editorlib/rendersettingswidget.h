#ifndef H_RENDERSETTINGSWIDGET_H
#define H_RENDERSETTINGSWIDGET_H

#include <QtWidgets/QComboBox>


class RenderControlsComboBox : public QComboBox
{
    Q_OBJECT

public:
    RenderControlsComboBox(QWidget *parent = nullptr);

    void showPopup() override;
    void hidePopup() override;

Q_SIGNALS:
    void setCullfacesChanged(bool);

private:
    QFrame *m_popup;

    bool m_setCullfaces;
};

#endif // H_RENDERSETTINGSWIDGET_H
