#include "viewer.h"

#include <iostream>

#include <QtCore/QDebug>

#include <QtGui/QKeyEvent>
#include <QtGui/QMouseEvent>
#include <QtGui/QWheelEvent>

#include "epwmath/camera.h"
#include "epwmath/vec3.h"
#include "epwmath/vec4.h"

#include "epwgl/color.h"
#include "epwgl/drawhelpers.h"
#include "epwgl/geometry.h"
#include "epwgl/raii.h"

#include "cameracontroller.h"

using namespace epwgl;
using namespace epwmath;


Viewer::Viewer(QWidget *parent) :
    QOpenGLWidget(parent),
    gl(nullptr),
    m_camera(new Camera()),
    m_cameraController(new CameraController(this)),
    m_eventState(EventState::NoEventState),
    m_lastMousePosOnPress(),
    m_lastMousePos()
{
    setFocus(Qt::MouseFocusReason);
    setFocusPolicy(Qt::StrongFocus);

    QSurfaceFormat format;
    format.setDepthBufferSize(24);
    format.setStencilBufferSize(8);
    format.setVersion(2, 1);

    QSurfaceFormat::setDefaultFormat(format);
}

Viewer::~Viewer()
{
    makeCurrent();

    delete m_cameraController;
    delete m_camera;

    // Delete shaders if necessary

    if (gl) {
        gl->glDisableClientState(GL_VERTEX_ARRAY);
    }

    doneCurrent();
}

Camera *Viewer::camera() const
{
    return m_camera;
}

CameraController *Viewer::cameraController() const
{
    return m_cameraController;
}

void Viewer::initializeGL()
{
    QOpenGLContext *ctx = QOpenGLContext::currentContext();
    gl = ctx->versionFunctions<QOpenGLFunctions_2_1>();
    Q_ASSERT (gl);

    eglClearColor(GlobalColor::Black);

    gl->glEnable(GL_DEPTH_TEST);
    gl->glDepthFunc(GL_LEQUAL);
    gl->glDepthRange(0.0f, 1.0f);

    gl->glEnableClientState(GL_VERTEX_ARRAY);
}

void Viewer::paintGL()
{
    gl->glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    gl->glMatrixMode(GL_PROJECTION);
    gl->glLoadMatrixf(m_camera->projectionMatrix().constData());

    gl->glMatrixMode(GL_MODELVIEW);
    gl->glLoadMatrixf(m_camera->viewMatrix().constData());

    render();
}

void Viewer::resizeGL(int w, int h)
{
    gl->glViewport(0, 0, w, h);

    m_camera->setAspectRatio(static_cast<float>(w) / static_cast<float>(h));

    gl->glMatrixMode(GL_PROJECTION);
    gl->glLoadIdentity();
    gl->glLoadMatrixf(m_camera->projectionMatrix().constData());
}

void Viewer::mousePressEvent(QMouseEvent *e)
{
    const int mouseBtn = e->button();

    if (mouseBtn == Qt::MidButton) {
        m_eventState = EventState::DraggingState;

        m_lastMousePosOnPress = m_lastMousePos = e->pos();
    }
    else if (mouseBtn == Qt::RightButton) {
        m_eventState = EventState::RotateAroundTarget;

        m_lastMousePosOnPress = m_lastMousePos = e->pos();
    }
    else if (mouseBtn == Qt::LeftButton) {
        m_eventState = EventState::RotateEye;

        m_lastMousePosOnPress = m_lastMousePos = e->pos();
    }
}

void Viewer::mouseMoveEvent(QMouseEvent *e)
{
    const QPoint newMousePos = e->pos();

    const QPoint mouseOffset = m_lastMousePos - newMousePos;
    const float offsetX = mouseOffset.x();
    const float offsetY = mouseOffset.y();

    if (m_eventState == EventState::DraggingState) {
        setCursor(Qt::BlankCursor);

        m_cameraController->drag(offsetX, offsetY);
    }
    else if (m_eventState == EventState::RotateAroundTarget) {
        setCursor(Qt::BlankCursor);

        m_cameraController->rotateAroundTarget(offsetX, offsetY);
    }
    else if (m_eventState == EventState::RotateEye) {
        setCursor(Qt::BlankCursor);

        m_cameraController->rotateEye(offsetX, offsetY);
    }

    QCursor::setPos(mapToGlobal(m_lastMousePosOnPress));

    m_lastMousePos = m_lastMousePosOnPress;
//    m_lastMousePos = newMousePos;
}

void Viewer::mouseReleaseEvent(QMouseEvent *e)
{
    Q_UNUSED(e);

    if (m_eventState == EventState::DraggingState ||
            m_eventState == EventState::RotateAroundTarget ||
            m_eventState == EventState::RotateEye) {
        setCursor(Qt::ArrowCursor);

        m_eventState = EventState::NoEventState;
    }
}

void Viewer::wheelEvent(QWheelEvent *e)
{
    m_cameraController->moveForward(e->angleDelta().y());

    update();
}

void Viewer::keyPressEvent(QKeyEvent *e)
{
    m_cameraController->keyPressEvent(e);
}

void Viewer::keyReleaseEvent(QKeyEvent *e)
{
    m_cameraController->keyReleaseEvent(e);
}

void Viewer::render()
{}
