#ifndef H_DRAGTOOLBUTTON_H
#define H_DRAGTOOLBUTTON_H

#include <functional>

#include <QtWidgets/QToolButton>


class DragToolButton : public QToolButton
{
public:
    DragToolButton(QWidget *parent = nullptr);

    void setCallback(const std::function<void (float, float)> &callback);

protected:
    void mousePressEvent(QMouseEvent *e) override;
    void mouseMoveEvent(QMouseEvent *e) override;
    void mouseReleaseEvent(QMouseEvent *e) override;

private:
    QPoint m_lastMousePosOnPress;
    QPoint m_lastMousePos;

    std::function<void (float, float)> m_callback;
};

#endif // H_DRAGTOOLBUTTON_H
