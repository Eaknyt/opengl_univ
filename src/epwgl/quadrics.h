#ifndef H_EPWGL_QUADRICS_H
#define H_EPWGL_QUADRICS_H

#include "geometry.h"


namespace epwmath {
class Vec3;
} // namespace epwmath


namespace epwgl {

Geometry cylinder(int meridians, float radius, float height);

Geometry cone(int meridians, const epwmath::Vec3 &top,
              float baseRadius, float height);

Geometry sphere(int meridians, int parallels, float radius);

} // namespace epwgl

#endif // H_EPWGL_QUADRICS_H
