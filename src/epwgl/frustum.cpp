#include "frustum.h"

#include <cmath>
#include <iostream>

#include "epwmath/camera.h"


using namespace epwmath;


namespace epwgl {

Frustum::Frustum(Camera *camera) :
    m_nearTopLeft(),
    m_nearTopRight(),
    m_nearBottomRight(),
    m_nearBottomLeft(),
    m_farTopLeft(),
    m_farTopRight(),
    m_farBottomRight(),
    m_farBottomLeft(),
    m_camera(camera),
    m_geom(new Geometry(5, 8))
{
    compute();
}

void Frustum::compute()
{
    const Vec3 pos = m_camera->pos();
    const Vec3 direction = m_camera->target() - pos;
    const Vec3 directionNorm = m_camera->forward();
    const Vec3 up = m_camera->up();
    const Vec3 right = direction.normal(up);

    const float fovY = m_camera->fovY();
    const float fovY_2 = fovY * 0.5;

    const float aspectRatio = m_camera->aspectRatio();

    // Compute near plane vertices
    const float near = m_camera->near();

    const Vec3 nearPlaneCenter = pos + directionNorm * near;
    const float nearPlaneHeight = 2 * near * std::tan(fovY_2);
    const float nearPlaneWidth = nearPlaneHeight * aspectRatio;

    m_nearTopLeft = nearPlaneCenter - right * nearPlaneWidth + up * nearPlaneHeight;
    m_nearTopRight = nearPlaneCenter + right * nearPlaneWidth + up * nearPlaneHeight;

    m_nearBottomLeft = m_nearTopLeft - up * nearPlaneHeight * 2;
    m_nearBottomRight = m_nearTopRight - up * nearPlaneHeight * 2;

    // Compute far plane vertices
    const float far = m_camera->far();

    const Vec3 farPlaneCenter = pos + directionNorm * far;
    const float farPlaneHeight = 2 * far * std::tan(fovY_2);
    const float farPlaneWidth = farPlaneHeight * aspectRatio;

    m_farTopLeft = farPlaneCenter - right * farPlaneWidth + up * farPlaneHeight;
    m_farTopRight = farPlaneCenter + right * farPlaneWidth + up * farPlaneHeight;

    m_farBottomLeft = m_farTopLeft - up * farPlaneHeight * 2;
    m_farBottomRight = m_farTopRight - up * farPlaneHeight * 2;

    // Compute the geometry
    m_geom->vertices = {
        m_camera->pos(), m_nearTopLeft, m_nearBottomLeft,
        m_nearBottomRight, m_nearTopRight
    };

    // Draw only from the camera position to the near plane
    m_geom->indices = {
        0, 1,
        0, 2,
        0, 3,
        0, 4,
        1, 2,
        2, 3,
        3, 4,
        4, 1,
    };

    /* Indices for drawing entire frustum
        0, 1,
        1, 2,
        2, 3,
        3, 0,
        4, 5,
        5, 6,
        6, 7,
        7, 4,
        0, 4,
        1, 5,
        2, 6,
        3, 7
    */
}

Geometry *Frustum::geometry()
{
    return m_geom;
}

} // namespace epwgl
