#include "volumes.h"

#include "epwmath/vec3.h"


using namespace epwmath;


namespace epwgl {

Geometry voxel(const Vec3 &center, float radius)
{
    Geometry ret;

    const float cx = center.x();
    const float cy = center.y();
    const float cz = center.z();

    const Vec3 v0 {cx - radius, cy - radius, cz + radius};
    const Vec3 v1 {cx + radius, cy - radius, cz + radius};
    const Vec3 v2 {cx + radius, cy + radius, cz + radius};
    const Vec3 v3 {cx - radius, cy + radius, cz + radius};
    const Vec3 v4 {cx - radius, cy - radius, cz - radius};
    const Vec3 v5 {cx + radius, cy - radius, cz - radius};
    const Vec3 v6 {cx + radius, cy + radius, cz - radius};
    const Vec3 v7 {cx - radius, cy + radius, cz - radius};

    ret.vertices = {
        v0, v1, v2, v3,
        v4, v5, v6, v7
    };

    ret.indices = {
        0, 1, 2, 3,
        0, 4, 5, 1,
        3, 2, 6, 7,
        0, 3, 7, 4,
        1, 5, 6, 2,
        5, 4, 7, 6
    };

    return ret;
}

Geometry *voxel(const std::array<Vec3, 8> &vertices)
{
    auto ret = new Geometry;

    ret->vertices = {
        vertices[0], vertices[1], vertices[2], vertices[3],
        vertices[4], vertices[5], vertices[6], vertices[7]
    };

    ret->indices = {
        0, 1, 2, 3,
        0, 4, 5, 1,
        3, 2, 6, 7,
        0, 3, 7, 4,
        1, 5, 6, 2,
        5, 4, 7, 6
    };

    return ret;
}

} //namespace epwgl
