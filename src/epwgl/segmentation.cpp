#include "segmentation.h"

#include <array>
#include <iostream>

#include "epwmath/units.h"

#include "geometry.h"


using namespace epwmath;


namespace epwgl {

std::vector<unsigned int> adjacentTriangles(const Geometry *geom,
                                            unsigned int index)
{
    std::vector<unsigned int> ret;

    for (std::size_t i = 0; i < geom->indicesCount(); i += 3) {
        const unsigned int fv1 = geom->indices[i];
        const unsigned int fv2 = geom->indices[i + 1];
        const unsigned int fv3 = geom->indices[i + 2];

        if (index == fv1 || index == fv2 || index == fv3) {
            ret.push_back(fv1);
            ret.push_back(fv2);
            ret.push_back(fv3);
        }
    }

    return ret;
}

std::vector<unsigned int> adjacentTriangles(const Geometry *geom,
                                            unsigned int edgeIndex1,
                                            unsigned int edgeIndex2)
{
    std::vector<unsigned int> ret;

    for (std::size_t i = 0; i < geom->indicesCount(); i += 3) {
        const unsigned int fv1 = geom->indices[i];
        const unsigned int fv2 = geom->indices[i + 1];
        const unsigned int fv3 = geom->indices[i + 2];

        if ( (edgeIndex1 == fv1 && edgeIndex2 == fv2) ||
             (edgeIndex1 == fv2 && edgeIndex2 == fv1) ) {
            ret.push_back(edgeIndex1);
            ret.push_back(edgeIndex2);
            ret.push_back(fv3);
        }

        if ( (edgeIndex1 == fv2 && edgeIndex2 == fv3) ||
             (edgeIndex1 == fv3 && edgeIndex2 == fv2) ) {
            ret.push_back(edgeIndex1);
            ret.push_back(edgeIndex2);
            ret.push_back(fv1);
        }

        if ( (edgeIndex1 == fv3 && edgeIndex2 == fv1) ||
             (edgeIndex1 == fv1 && edgeIndex2 == fv3) ) {
            ret.push_back(edgeIndex1);
            ret.push_back(edgeIndex2);
            ret.push_back(fv2);
        }
    }

    return ret;
}

float angleBetweenTriangles(const Vec3 &commonEdgeV1, const Vec3 &commonEdgeV2,
                             const Vec3 &face1V3, const Vec3 &face2V3)
{
    const Vec3 commonEdgeCenter = Vec3(commonEdgeV2.x() - commonEdgeV1.x(),
                                       commonEdgeV2.y() - commonEdgeV1.y(),
                                       commonEdgeV2.z() - commonEdgeV1.z()) / 2;

    const Vec3 face1Dir = Vec3(face1V3.x() - commonEdgeCenter.x(),
                               face1V3.y() - commonEdgeCenter.y(),
                               face1V3.z() - commonEdgeCenter.z());

    const Vec3 face2Dir = Vec3(face2V3.x() - commonEdgeCenter.x(),
                               face2V3.y() - commonEdgeCenter.y(),
                               face2V3.z() - commonEdgeCenter.z());

    const float ret = face1Dir.angle(face2Dir);

    return ret;
}

float angleBetweenTriangles(const Geometry *geom,
                             unsigned int t1v1, unsigned int t1v2, unsigned int t1v3,
                             unsigned int t2v1, unsigned int t2v2, unsigned int t2v3)
{
    Vec3 commonEdgeV1;
    Vec3 commonEdgeV2;

    Vec3 face1V3;
    Vec3 face2V3;

    if (t1v1 == t2v1 && t1v2 == t2v2) {
        commonEdgeV1 = geom->vertices[t1v1];
        commonEdgeV2 = geom->vertices[t1v2];

        face1V3 = geom->vertices[t1v3];
        face2V3 = geom->vertices[t2v3];
    }
    else if (t1v1 == t2v1 && t1v3 == t2v3) {
        commonEdgeV1 = geom->vertices[t1v1];
        commonEdgeV2 = geom->vertices[t2v3];

        face1V3 = geom->vertices[t1v2];
        face2V3 = geom->vertices[t2v3];
    }
    else if (t1v2 == t2v2 && t1v3 == t2v3) {
        commonEdgeV1 = geom->vertices[t1v2];
        commonEdgeV2 = geom->vertices[t2v3];

        face1V3 = geom->vertices[t1v1];
        face2V3 = geom->vertices[t2v1];
    }


    return angleBetweenTriangles(commonEdgeV1, commonEdgeV2, face1V3, face2V3);
}

std::vector<unsigned int> sharpEdges(const Geometry *geom, float angle)
{
    std::vector<unsigned int> ret;

    for (std::size_t idx = 0; idx < geom->indicesCount(); idx += 3) {
        const unsigned int fv1 = geom->indices[idx];
        const unsigned int fv2 = geom->indices[idx + 1];
        const unsigned int fv3 = geom->indices[idx + 2];

        std::array<std::vector<unsigned int>, 3> eAdjsPack = {
            adjacentTriangles(geom, fv1, fv2),
            adjacentTriangles(geom, fv2, fv3),
            adjacentTriangles(geom, fv3, fv1)
        };

        for (std::size_t eIt = 0; eIt < 3; ++eIt) {
            std::vector<unsigned int> eAdjs = eAdjsPack[eIt];

            for (std::size_t i = 0; i < eAdjs.size(); i += 3) {
                const unsigned int eAdjV1 = eAdjs[i];
                const unsigned int eAdjV2 = eAdjs[i + 1];
                const unsigned int eAdjV3 = eAdjs[i + 2];

                for (std::size_t j = 0; j < eAdjs.size() && j != i; j += 3) {
                    const unsigned int eAdjV1_n = eAdjs[j];
                    const unsigned int eAdjV2_n = eAdjs[j + 1];
                    const unsigned int eAdjV3_n = eAdjs[j + 2];

                    float a = angleBetweenTriangles(geom,
                                                     eAdjV1, eAdjV2, eAdjV3,
                                                     eAdjV1_n, eAdjV2_n, eAdjV3_n);

                    if (radToDeg(a) >= angle) {
                        ret.push_back(fv1);
                        ret.push_back(fv2);
                    }
                }
            }
        }
    }

    return ret;
}

} // namespace epwgl
