#include "picking.h"

#include <assert.h>

#include <QtGui/QOpenGLContext>
#include <QtGui/QOpenGLFunctions>

#include "epwmath/camera.h"

using namespace epwmath;


namespace epwgl {

//PickingService::PickingService(Viewer *viewer) :
//    m_viewer(viewer)
//{
//    assert (m_viewer);
//}

//Vec3 PickingService::pickWithZBuffer(int winX, int winY) const
//{
//    const int width = m_viewer->width();
//    const int height = m_viewer->height();

//    float z = 1;

//    m_viewer->makeCurrent();

//    QOpenGLFunctions *gl = m_viewer->context()->functions();
//    gl->glReadPixels(winX, height - winY - 1, 1, 1,
//                     GL_DEPTH_COMPONENT, GL_FLOAT, &z);

//    m_viewer->doneCurrent();

//    const Camera *camera = m_viewer->camera();

//    const Mat4 mvMat = camera->viewMatrix();
//    const Mat4 projMat = camera->projectionMatrix();

//    const Vec3 winCoords(winX, winY, z);

//    Vec3 ret = winCoords.unproject(mvMat, projMat, width, height);

//    return ret;
//}

//Vec3 PickingService::pick(int x, int y, ZPickMethod method) const
//{
//    if (method == ZPickMethod::ReadFromZBuffer) {
//        return pickWithZBuffer(x, y);
//    }
//}

} // namespace epwgl
