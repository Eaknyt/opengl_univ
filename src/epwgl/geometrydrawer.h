#ifndef H_EPWGL_GEOMETRYDRAWER_H
#define H_EPWGL_GEOMETRYDRAWER_H

#include "color.h"


namespace epwgl {

class Geometry;

class GeometryDrawer
{
public:
    enum DrawMode
    {
        Points = 0x0000,
        Lines = 0x0001,
        LineLoop = 0x0002,
        LineStrip = 0x0003,
        Triangles = 0x0004,
        TriangleStrip = 0x0005,
        TriangleFan = 0x0006
    };

    GeometryDrawer();
    explicit GeometryDrawer(Geometry *mesh);
    GeometryDrawer(Geometry *mesh, DrawMode draw_mode);
    ~GeometryDrawer();

    Geometry *geometry() const;
    void setGeometry(Geometry *mesh);

    DrawMode drawMode() const;
    void setDrawMode(DrawMode mode);

    Color color() const;
    void setColor(const Color &color);

private:
    Geometry *m_mesh;
    DrawMode m_drawMode;

    Color m_color;
};

} // namespace epwgl

#endif // H_EPWGL_GEOMETRYDRAWER_H
