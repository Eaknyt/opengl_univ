#ifndef H_EPWGL_GEOMETRY_H
#define H_EPWGL_GEOMETRY_H

#include <vector>

#include "epwmath/vec3.h"

namespace epwgl {

struct Geometry
{
    struct Triangle
    {
        unsigned int v1 = 0;
        unsigned int v2 = 0;
        unsigned int v3 = 0;
    };

    Geometry();
    Geometry(std::size_t vertexCount);
    Geometry(std::size_t vertexCount, std::size_t faceCount);
    Geometry(std::size_t vertexCount, std::size_t indexCount,
             std::size_t normalCount);

    std::size_t vertexCount() const;
    std::size_t indicesCount() const;
    std::size_t normalCount() const;

    void clear();

    std::vector<epwmath::Vec3> vertices;
    std::vector<unsigned int> indices;
    std::vector<epwmath::Vec3> normals;
};

} // namespace epwgl

#endif // H_EPWGL_GEOMETRY_H
