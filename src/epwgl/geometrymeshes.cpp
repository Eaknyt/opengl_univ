#include "geometrymeshes.h"

#include <cmath>

#include "epwmath/vec3.h"

#include "geometry.h"


using namespace epwmath;


namespace epwgl {

Geometry *cylinderMesh(int meridians, float radius, float height)
{
    auto ret = new Geometry(2 * meridians, 4 * meridians);

    for (int m = 0; m <= meridians; ++m) {
        const float meridianAngle = 2 * M_PI * (static_cast<float>(m) / meridians);

        Vec3 vertex1 { radius * std::cos(meridianAngle),
                    radius * std::sin(meridianAngle),
                    -static_cast<float>(height) / 2 };

        Vec3 vertex2 { vertex1.x(), vertex1.y(), -vertex1.z() };

        ret->vertices.push_back(vertex1);
        ret->vertices.push_back(vertex2);

        const int vCount = ret->vertexCount();

        // Make face
        if (m < meridians && vCount > 2 && vCount % 2 == 0) {
            const int firstIdx = vCount - 3;

            ret->indices.push_back(firstIdx);
            ret->indices.push_back(firstIdx + 1);
            ret->indices.push_back(firstIdx + 2);

            ret->indices.push_back(firstIdx + 2);
            ret->indices.push_back(firstIdx + 1);
            ret->indices.push_back(firstIdx + 3);
        }
        else if (m == meridians) {
            unsigned int fi1 = ret->indices[ret->indicesCount() - 3];
            unsigned int fi2 = ret->indices.back();

            ret->indices.push_back(fi1);
            ret->indices.push_back(fi2);
            ret->indices.push_back(ret->indices[0]);

            ret->indices.push_back(ret->indices[0]);
            ret->indices.push_back(fi2);
            ret->indices.push_back(ret->indices[1]);
        }
    }

    return ret;
}

Geometry *sphereMesh(int meridians, int parallels, float radius)
{
    auto ret = new Geometry(meridians * parallels * 3);

    const float dTheta = (2 * M_PI) / static_cast<float>(meridians);
    const float dPhi = M_PI / static_cast<float>(parallels);

    for (int p = 0; p < parallels + 1; ++p) {
        const float phi = M_PI_2 - static_cast<float>(p) * dPhi;
        const float cosPhi = std::cos(phi);
        const float sinPhi = std::sin(phi);

        for (int m = 0; m < meridians + 1; ++m) {
            const float theta = static_cast<float>(m) * dTheta;
            const float cosTheta = std::cos(theta);
            const float sinTheta = std::sin(theta);

            const float x = radius * cosTheta * cosPhi;
            const float y = radius * sinPhi;
            const float z = radius * sinTheta * cosPhi;

            Vec3 vertex {x, y, z};

            ret->vertices.push_back(vertex);
        }
    }

    for (int p = 1; p <= parallels - 1; ++p) {
        const int parallelStartIdx = p * (meridians + 1);
        const int nextParallelStartIdx = (p + 1 ) * (meridians + 1);

        for (int m = 0; m <= meridians; ++m) {
            ret->indices.push_back(parallelStartIdx + m);
            ret->indices.push_back(parallelStartIdx + m + 1);
            ret->indices.push_back(nextParallelStartIdx + m);

            ret->indices.push_back(nextParallelStartIdx + m);
            ret->indices.push_back(parallelStartIdx + m + 1);
            ret->indices.push_back(nextParallelStartIdx + m + 1);
        }
    }

    return ret;
}

} // namespace epwgl
