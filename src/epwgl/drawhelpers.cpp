#include "drawhelpers.h"

#include "epwmath/octree.h"
#include "epwmath/vec3.h"

#include "geometry.h"
#include "volumes.h"


namespace epwgl {

using namespace epwmath;

void eglDrawPoint(const Vec3 &vertex)
{
    glBegin(GL_POINTS);
    {
        glVertex3f(vertex.x(), vertex.y(), vertex.z());
    }
    glEnd();
}

void eglDrawLine(const Vec3 &p1, const Vec3 &p2)
{
    glBegin(GL_LINES);
    {
        glVertex3f(p1.x(), p1.y(), p1.z());
        glVertex3f(p2.x(), p2.y(), p2.z());
    }
    glEnd();
}

void eglDrawCurve(const std::vector<Vec3> &points)
{
    glBegin(GL_LINE_STRIP);
    {
        for (std::size_t i = 0; i < points.size(); ++i) {
            Vec3 p = points[i];

            glVertex3f(p.x(), p.y(), p.z());
        }
    }
    glEnd();
}

void eglDrawSurface(const std::vector<std::vector<Vec3> > &surface)
{
    for (const std::vector<Vec3> &curve : surface) {
        eglDrawCurve(curve);
    }

    glBegin(GL_LINES);
    {
        const std::vector<Vec3> &firstCurve = surface[0];
        const std::vector<Vec3> &lastCurve = surface[surface.size() - 1];

        for (std::size_t i = 0; i < firstCurve.size(); ++i) {
            const Vec3 &p1 = firstCurve[i];
            const Vec3 &p2 = lastCurve[i];

            glVertex3f(p1.x(), p1.y(), p1.z());
            glVertex3f(p2.x(), p2.y(), p2.z());
        }
    }
    glEnd();
}

void eglDrawGeometry(const Geometry &geom, int draw_mode)
{
    const std::size_t iCount = geom.indicesCount();

    glVertexPointer(3, GL_FLOAT, 0, geom.vertices.data());

    glDrawElements(draw_mode, iCount, GL_UNSIGNED_INT,
                   geom.indices.data());
}

void eglDrawOctree(Octree *octree)
{
    for (Octree *child : octree->children()) {
        eglDrawOctree(child);
    }

    if (octree->isEmpty() && octree->draw()) {
//        eglDrawGeometry(octree->geometry(), GL_QUADS);
    }
}

} // namespace epwgl
