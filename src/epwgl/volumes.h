#ifndef H_EPWGL_VOLUMES_H
#define H_EPWGL_VOLUMES_H

#include <array>

#include "geometry.h"


namespace epwmath {
class Vec3;
} // namespace epwmath


namespace epwgl {

Geometry voxel(const epwmath::Vec3 &center, float radius);

Geometry *voxel(const std::array<epwmath::Vec3, 8> &vertices);

} //namespace epwgl

#endif // H_EPWGL_VOLUMES_H
