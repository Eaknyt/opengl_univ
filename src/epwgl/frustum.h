#ifndef H_EPWGL_FRUSTUM_H
#define H_EPWGL_FRUSTUM_H

#include "epwmath/vec3.h"

#include "geometry.h"


namespace epwmath {
class Camera;
} // namespace epwmath


namespace epwgl {

class Frustum
{
public:
    Frustum(epwmath::Camera *camera);

    void compute();

    Geometry *geometry();

private:
    epwmath::Vec3 m_nearTopLeft;
    epwmath::Vec3 m_nearTopRight;
    epwmath::Vec3 m_nearBottomRight;
    epwmath::Vec3 m_nearBottomLeft;

    epwmath::Vec3 m_farTopLeft;
    epwmath::Vec3 m_farTopRight;
    epwmath::Vec3 m_farBottomRight;
    epwmath::Vec3 m_farBottomLeft;

    epwmath::Camera *m_camera;
    Geometry *m_geom;
};

} // namespace epwgl

#endif // H_EPWGL_FRUSTUM_H
