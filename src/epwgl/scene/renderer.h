#ifndef H_EPWGL_RENDERER_H
#define H_EPWGL_RENDERER_H

class QOpenGLFunctions_2_1;


namespace epwmath {
class Camera;
} // namespace epwmath


namespace epwgl {

class GeometryDrawer;
class Renderable;
class Scene;

class Renderer
{
public:
    enum class RenderMode : int
    {
        Solid = 0x1B02,
        Wireframe = 0x1B01,
        Points = 0x1B00
    };

    Renderer();

    void setScene(Scene *scene);

    void setRenderMode(RenderMode mode);

    bool areBackfacesCulled() const;
    void setCullBackfaces(bool cull);

    bool isShowingHelpers() const;
    void setShowHelpers(bool show);

    virtual void render(QOpenGLFunctions_2_1 *gl, epwmath::Camera *camera);

protected:
    void drawMeshes(QOpenGLFunctions_2_1 *gl, epwmath::Camera *camera);

private:
    void drawRenderable(QOpenGLFunctions_2_1 *gl, const Renderable *renderable);
    void drawMesh(QOpenGLFunctions_2_1 *gl, const GeometryDrawer *drawer);

private:
    Scene *m_scene;

    RenderMode m_renderMode;

    bool m_cullBackfaces;

    bool m_showHelpers;
};

} // namespace epwgl

#endif // H_EPWGL_RENDERER_H
