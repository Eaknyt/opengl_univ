#include "scene.h"

#include <iostream>

#include <QtGui/QOpenGLFunctions_2_1>

#include "epwmath/camera.h"

#include "epwgl/frustum.h"
#include "epwgl/geometry.h"
#include "epwgl/geometrydrawer.h"

#include "epwgl/scene/renderable.h"


using namespace epwmath;


namespace epwgl {

Scene::Scene(QObject *parent) :
    QObject(parent),
    m_renderables(),
    m_cameras(),
    m_helperDrawers(),
    m_cameraToDrawer()
{}

Scene::~Scene()
{
    m_renderables.clear();
    m_cameras.clear();
    m_helperDrawers.clear();
}

void Scene::addRenderable(Renderable *renderable)
{
    if (!m_renderables.contains(renderable)) {
        m_renderables.push_back(renderable);

        connect(renderable, &Renderable::changed,
                this, &Scene::sceneChanged);

        Q_EMIT renderableAdded(renderable);
        Q_EMIT sceneChanged();
    }
}

void Scene::removeRenderable(Renderable *renderable)
{
    if (m_renderables.contains(renderable)) {
        m_renderables.removeAll(renderable);

        disconnect(renderable, &Renderable::changed,
                   this, &Scene::sceneChanged);

        Q_EMIT renderableRemoved(renderable);
        Q_EMIT sceneChanged();
    }
}

QVector<Renderable *> Scene::renderables() const
{
    return m_renderables;
}

void Scene::addCamera(Camera *camera)
{
    if (!m_cameras.contains(camera)) {
        m_cameras.push_back(camera);

        Geometry *cameraFrustumGeom = Frustum(camera).geometry();
        auto cameraFrustumDrawer = new GeometryDrawer(cameraFrustumGeom,
                                                      GeometryDrawer::Lines);

        addHelperDrawer(cameraFrustumDrawer);

        camera->setCallback([=] (Camera *camera) {
            updateCameraFrustum(camera);

            Q_EMIT sceneChanged();
        });

        m_cameraToDrawer.insert(camera, cameraFrustumDrawer);
    }
}

GeometryDrawer *Scene::drawerForCamera(Camera *camera) const
{
    return m_cameraToDrawer.value(camera);
}

QVector<GeometryDrawer *> Scene::helperDrawers() const
{
    return m_helperDrawers;
}

std::size_t Scene::size() const
{
    return m_renderables.size();
}

void Scene::clear()
{
    auto it = m_renderables.begin();

    while (it != m_renderables.end()) {
        Renderable *toDelete = (*it);

        it = m_renderables.erase(it);

        Q_EMIT renderableRemoved(toDelete);

        delete toDelete;
    }

    Q_EMIT sceneChanged();
}

void Scene::updateCameraFrustum(Camera *camera)
{
    if (!m_cameraToDrawer.contains(camera)) {
        return;
    }

    GeometryDrawer *cameraDrawer = m_cameraToDrawer.value(camera);

    Geometry *cameraFrustumGeom = Frustum(camera).geometry();

    cameraDrawer->setGeometry(cameraFrustumGeom);
}

void Scene::addHelperDrawer(GeometryDrawer *drawer)
{
    if (!m_helperDrawers.contains(drawer)) {
        m_helperDrawers.push_back(drawer);
    }
}

} // namespace epwgl
