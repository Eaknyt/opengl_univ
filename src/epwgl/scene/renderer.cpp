#include "renderer.h"

#include <assert.h>

#include <QtCore/QDebug>

#include <QtGui/QOpenGLFunctions_2_1>

#include "epwmath/camera.h"

#include "epwgl/geometry.h"
#include "epwgl/geometrydrawer.h"
#include "epwgl/raii.h"

#include "renderable.h"
#include "scene.h"


using namespace epwmath;


namespace epwgl {

Renderer::Renderer() :
    m_scene(nullptr),
    m_renderMode(RenderMode::Solid),
    m_cullBackfaces(false),
    m_showHelpers(true)
{}

void Renderer::setScene(Scene *scene)
{
    if (m_scene != scene) {
        m_scene = scene;
    }
}

void Renderer::setRenderMode(RenderMode mode)
{
    if (m_renderMode != mode) {
        m_renderMode = mode;
    }
}

bool Renderer::areBackfacesCulled() const
{
    return m_cullBackfaces;
}

void Renderer::setCullBackfaces(bool cull)
{
    if (m_cullBackfaces != cull) {
        m_cullBackfaces = cull;
    }
}

bool Renderer::isShowingHelpers() const
{
    return m_showHelpers;
}

void Renderer::setShowHelpers(bool show)
{
    if (m_showHelpers != show) {
        m_showHelpers = show;
    }
}

void Renderer::render(QOpenGLFunctions_2_1 *gl, Camera *camera)
{
    gl->glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    gl->glMatrixMode(GL_PROJECTION);
    gl->glLoadMatrixf(camera->projectionMatrix().constData());

    gl->glMatrixMode(GL_MODELVIEW);
    gl->glLoadMatrixf(camera->viewMatrix().constData());

    gl->glPolygonMode(GL_FRONT_AND_BACK,
                      static_cast<unsigned int>(m_renderMode));

    if (m_cullBackfaces) {
        gl->glEnable(GL_CULL_FACE);
    }
    else {
        gl->glDisable(GL_CULL_FACE);
    }

    drawMeshes(gl, camera);
}

void Renderer::drawMeshes(QOpenGLFunctions_2_1 *gl, Camera *camera)
{
    for (const Renderable *renderable : m_scene->renderables()) {
        drawRenderable(gl, renderable);
    }

    if (m_showHelpers) {
        const GeometryDrawer *cameraDrawer = m_scene->drawerForCamera(camera);

        for (const GeometryDrawer *hDrawer : m_scene->helperDrawers()) {
            if (hDrawer != cameraDrawer) {
                drawMesh(gl, hDrawer);
            }
        }
    }
}

void Renderer::drawRenderable(QOpenGLFunctions_2_1 *gl,
                              const Renderable *renderable)
{
    GeometryDrawer *mesh = renderable->mesh();
    Geometry *drawerGeom = mesh->geometry();

    if (!drawerGeom) {
        return;
    }

    const std::size_t iCount = drawerGeom->indicesCount();

    gl->glVertexPointer(3, GL_FLOAT, 0, drawerGeom->vertices.data());

    const bool drawNormals =
            !drawerGeom->normals.empty() && renderable->isShowingNormals();

    if (drawNormals) {
        gl->glNormalPointer(GL_FLOAT, 0, drawerGeom->normals.data());
    }

    GlAttribGuard attribGuard(gl, GL_CURRENT_BIT | GL_ENABLE_BIT);

    const Color color = mesh->color();
    gl->glColor3f(color.red(), color.green(), color.blue());

    gl->glDrawElements(mesh->drawMode(), iCount, GL_UNSIGNED_INT,
                       drawerGeom->indices.data());

    if (drawNormals) {
        GlAttribGuard attribGuard2(gl, GL_CURRENT_BIT | GL_ENABLE_BIT);

        gl->glColor3f(0, 1., 0);

        glBegin(GL_LINES);

        for (std::size_t i = 0; i < drawerGeom->normalCount(); ++i) {
            const Vec3 n = drawerGeom->normals[i];
            const Vec3 v = drawerGeom->vertices[i];

            glVertex3f(v.x(), v.y(), v.z());
            glVertex3f(v.x() + n.x(), v.y() + n.y(), v.z() + n.z());
        }

        glEnd();
    }

    if (renderable->isShowingBoundingVolume()) {
        drawMesh(gl, renderable->aabbDrawer());
    }
}

void Renderer::drawMesh(QOpenGLFunctions_2_1 *gl, const GeometryDrawer *drawer)
{
    Geometry *mesh = drawer->geometry();

    if (!mesh) {
        return;
    }

    const std::size_t iCount = mesh->indicesCount();

    gl->glVertexPointer(3, GL_FLOAT, 0, mesh->vertices.data());

    GlAttribGuard attribGuard(gl, GL_CURRENT_BIT | GL_ENABLE_BIT);

    const Color color = drawer->color();
    gl->glColor3f(color.red(), color.green(), color.blue());

    gl->glDrawElements(drawer->drawMode(), iCount, GL_UNSIGNED_INT,
                       mesh->indices.data());
}

} // namespace epwgl
