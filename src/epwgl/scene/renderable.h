#ifndef H_EPGWGL_RENDERABLE_H
#define H_EPGWGL_RENDERABLE_H

#include <memory>

#include <QtCore/QObject>

namespace epwmath {
class AABoundingBox;
class Transform;
} // namespace epwmath

namespace epwgl {

class GeometryDrawer;

class Renderable : public QObject
{
    Q_OBJECT

public:
    Renderable();
    ~Renderable();

    GeometryDrawer *mesh() const;
    epwmath::Transform *transform() const;

    GeometryDrawer *aabbDrawer() const;

    bool isShowingBoundingVolume() const;
    void setShowBoundingVolume(bool show);

    bool isShowingNormals() const;
    void setShowNormals(bool show);

Q_SIGNALS:
    void changed(Renderable *);

private:
    void updateBoundingVolume();

private:
    std::unique_ptr<GeometryDrawer> m_mesh;
    std::unique_ptr<epwmath::Transform> m_transform;

    std::unique_ptr<epwmath::AABoundingBox> m_aabb;

    std::unique_ptr<GeometryDrawer> m_aabbDrawer;

    bool m_showBoundingVolume;
    bool m_showNormals;
};

} // namespace epwgl

#endif // H_EPGWGL_OBJECT_H
