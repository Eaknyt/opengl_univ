#include "renderable.h"

#include "epwmath/boundingbox.h"
#include "epwmath/transform.h"

#include "epwgl/geometrydrawer.h"
#include "epwgl/volumes.h"

#include <QDebug>


using namespace epwmath;


namespace epwgl {

Renderable::Renderable() :
    QObject(),
    m_mesh(std::make_unique<GeometryDrawer>()),
    m_transform(std::make_unique<Transform>()),
    m_aabb(std::make_unique<AABoundingBox>()),
    m_aabbDrawer(std::make_unique<GeometryDrawer>()),
    m_showBoundingVolume(false),
    m_showNormals(false)
{
    m_aabbDrawer->setDrawMode(GeometryDrawer::Lines);
}

Renderable::~Renderable()
{}

//TODO update aabb when modifying the mesh or the transform

GeometryDrawer *Renderable::mesh() const
{
    return m_mesh.get();
}

Transform *Renderable::transform() const
{
    return m_transform.get();
}

GeometryDrawer *Renderable::aabbDrawer() const
{
    return m_aabbDrawer.get();
}

bool Renderable::isShowingBoundingVolume() const
{
    return m_showBoundingVolume;
}

void Renderable::setShowBoundingVolume(bool show)
{
    if (m_showBoundingVolume != show) {
        m_showBoundingVolume = show;

        Q_EMIT changed(this);
    }
}

bool Renderable::isShowingNormals() const
{
    return m_showNormals;
}

void Renderable::setShowNormals(bool show)
{
    if (m_showNormals != show) {
        m_showNormals = show;

        Q_EMIT changed(this);
    }
}

void Renderable::updateBoundingVolume()
{
    m_aabb.reset(new AABoundingBox(m_mesh->geometry()->vertices));

    std::array<Vec3, 8> corners = m_aabb->getCorners();
    Geometry *aabbGeom = voxel(corners);

    m_aabbDrawer->setGeometry(aabbGeom);
}

} // namespace epwgl
