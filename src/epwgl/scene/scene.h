#ifndef H_EPWGL_SCENE_H
#define H_EPWGL_SCENE_H

#include <QtCore/QMap>
#include <QtCore/QObject>
#include <QtCore/QVector>

#include "epwmath/boundingbox.h"

class QOpenGLFunctions_2_1;

namespace epwmath {
class AABoundingBox;
class Camera;
} // namespace epwmath


namespace epwgl {

class GeometryDrawer;
class Renderable;

class Scene : public QObject
{
    Q_OBJECT

public:
    explicit Scene(QObject *parent = nullptr);
    ~Scene();

    void addRenderable(Renderable *renderable);
    void removeRenderable(Renderable *renderable);

    QVector<Renderable *> renderables() const;

    void addCamera(epwmath::Camera *camera);

    GeometryDrawer *drawerForCamera(epwmath::Camera *camera) const;

    QVector<GeometryDrawer *> helperDrawers() const;

    std::size_t size() const;
    void clear();

Q_SIGNALS:
    void sceneChanged();
    void renderableAdded(Renderable *);
    void renderableRemoved(Renderable *);

private:
    void updateCameraFrustum(epwmath::Camera *camera);

    void addHelperDrawer(GeometryDrawer *drawer);

private:
    QVector<Renderable *> m_renderables;
    QVector<epwmath::Camera *> m_cameras;
    QVector<GeometryDrawer *> m_helperDrawers;

    QMap<epwmath::Camera *, GeometryDrawer *> m_cameraToDrawer;
};

} // namespace epwgl

#endif // H_EPWGL_SCENE_H
