#include "quadrics.h"

#include <cmath>
#include <iostream>
#include <vector>

#include "epwmath/vec3.h"


using namespace epwmath;


namespace epwgl {

Geometry cylinder(int meridians, float radius, float height)
{
    Geometry ret(2 * meridians, 4 * meridians);

    for (int i = 0; i <= meridians; ++i) {
        const float meridianAngle = 2 * M_PI * (static_cast<float>(i) / meridians);

        Vec3 p1 { radius * std::cos(meridianAngle),
                    radius * std::sin(meridianAngle),
                    -static_cast<float>(height) / 2 };

        Vec3 p2 { p1.x(), p1.y(), -p1.z() };

        ret.vertices.push_back(p1);
        ret.vertices.push_back(p2);

        const int vCount = ret.vertexCount();

        // Make face
        if (i < meridians && vCount > 2 && vCount % 2 == 0) {
            const int firstIdx = vCount - 3;

            ret.indices.push_back(firstIdx);
            ret.indices.push_back(firstIdx + 1);
            ret.indices.push_back(firstIdx + 3);
            ret.indices.push_back(firstIdx + 2);
        }
        else if (i == meridians) {
            unsigned int fi1 = ret.indices.back();
            unsigned int fi2 = ret.indices[ret.indicesCount() - 2];

            ret.indices.push_back(fi1);
            ret.indices.push_back(fi2);
            ret.indices.push_back(ret.indices[1]);
            ret.indices.push_back(ret.indices[0]);
        }
    }

    return ret;
}

Geometry cone(int meridians, const Vec3 &top,
              float baseRadius, float height)
{
    Geometry ret(2 * meridians, 4 * meridians);

    ret.vertices.push_back(top);

    for (int i = 0; i <= meridians; ++i) {
        const float meridianAngle = 2 * M_PI * (static_cast<float>(i) / meridians);

        Vec3 p { baseRadius * std::cos(meridianAngle),
                   baseRadius * std::sin(meridianAngle),
                    -height * 0.5f };

        ret.vertices.push_back(p);

        const int vCount = ret.vertexCount();

        // Make face
        if (i < meridians && (vCount - 1) > 2) {
            ret.indices.push_back(0);
            ret.indices.push_back(vCount - 1);
            ret.indices.push_back(vCount);
        }
        else if (i == meridians) {
            unsigned int fi1 = ret.indices.back();
            unsigned int fi2 = ret.indices[1];

            ret.indices.push_back(0);
            ret.indices.push_back(fi1);
            ret.indices.push_back(fi2);
        }
    }

    return ret;
}

Geometry sphere(int meridians, int parallels, float radius)
{
    Geometry ret(meridians * parallels * 3, meridians * parallels * 4);

    const float R = 1. / static_cast<float>(meridians - 1);
    const float S = 1. / static_cast<float>(parallels - 1);

    for (int r = 0; r < meridians; ++r) {
        for (int s = 0; s < parallels; ++s) {
            const float x = std::cos(2 * M_PI * s * S) * std::sin(M_PI * r * R);
            const float y = std::sin(-M_PI_2 + M_PI * r * R);
            const float z = std::sin(2 * M_PI * s * S) * std::sin(M_PI * r * R);

            Vec3 p { x * radius, y * radius, z * radius };

            ret.vertices.push_back(p);
        }
    }

    for (int r = 0; r < meridians; ++r) {
        for (int s = 0; s < parallels; ++s) {
            ret.indices.push_back(r * parallels + s);
            ret.indices.push_back(r * parallels + (s + 1));
            ret.indices.push_back((r + 1) * parallels + (s + 1));
            ret.indices.push_back((r + 1) * parallels + s);
        }
    }

    return ret;
}

} // namespace epwgl
