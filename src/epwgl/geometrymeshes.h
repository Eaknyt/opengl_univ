#ifndef H_EPWGL_GEOMETRYMESHES_H
#define H_EPWGL_GEOMETRYMESHES_H

namespace epwgl {

class Geometry;

Geometry *cylinderMesh(int meridians, float radius, float height);

Geometry *sphereMesh(int meridians, int parallels, float radius);

} // namespace epwgl

#endif // H_EPWGL_GEOMETRYMESHES_H
