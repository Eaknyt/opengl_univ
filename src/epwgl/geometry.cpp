#include "geometry.h"


using namespace epwmath;


namespace epwgl {

Geometry::Geometry() :
    vertices(),
    indices()
{}

Geometry::Geometry(std::size_t vertexCount) :
    vertices(),
    indices(),
    normals()
{
    vertices.reserve(vertexCount);
}

Geometry::Geometry(std::size_t vertexCount, std::size_t indexCount) :
    vertices(),
    indices(),
    normals()
{
    vertices.reserve(vertexCount);
    indices.reserve(indexCount);
}

Geometry::Geometry(std::size_t vertexCount, std::size_t indexCount,
                   std::size_t normalCount) :
    vertices(),
    indices(),
    normals()
{
    vertices.reserve(vertexCount);
    indices.reserve(indexCount);
    indices.reserve(normalCount);
}

std::size_t Geometry::vertexCount() const
{
    return vertices.size();
}

std::size_t Geometry::indicesCount() const
{
    return indices.size();
}

std::size_t Geometry::normalCount() const
{
    return normals.size();
}

void Geometry::clear()
{
    vertices.clear();
    indices.clear();
    normals.clear();
}

} // namespace epwgl
