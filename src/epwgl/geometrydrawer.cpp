#include "geometrydrawer.h"

#include "geometry.h"

using namespace epwmath;

namespace epwgl {

GeometryDrawer::GeometryDrawer() :
    m_mesh(nullptr),
    m_drawMode(DrawMode::Triangles),
    m_color(GlobalColor::Red)
{}

GeometryDrawer::GeometryDrawer(Geometry *mesh) :
    m_mesh(mesh),
    m_drawMode(DrawMode::Triangles),
    m_color(GlobalColor::Red)
{}

GeometryDrawer::GeometryDrawer(Geometry *mesh, DrawMode draw_mode) :
    m_mesh(mesh),
    m_drawMode(draw_mode),
    m_color(GlobalColor::Red)
{}

GeometryDrawer::~GeometryDrawer()
{
    delete m_mesh;
}

Geometry *GeometryDrawer::geometry() const
{
    return m_mesh;
}

void GeometryDrawer::setGeometry(Geometry *mesh)
{
    if (m_mesh != mesh) {
        delete m_mesh;
        m_mesh = nullptr;

        m_mesh = mesh;
    }
}

GeometryDrawer::DrawMode GeometryDrawer::drawMode() const
{
    return m_drawMode;
}

void GeometryDrawer::setDrawMode(GeometryDrawer::DrawMode mode)
{
    if (m_drawMode != mode) {
        m_drawMode = mode;
    }
}

Color GeometryDrawer::color() const
{
    return m_color;
}

void GeometryDrawer::setColor(const Color &color)
{
    if (m_color != color) {
        m_color = color;
    }
}

} // namespace epwgl
