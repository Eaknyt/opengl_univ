#include "offloader.h"

#include <algorithm>
#include <array>
#include <assert.h>
#include <iostream>
#include <fstream>
#include <limits>

using namespace epwmath;

namespace {

using namespace epwgl;

void ifs_jump_to_next_line(std::ifstream &ifs)
{
    ifs.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
}

std::vector<Geometry::Triangle> trianglesSharedByVertex(Geometry *mesh,
                                                        unsigned int vi)
{
    std::vector<Geometry::Triangle> ret;

    for (std::size_t i = 0; i < mesh->indicesCount(); i += 3) {
        const unsigned int fi1 = mesh->indices[i];
        const unsigned int fi2 = mesh->indices[i + 1];
        const unsigned int fi3 = mesh->indices[i + 2];

        if (fi1 == vi || fi2 == vi || fi3 == vi) {
            ret.push_back({fi1, fi2, fi3});
        }
    }

    return ret;
}

} // anon namespace


namespace epwgl {

Geometry *readOff(const std::string &filePath)
{
    Geometry *ret = nullptr;

    std::ifstream ifs(filePath);

    if (!ifs.is_open()) {
        std::cerr << "readOff() > problem reading " << filePath << std::endl;
        return ret;
    }

    // Read header
    std::string header;

    ifs >> header;

    if (header != "OFF") {
        std::cerr << "readOff() > " << filePath << " is not a .off."
                  << std::endl;
        return ret;
    }

    // Read data counts
    ifs_jump_to_next_line(ifs);

    int vertexCount = 0;
    int triCount = 0;
    int edgeCount = 0;

    ifs >> vertexCount >> triCount >> edgeCount;

    if (vertexCount < 0 || triCount < 0 || edgeCount < 0) {
        std::cerr << "readOff() > invalid data counts in " << filePath
                  << std::endl;
        return ret;
    }

    const int normalCount = vertexCount;

    ret = new Geometry(vertexCount, triCount, normalCount);

    // Read vertices...
    ifs_jump_to_next_line(ifs);

    int vertexCounter = vertexCount;

    while (vertexCounter > 0) {
        float x = 0;
        float y = 0;
        float z = 0;

        ifs >> x >> y >> z;

        ret->vertices.push_back({x, y, z});

        vertexCounter--;

        ifs_jump_to_next_line(ifs);
    }

    // ... and faces
    int faceCounter = triCount;

    while (faceCounter > 0) {
        int faceVCount = 0;
        int v1 = 0;
        int v2 = 0;
        int v3 = 0;

        ifs >> faceVCount;
        assert(faceVCount == 3);

        ifs >> v1 >> v2 >> v3;

        ret->indices.push_back(v1);
        ret->indices.push_back(v2);
        ret->indices.push_back(v3);

        faceCounter--;

        ifs_jump_to_next_line(ifs);
    }

    ifs_jump_to_next_line(ifs);
    assert(ifs.eof());

    ifs.close();

    // Compute normals
    std::vector<unsigned int> processedVertices;

    for (std::size_t i = 0; i < ret->vertexCount(); ++i) {
        auto vertexProcessed = std::find(processedVertices.begin(), processedVertices.end(), i);
        if (vertexProcessed != processedVertices.end()) {
            continue;
        }

        const std::vector<Geometry::Triangle> triangles =
                trianglesSharedByVertex(ret, i);

        Vec3 normalsSum;

        for (const Geometry::Triangle &tri : triangles) {
            const unsigned int trv1 = tri.v1;
            const unsigned int trv2 = tri.v2;
            const unsigned int trv3 = tri.v3;

            assert (trv1 != trv2 && trv2 != trv3 && trv1 != trv3);

            const Vec3 triNormal = Vec3::normal(ret->vertices[trv1],
                                                ret->vertices[trv2],
                                                ret->vertices[trv3]);

            normalsSum += triNormal;
        }

        const Vec3 normal = normalsSum.normalized();
        ret->normals.push_back(normal);

        processedVertices.push_back(i);
    }

    assert (ret->vertexCount() == ret->normalCount());

    return ret;
}

} // namespace epwgl
