#ifndef H_EPWGL_HELPERS_H
#define H_EPWGL_HELPERS_H

#include <vector>

#include <GL/gl.h>

#include "color.h"


// Forward decls
namespace epwmath {
class Octree;
class Vec3;
} // namespace epwmath


namespace epwgl {

class Geometry;

inline void eglClearColor(const Color &color)
{
    glClearColor(color.red(), color.green(), color.blue(), color.alpha());
}

inline void eglColor(const Color &color)
{
    glColor4f(color.red(), color.green(), color.blue(), color.alpha());
}

void eglDrawPoint(const epwmath::Vec3 &vertex);

void eglDrawLine(const epwmath::Vec3 &p1, const epwmath::Vec3 &p2);

void eglDrawCurve(const std::vector<epwmath::Vec3> &points);

void eglDrawSurface(const std::vector<std::vector<epwmath::Vec3>> &surface);

void eglDrawGeometry(const Geometry &geom, int draw_mode);

void eglDrawOctree(epwmath::Octree *octree);

} // namespace epwgl

#endif // H_EPWGL_HELPERS_H
