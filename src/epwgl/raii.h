#ifndef H_EPWGL_RAII_H
#define H_EPWGL_RAII_H

#include <QtGui/QOpenGLFunctions_2_1>

namespace epwgl {

class GlAttribGuard
{
public:
    GlAttribGuard(QOpenGLFunctions_2_1 *gl, GLbitfield mask) :
        m_gl(gl)
    {
        m_gl->glPushAttrib(mask);
    }

    ~GlAttribGuard()
    {
        m_gl->glPopAttrib();
    }

private:
    QOpenGLFunctions_2_1 *m_gl;
};

} // namespace epwgl

#endif // H_EPWGL_RAII_H
