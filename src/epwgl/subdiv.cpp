#include "subdiv.h"

#include "epwgl/geometry.h"


using namespace epwmath;

namespace epwgl {

Vec3 midpoint(const Vec3 &a, const Vec3 &b)
{
    Vec3 ret((a.x() + b.x()) * 0.5,
             (a.y() + b.y()) * 0.5,
             (a.z() + b.z()) * 0.5);

    return ret;
}

void subdivPolyhedral(Geometry *geom)
{
    const std::vector<unsigned int> oldIndices = geom->indices;

    std::vector<unsigned int> indices;

    for (std::size_t i = 0; i < oldIndices.size(); i += 3) {
        const unsigned int i0 = oldIndices[i];
        const unsigned int i1 = oldIndices[i + 1];
        const unsigned int i2 = oldIndices[i + 2];

        const Vec3 v0 = geom->vertices[i0];
        const Vec3 v1 = geom->vertices[i1];
        const Vec3 v2 = geom->vertices[i2];

        const Vec3 m01 = midpoint(v0, v1);
        const Vec3 m12 = midpoint(v1, v2);
        const Vec3 m02 = midpoint(v0, v2);

        geom->vertices.push_back(m01);
        geom->vertices.push_back(m12);
        geom->vertices.push_back(m02);

        const unsigned int m01i = geom->vertices.size() - 3;
        const unsigned int m12i = m01i + 1;
        const unsigned int m02i = m01i + 2;

        indices.push_back(i0);
        indices.push_back(m01i);
        indices.push_back(m02i);

        indices.push_back(i1);
        indices.push_back(m12i);
        indices.push_back(m01i);

        indices.push_back(i2);
        indices.push_back(m02i);
        indices.push_back(m12i);

        indices.push_back(m01i);
        indices.push_back(m12i);
        indices.push_back(m02i);
    }

    geom->indices = indices;
}

} // namespace epwgl
