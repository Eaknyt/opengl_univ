#ifndef H_EPWGL_OFFLOADER_H
#define H_EPWGL_OFFLOADER_H

#include <string>

#include "geometry.h"

namespace epwgl {

Geometry *readOff(const std::string &filePath);

} // namespace epwgl

#endif // H_EPWGL_OFFLOADER_H
