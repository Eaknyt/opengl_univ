#ifndef H_EPWGL_PICKING_H
#define H_EPWGL_PICKING_H

#include "epwmath/vec3.h"

namespace epwgl {

//class PickingService
//{
//public:
//    enum class ZPickMethod
//    {
//        ReadFromZBuffer
//    };

//    PickingService(Viewer *viewer);

//    epwmath::Vec3 pick(int x, int y, ZPickMethod method) const;

//private:
//    epwmath::Vec3 pickWithZBuffer(int winX, int winY) const;

//private:
//    Viewer *m_viewer;
//};

} // namespace epwgl

#endif // H_EPWGL_PICKING_H
