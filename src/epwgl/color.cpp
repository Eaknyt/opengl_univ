#include "color.h"

namespace {

const epwgl::Color globalColors[] = {
	epwgl::Color::fromRgb(0, 0, 0),
	epwgl::Color::fromRgb(255, 255, 255),
	epwgl::Color::fromRgb(128, 128, 128),
	epwgl::Color::fromRgb(160, 160, 164),
	epwgl::Color::fromRgb(192, 192, 192),
	epwgl::Color::fromRgb(255, 0, 0),
	epwgl::Color::fromRgb(0, 255, 0),
	epwgl::Color::fromRgb(0, 0, 255),
	epwgl::Color::fromRgb(0, 255, 255),
	epwgl::Color::fromRgb(255, 0, 255),
	epwgl::Color::fromRgb(255, 255, 0),
	epwgl::Color::fromRgb(128, 0, 0),
	epwgl::Color::fromRgb(0, 128, 0),
	epwgl::Color::fromRgb(0, 0, 128),
	epwgl::Color::fromRgb(0, 128, 128),
	epwgl::Color::fromRgb(128, 0, 128),
	epwgl::Color::fromRgb(128, 128, 0),
	epwgl::Color::fromRgb(0, 0, 0, 0),
	epwgl::Color(0.24f, 0.73f, 0.13f),
	epwgl::Color(0.26f, 0.54f, 1.0f)
};

} // anon namespace

namespace epwgl {

Color::Color() :
	m_r(0.),
	m_g(0.),
	m_b(0.),
	m_a(1.)
{}

Color::Color(float r, float g, float b, float a) :
	m_r(r > 1. ? 0. : r),
	m_g(g > 1. ? 0. : g),
	m_b(b > 1. ? 0. : b),
	m_a(a > 1. ? 0. : a)
{}

Color::Color(GlobalColor color)
{
	Color col = globalColors[static_cast<int>(color)];

	m_r = col.m_r;
	m_g = col.m_g;
	m_b = col.m_b;
	m_a = col.m_a;
}

Color::Color(const Color &other) :
	m_r(other.m_r),
	m_g(other.m_g),
	m_b(other.m_b),
	m_a(other.m_a)
{}

Color Color::fromRgb(unsigned char r, unsigned char g, unsigned char b,
		  			 unsigned char a)
{
	Color ret((1. / 255.) * r,
			  (1. / 255.) * g,
			  (1. / 255.) * b,
			  (1. / 255.) * a);

	return ret;
}

float Color::red() const
{
	return m_r;
}

void Color::setRed(float r)
{
	if (m_r != r) {
		m_r = r;
	}
}

float Color::green() const
{
	return m_g;
}

void Color::setGreen(float g)
{
	if (m_g != g) {
		m_g = g;
	}
}

float Color::blue() const
{
	return m_b;
}

void Color::setBlue(float b)
{
	if (m_b != b) {
		m_b = b;
	}
}

float Color::alpha() const
{
	return m_a;
}

void Color::setAlpha(float a)
{
	if (m_a != a) {
		m_a = a;
	}
}

Color &Color::operator=(const Color &color)
{
	m_r = color.m_r;
	m_g = color.m_g;
	m_b = color.m_b;
	m_a = color.m_a;

	return *this;
}

Color &Color::operator=(GlobalColor color)
{
    return operator=(Color(color));
}

bool operator==(const Color &lhs, const Color &rhs)
{
    if (lhs.m_r != rhs.m_r) {
        return false;
    }

    if (lhs.m_g != rhs.m_g) {
        return false;
    }

    if (lhs.m_b != rhs.m_b) {
        return false;
    }

    if (lhs.m_a != rhs.m_a) {
        return false;
    }

    return true;
}

bool operator!=(const Color &lhs, const Color &rhs)
{
    return !(lhs == rhs);
}

} // namespace epwgl
