#ifndef H_EPWGL_SEGMENTATION_H
#define H_EPWGL_SEGMENTATION_H

#include <vector>

#include "epwmath/vec3.h"

namespace epwgl {

class Geometry;

std::vector<unsigned int> adjacentTriangles(const Geometry *geom,
                                            unsigned int index);

std::vector<unsigned int> adjacentTriangles(const Geometry *geom,
                                            unsigned int edgeIndex1,
                                            unsigned int edgeIndex2);

float angleBetweenTriangles(const epwmath::Vec3 &commonEdgeV1, const epwmath::Vec3 &commonEdgeV2,
                             const epwmath::Vec3 &face1V3, const epwmath::Vec3 &face2V3);

float angleBetweenTriangles(const Geometry *geom,
                             unsigned int t1v1, unsigned int t1v2, unsigned int t1v3,
                             unsigned int t2v1, unsigned int t2v2, unsigned int t2v3);

std::vector<unsigned int> sharpEdges(const Geometry *geom, float angle);

} // namespace epwgl

#endif // H_EPWGL_SEGMENTATION_H
