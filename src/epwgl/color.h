#ifndef H_EPWGL_COLOR_H
#define H_EPWGL_COLOR_H

namespace epwgl {

enum class GlobalColor : short
{
	Black,
	White,
	DarkGray,
	MediumGray,
	LightGray,
	Red,
	Green,
	Blue,
	Cyan,
	Magenta,
	Yellow,
	DarkRed,
	DarkGreen,
	DarkBlue,
	DarkCyan,
	DarkMagenta,
	DarkYellow,
	Transparent,
	XAxis = Red,
	YAxis,
	ZAxis
};

class Color
{
public:
	Color();
	Color(float r, float g, float b, float a = 1.);
	Color(GlobalColor color);
	Color(const Color &other);

	static Color fromRgb(unsigned char r, unsigned char g, unsigned char b,
		  				 unsigned char a = 255);

	float red() const;
	void setRed(float r);

	float green() const;
	void setGreen(float g);
	
	float blue() const;
	void setBlue(float b);

	float alpha() const;
	void setAlpha(float a);

	Color &operator=(const Color &color);
	Color &operator=(GlobalColor color);

    friend bool operator==(const Color &lhs, const Color &rhs);
    friend bool operator!=(const Color &lhs, const Color &rhs);

private:
	float m_r;
	float m_g;
	float m_b;
	float m_a;
};

} // namespace epwgl

#endif // H_EPWGL_COLOR_H
