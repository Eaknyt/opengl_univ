#ifndef H_EPWGL_SUBDIV_H
#define H_EPWGL_SUBDIV_H

#include "epwmath/vec3.h"


namespace epwgl {

class Geometry;

epwmath::Vec3 midpoint(const epwmath::Vec3 &a, const epwmath::Vec3 &b);

void subdivPolyhedral(Geometry *geom);

} // namespace epwgl

#endif // H_EPWGL_SUBDIV_H
